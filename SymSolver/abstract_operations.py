#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 25 2021

@author: Sevans

File purpose: AbstractOperation

TODO:
    - should "Abstract" Sum, Product, Power go in this file?
    - something is wrong with the caching for _expand, I think? ("Generic" case gets mad if rerun.)
    - can probably improve simplify & distribute by enforcing coefficient outside is positive.
        e.g. somewhere in simplify(), convert -7 * (a - b) --> 7 * (- a + b).
"""

''' --------------------- Imports --------------------- '''
# import built-in packages
import functools
import warnings
import itertools
import collections
import time

# import internal modules
from . import tools
from .tools import (    # put into this namespace for historical reasons
    view, _str, _repr, apply, is_integer,
    equals, list_equals, dict_equals,
    RENDER_MATH,
)
from . import polynomials as poly


''' --------------------- Defaults --------------------- '''

TRACEBACKHIDE = tools.TRACEBACKHIDE
DEFAULT_OPERATIONS = ['Sum', 'Product', 'Power']
CACHING_OPS = True    # whether to do caching of simplify() or expand() -related ops.  # testing shows: faster when ON.
CACHING_CD  = True    # whether to do caching of _contains_deep_.                      # testing shows: faster when ON.
#CACHING_EQ  = False   # whether to do caching for equals                               # testing shows: faster when OFF.
#CACHING_EQ_LEN = 10  # max length of cache for a single object, for equals.

TIMEOUT_WARNINGS = True
TIMEOUT_DEFAULT = 5   # number of second before giving up on simplify or expand related ops.

# global value:
_timeout_warned = [False]

''' --------------------- Convenience --------------------- '''

def _simplyfied(x, **kw__None):
    '''apply _simple_simplify to x, or return x if x doesn't have '_simple_simplify' attribute.'''
    return apply(x, '_simple_simplify')

def is_constant(x):
    '''returns whether x is a constant. Checks:
    x not an AbstractOperation --> True
    x has is_constant() method --> x.is_constant()
    else --> False
    '''
    if not isinstance(x, AbstractOperation):
        return True
    return apply(x, 'is_constant', default=False)

def is_number(x):
    '''returns whether x is a number. Checks:
    x not an AbstractOperation --> True
    x has is_number() method --> x.is_number()
    else --> False
    '''
    if not isinstance(x, AbstractOperation):
        return True
    return apply(x, 'is_number', default=False)

def optypes_match(x, y):
    '''returns whether x.OPTYPE == y.OPTYPE.
    if both are None (which is the default if OPTYPE hasn't been set),
    returns whether type(x) == type(y).
    '''
    if not isinstance(x, type): x = type(x)
    if not isinstance(y, type): y = type(y)
    o_x = getattr(x, 'OPTYPE', None)
    o_y = getattr(y, 'OPTYPE', None)
    if (o_x is None) and (o_y is None):
        return x == y
    else:
        return o_x == o_y

"""  # to use CACHING_EQ = True, uncomment this section.
def equals(x, y, *more_args):
    '''return whether x == y.'''
    if len(more_args) > 0:
        return equals(x, y) and equals(y, more_args[0], *more_args[1:])
    # check caches.
    if CACHING_EQ:
        from_cache = _eq_cache_get(x, y) or _eq_cache_get(y, x)
        if from_cache is not None:
            return from_cache
    # do the actual work
    result = tools.equals(x, y)
    # put in cache
    if CACHING_EQ:
        _eq_cache_put(x, y, result)
        _eq_cache_put(y, x, result)
    return result

def _eq_cache_get(z, elem):
    '''get value for elem from equals cache of z.
    return True or False (the value for elem from cache) upon success;
    return None upon failure (elem wasn't in the cache).
    '''
    if hasattr(z, '_cache_eq'):
        try:
            return z._cache_eq[id(elem)]
        except KeyError:
            return None
    else:
        return None

def _eq_cache_put(z, elem, value):
    '''put elem:value in cache for equals for z.
    return False if z does not have _cache_eq and z._cache_eq cannot be created.
    return True upon success.
    '''
    if not hasattr(z, '_cache_eq'):
        try:
            setattr(z, '_cache_eq', collections.OrderedDict())
        except AttributeError:
            return False  # leave the function; we can't make a cache for this object.
    # << if we reach this line, z has _cache_eq attribute (because it already did, or we just made it.)
    z._cache_eq[id(elem)] = value
    if len(z._cache_eq) > CACHING_EQ_LEN:
        z._cache_eq.popitem(last=False)  # cache too long; forget the oldest entry.
    return True   # return True to indicate we put something.
"""

def is_opinstance(x, y):
    '''returns whether x is an instance of y, using 'AbstractOperation' rules.
    x: an object
    y: a type, or list or tuple of types.
        list or tuple --> returns any(is_opinstance(x, z) for z in y)

    The 'rules' are:
    neither x nor y has OPTYPE attr  -->  return type(x) == y
    x or y has OPTYPE attr           -->  return x.OPTYPE == y.OPTYPE
    '''
    if isinstance(y, (tuple, list)):
        return any(is_opinstance(x, z) for z in y)
    else:
        o_x = getattr(x, 'OPTYPE', None)
        o_y = getattr(y, 'OPTYPE', None)
        if (o_x is None) and (o_y is None):
            return type(x) == y
        else:
            return o_x == o_y

def _equals0(x):
    '''returns whether x equals 0.
    (This exists for efficiency purposes, only.)
    '''
    try:
        return x._equals0()
    except AttributeError:
        return equals(x, 0)

def _symbol_maker(Symbol_type):
    '''return a function which makes Symbols of type Symbol_type'''
    def Symbols(strings, *args__Symbol, **kw__Symbol):
        '''returns a tuple of Symbol objects.
        E.g. Symbols('x', 'y', **kw) == Symbol('x', **kw), Symbol('y', **kw)

        strings can be a list of strings, or a single string with whitespace separators.
        E.g. Symbols('x', 'y') == Symbols('x y')
        '''
        if isinstance(strings, str):
            strings = strings.split()
        return tuple((Symbol_type(s, *args__Symbol, **kw__Symbol) for s in strings))
    return Symbols

class SubstitutionInterface():
    '''helps manage substitutions.
    substitutions can be input via indexing of self.
    Can index via number, or AbstractOperation.

    Example:
        x = Symbol('x')
        y = Symbol('y')
        z = Symbol('z')
        obj = x + 7 * y / x + z
        subsi = SubstitutionInterface(obj, show_header=False)
        repr(subsi)
        >>> [0] Symbol('x') | None
            [1] Symbol('y') | None
            [2] Symbol('z') | None

        subsi[0] = 3
        repr(subsi)
        >>> [0] Symbol('x') | 3
            [1] Symbol('y') | None
            [2] Symbol('z') | None

        subsi[y] = np.arange(1000).reshape(10, 100)
        repr(subsi)
        >>> [0] Symbol('x') | 3
            [1] Symbol('y') | <class 'numpy.ndarray'>; NumArrayInfo(min=0, mean=499.5, max=999, shape=(10, 100))
            [2] Symbol('z') | None

        subsi.apply()      # equivalent to: obj.subs(*subsi)
        >>> 3 + 7 * np.arange(1000).reshape(10, 100) / 3 + z

    Note: long reprs (longer than _n_repr_max) of substitutions will be replaced with object.__repr__
    '''
    def __init__(self, abstract_operation_object, show_header=True, _debug=False, _n_repr_max=200):
        self.obj = abstract_operation_object
        self.show_header = show_header
        self._n_repr_max = _n_repr_max
        self._debug      = _debug
        self.subables, self.unsubables = self.obj.num_subs()
        self.subs = [None for _ in self.subables]

    def get_index(self, i_or_ao):
        '''gets index number for subable.
        i_or_ao: index or AbstractOperation object.
            if AbstractOperation, return index of first subable equal to this value.
            Otherwise just return i.
        '''
        if isinstance(i_or_ao, AbstractOperation):
            i = tools.find(self.subables, i_or_ao, default=None)
            if i is None:
                raise ValueError(f"value not found: {i_or_ao}")
            else:
                return i
        else:
            return i_or_ao

    def __setitem__(self, i_or_ao, value):
        self.subs[self.get_index(i_or_ao)] = value

    def __getitem__(self, i_or_ao):
        i = self.get_index(i_or_ao)
        return (self.subables[i], self.subs[i])

    def _internal_iter_(self):
        '''convenient iter for internal purposes, only.'''
        return zip(self.subables, self.subs)

    def __iter__(self):
        '''yields tuples of (subable, quant_to_sub) for all subs which have been entered.'''
        for subable, sub in self._internal_iter_():
            if sub is None:
                continue
            else:
                yield (subable, sub)

    def __repr__(self):
        if self._debug:
            return '{}; _debug=True. use __str__ to see interface.'.format(object.__repr__(self))
        else:
            return self.__str__()

    def __str__(self):
        if self.show_header:
            result = [('Welcome to SubstitutionInterface.\n'
                       'Enter the value of any quantity using the corresponding index.\n'
                       'For example: subsi = SubstitutionInterface(obj);\n'
                       '    subsi[0] = 7   # sets up subbing 7 for the quantity labeled [0].\n'
                       'To apply the substitutions, use subsi.apply().\n')]
        else:
            result = []
        for i, (subable, sub) in enumerate(self._internal_iter_()):
            reprsub = tools.array_info_str(sub)
            result.append('[{}] {} | {}'.format(i, repr(subable), reprsub))
        return '\n'.join(result)

    def apply(self):
        '''applies all the substitutions in self to the originally input object.
        (Non-destructive; generates a new object rather than smashing the old object)
        '''
        return self.obj.subs(*self)

    do      = property(lambda self: self.apply)  # alias
    doit    = property(lambda self: self.apply)  # alias
    applied = property(lambda self: self.apply)  # alias


''' --------------------- Operation Collection --------------------- '''

def _operation_collection(namespace=None, operations=DEFAULT_OPERATIONS):
    '''define OperationCollection for namespace (a dict, e.g. vars(module))
    if namespace is None, use current namespace (i.e. globals())

    operations: list of strs. default: ['Sum', 'Product', 'Power', 'Equation']
        the OperationCollection will know about these operations.
    '''
    if namespace is None:
        namespace = globals()

    def op_getter(opstr):
        '''returns a function which gets operation opstr.'''
        internal_opstr = '_' + opstr
        def get_op(self):
            if not hasattr(self, internal_opstr):
                setattr(self, internal_opstr, namespace[opstr])
            return getattr(self, internal_opstr)
        get_op.__name__ = 'get_{}'.format(opstr)
        get_op.__doc__ = 'gets operation {}'.format(opstr)
        return get_op

    def op_setter(opstr):
        '''returns a function which sets operation opstr.'''
        internal_opstr = '_' + opstr
        def set_op(self, value):
            setattr(self, internal_opstr, value)
        set_op.__name__ = 'set_{}'.format(opstr)
        set_op.__doc__ = 'sets operation {}'.format(opstr)
        return set_op

    class OperationCollection():
        '''tells which classes to use for basic operations.'''
        # make this class's "longform" (with module included) name be more intuitive.
        __module__ = namespace['__name__']

        # remember the list of operations defined here.
        OPERATIONS = operations

        def _update_maths(self, **kw):
            '''updates operations in self.

            kw are pairs of opstr, opvalue. E.g. 'Sum', Sum
            '''
            for opstr, opvalue in kw.items():
                self.opstr = opvalue

        def _update_maths_like(self, y):
            '''updates math from self to match math from y'''
            if not hasattr(y, 'OPERATIONS'):
                return None
            #assert hasattr(y, 'OPERATIONS'), "can't update maths using object of type {}".format(type(y))
            for opstr in y.OPERATIONS:
                setattr(self, opstr, getattr(y, opstr))
                if not opstr in self.OPERATIONS:
                    self.OPERATIONS.append(opstr)

    # here is where we actually tell OperationCollection about the operations:
    for opstr in operations:
        opprop = property(op_getter(opstr), op_setter(opstr))
        setattr(OperationCollection, opstr, opprop)

    return OperationCollection

OperationCollection = _operation_collection(locals(), DEFAULT_OPERATIONS)


def make_operations(modulename, inherit_from_namespace=None,
                    subclasses=[], operations=DEFAULT_OPERATIONS):
    '''return a namedtuple of operations.
    modulename: str
        name of module to associate with the created classes.
        suggestion: when calling this function from the namespace
            wherein the classes are going to end up, use modulename = __name__
    inherit_from_namespace: None or dict.
        lookup values of operations here. E.g. 'Sum' --> namespace['Sum'].
        None --> use local globals().
    subclasses: list of classes.
        add everything here as a subclass to the defined operations.
    operations: list of strings
        names of operations to define.
        default: ['Sum', 'Product', 'Power']

    Example:
        Sum, Product, Power = make_operations(__name__, vars(lz), subclasses=[MultipleOperation])
        << is equivalent to >>
        class Sum(lz.Sum, MultipleOperation): pass
        class Product(lz.Product, MultipleOperation): pass
        class Power(lz.Power, MultipleOperation): pass
    '''
    if inherit_from_namespace is None:
        inherit_from_namespace = globals()
    result = []
    for opstr in operations:
        Op = type(opstr, (inherit_from_namespace[opstr], *subclasses),
                  dict(__module__=modulename))
        result.append(Op)
    OperationsList = collections.namedtuple('OperationsList', operations)
    return OperationsList(*result)

def define_operations(define_in_namespace, inherit_from_namespace,
                      subclasses=[], operations=DEFAULT_OPERATIONS):
    '''define operations in define_in_namespace
    Example:
        Sum, Product, Power = make_operations(__name__, vars(lz), subclasses=[MultipleOperation])
        << is equivalent to >>
        define_operations(globals(), vars(lz), subclasses=[MultipleOperation])
    '''
    _name_ = define_in_namespace['__name__']
    ops = make_operations(_name_, inherit_from_namespace,
                          subclasses=subclasses, operations=operations)
    for op, opstr in zip(ops, operations):
        define_in_namespace[opstr] = op

''' --------------------- AbstractOperation --------------------- '''

def ao_arithmetic(f):
    '''wrap AbstractOperation base arithmetic functions.
    This wraps all functions in AbstractArithmetic which explicitly use Sum, Product, or Power.

    Right now, the wrapped function will do the following:
        - return f(self, b) or return NotImplemented for b in self.ARITHMETIC_FORBIDDEN_TYPES()
        - use f from b instead of self, if b is higher in the hierarchy (e.g. vectors > scalars)
    '''
    @functools.wraps(f)
    def f_ao_arithmetic_form(self, b):
        __trackebackhide__ = True
        if isinstance(b, self.ARITHMETIC_FORBIDDEN_TYPES()):
            return NotImplemented
        elif self.HIERARCHY < getattr(b, 'HIERARCHY', -100):
            op = f.__name__
            assert op.startswith('__') and op.endswith('__')
            rop = '__r' + op[2:-2] + '__'   # e.g. '__add__' --> '__radd__'
            return getattr(b, rop)(self)
        else:
            return f(self, b)
    return f_ao_arithmetic_form

class AbstractOperation(OperationCollection, metaclass=tools.CustomNewDoesntInit):
    OPTYPE = None   # tells the type of operation we are doing.
                    # e.g. 'Power' for Power, 'Sum' for Sum.
                    # this is sometimes used for "type"-matching;
                    # see e.g. AssociativeOperation._flatten
    HIERARCHY = -1

    def ARITHMETIC_FORBIDDEN_TYPES(self):
        '''returns types for which AbstractOperation should return NotImplemented during arithmetic.'''
        return tuple(getattr(self, attr) for attr in ('Equation', 'EquationSystem') if hasattr(self, attr))

    @property
    def type(self):
        return type(self)

    def __new__(cls, *terms, **kwargs):
        '''construct new AbstractOperation (usually), but first check if it's appropriate.
        if terms is empty, return self.IDENTITY (or []) instead.
        if terms has length 1, return terms[0] instead.
        '''
        __tracebackhide__ = True
        if len(terms) == 0:
            return getattr(cls, 'IDENTITY', [])
        elif len(terms) == 1:
            return terms[0]
        else:
            instance = object.__new__(cls)
            instance.__init__(*terms, **kwargs)
            return instance

    def _new(self, *terms, **kw):
        '''create new instance of self, possibly utilizing properties of self to do so.
        (This is mainly provided so that subclasses can overwrite this method.)
        The method here just returns type(self)(*terms, **kw)
        '''
        result = type(self)(*terms, **kw)
        self._transmit_genes(result)
        return result

    def copy(self):
        '''tries to make a copy of self.'''
        return self._new(*self)

    def _transmit_genes(self, x):
        '''if self has a _genes property, for gene in self._genes, set x.gene = self.gene.
        (If type(x) != type(self), don't transmit genes.)

        The idea is, genes are properties which should will be passed down anytime self._new is used,
            any number of times. E.g. self._new(...)._new(...) will have the same genes as self.

        returns whether we transmitted any genes.
        '''
        if (type(self) == type(x)) and hasattr(self, '_genes'):
            x._genes = self._genes
            for gene in self._genes:
                self_gene_value = getattr(self, gene)
                setattr(x, gene, self_gene_value)
            return len(self._genes) > 0
        return False

    def set_gene(self, gene_name, *value_or_blank):
        '''put gene_name as a gene of self. It will be transmitted whenever self._new() is called.
        *value_or_blank: 0 args or 1 arg.
            0 --> ignore
            1 --> self.gene_name will be assigned to the value provided.

        Example:
            x.set_gene('_secret', 7)     # x._secret = 7; and tells _new to pass on _secret.
            y = x._new()
            y._secret
            >>> 7
        '''
        vob = value_or_blank
        assert len(vob) in (0, 1), 'self.set_gene() expected 1 or 2 arguments but got {}'.format(1+len(vob))
        genes = getattr(self, '_genes', set())
        genes.add(gene_name)
        setattr(self, '_genes', genes)
        if len(vob) == 1:
            value = vob[0]
            setattr(self, gene_name, value)

    ## BASIC ARITHMETIC ##
    @ao_arithmetic
    def __add__(self, b):
        '''return self + b'''
        return _simplyfied(self.Sum(self, b))

    @ao_arithmetic
    def __mul__(self, b):
        '''return self * b'''
        return _simplyfied(self.Product(self, b))

    @ao_arithmetic
    def __pow__(self, b):
        '''return self ** b'''
        return _simplyfied(self.Power(self, b))

    ## "DERIVED" ARITHMETIC ##
    def __sub__(self, b):
        '''return self - b'''
        return _simplyfied(self + (-1 * b))

    def __truediv__(self, b):
        '''return self / b'''
        return _simplyfied(self * b**(-1))

    ## "UNARY" ARITHMETIC ##
    def __pos__(self):
        '''return +self'''
        return self

    def __neg__(self):
        '''return -self'''
        return -1 * self

    ## "REVERSE" ARITHMETIC ##
    @ao_arithmetic
    def __radd__(self, b):
        '''return b + self'''
        return _simplyfied(self.Sum(b, self))

    @ao_arithmetic
    def __rmul__(self, b):
        '''return b * self'''
        return _simplyfied(self.Product(b, self))

    def __rpow__(self, b):
        '''return b ** self'''
        return _simplyfied(self.Power(b, self))

    def __rsub__(self, b):
        '''return b - self'''
        return _simplyfied(b + (-1 * self))

    def __rtruediv__(self, b):
        '''return b / self'''
        return _simplyfied(b * (self ** (-1)))

    ## NUMPY INTERACTIONS ##
    __array_ufunc__ = None   # tell numpy to let AbstractOperation handle interactions.

    ## SUBSTITUTIONS ##
    def __call__(self, *args, subs=[], subscripts=[], **kw):
        '''substitutions should be tuples of (key, new_value).
        if there is one arg and self has only one symbol, substitutes that value.
            if the value is non-abstract, also evaluates numbers.
        '''
        # "evaluate" case (one arg and one symbol, only)
        if len(args) == 1:
            syms = self.get_symbols()
            if len(syms)==1:
                sym, arg = syms[0], args[0]
                result = self(subs=[(sym, arg)])
                if is_number(arg):
                    result = result.apply('evaluate_numbers', evaluate_constants=True)
                return result
            else:
                raise TypeError('__call__ with 1 arg expects exactly 1 symbol but self has symbols: {}'.format(syms))
        # bookkeeping
        substitutions = subs
        orig_self = self
        # handle basic substitutions
        for s in substitutions:
            if s[0] == self:
                self = s[1]
        # handle subscripts
        if (len(subscripts) > 0) and hasattr(self, 'subscripts'):
            ss_old, ss_new = zip(*subscripts)
            new_subscripts = []
            for t in self.subscripts:
                for s_old, s_new in zip(ss_old, ss_new):
                    if t == s_old:
                        new_subscripts.append(s_new)
                        break
                else: # didn't break
                    new_subscripts.append(t)
            if new_subscripts != self.subscripts:
                self = self._new(subscripts=new_subscripts)
        # bookkeeping - if we changed self, return result now without going to terms of self.
        if self is not orig_self:
            return self
        # handle terms inside self
        try:
            self_terms = list(iter(self))
        except TypeError:
            return self
        else:
            if len(self_terms) == 0:
                return self
            subbed_any = False
            for i, term in enumerate(self_terms):
                if callable(term):
                    orig_term = term
                    new_term  = term(*args, subs=substitutions, subscripts=subscripts, **kw)
                    self_terms[i] = new_term
                    if (not subbed_any) and (new_term is not orig_term):
                        subbed_any = True
            if subbed_any:
                return self._new(*self_terms)
            else:
                return self

    def substitute(self, *substitutions, subscripts=[], **kw):
        '''substitutions should be tuples of (key, new_value)'''
        return self(subs=substitutions, subscripts=subscripts, **kw)

    def subscript_substitute(self, *subscripts, substitutions=[], **kw):
        return self(subs=substitutions, subscripts=subscripts, **kw)

    def subscript_swap(self, old, new):
        '''swap a single subscript (everywhere it appears in self) from old to new.'''
        return self(subscripts=[(old, new)])

    def substitute_everywhere(self, *substitutions, **kw):
        '''make these substitutions (of (key, new_value)) everywhere; in AND out of subscripts.'''
        return self(subs=substitutions, subscripts=substitutions, **kw)

    subs     = property(lambda self: self.substitute)  # alias
    replace  = property(lambda self: self.substitute)  # alias
    subsubs  = property(lambda self: self.subscript_substitute)  # alias
    subssubs = property(lambda self: self.subscript_substitute)  # alias
    ss       = property(lambda self: self.subscript_swap)  # alias
    subsall     = property(lambda self: self.substitute_everywhere)  # alias
    replace_all = property(lambda self: self.substitute_everywhere)  # alias


    def num_subs(self):
        '''returns (subables, unsubables).

        subables:
            list of terms into which a number can be substituted, directly
            E.g. scalar symbols, dot products
        unsubables:
            list of non-numeric terms into which a number cannot be substituted
            E.g. vector symbols, cross products

        Ignores "number" object (e.g. AbstractConstant such as ComplexRational).

        (Subclasses which can or cannot be subbed should overwrite this function appropriately.)
        '''
        subables   = []
        unsubables = []
        for term in self:
            subs, unsubs = apply(term, 'num_subs', default=([],[]))
            subables   += subs
            unsubables += unsubs
        unique_subs   = list(tools.Set(subables))
        unique_unsubs = list(tools.Set(unsubables))
        return (unique_subs, unique_unsubs)

    def substitution_interface(self, *args__subsi, **kw__subsi):
        '''returns an interface for making it easier to do substitutions into self.'''
        return SubstitutionInterface(self, *args__subsi, **kw__subsi)

    subs_interface      = property(lambda self: self.substitution_interface)  # alias
    substitution_helper = property(lambda self: self.substitution_interface)  # alias
    subs_helper         = property(lambda self: self.substitution_interface)  # alias
    allsub              = property(lambda self: self.substitution_interface)  # alias

    ## TERMS ##
    def __iter__(self):
        raise NotImplementedError("{}.__iter__".format(type(self)))

    def __len__(self):
        return len(list(iter(self)))

    def __getitem__(self, i_or_ao):
        '''returns self[i_or_ao]
        if i_or_ao is an AbstractOperation, get index using self.get_index.
        otherwise, use index=i_or_ao.
        To override this behavior for subclass,
        consider replacing _get_index_equality_check or get_index instead.
        '''
        i = self.get_index(i_or_ao)
        if isinstance(i, slice):
           return self._new(*self.__list__()[i])
        else:
            return self.__list__()[i]

    def _get_index_equality_check(self, item_from_self, abstract_op):
        '''tells whether item_from_self equals abstract_op for the purposes of indexing (see self.get_index).
        Here, this method just returns whether item_from_self==abstract_op.

        But, subclasses may wish to override it.
        For example, EquationSystem will override it
            so that it instead checks if the LHS of the equation equals abract_op,
            i.e. item_from_self[0] == abtract_op.
        '''
        return item_from_self == abstract_op

    def get_index(self, i_or_ao):
        '''gets index number corresponding to index or abstract operation.
        i_or_ao: index or AbstractOperation object.
            if AbstractOperation, return index of first element in self equal to this value.
            Otherwise just return i.

        for equality checking, use self._get_index_equality_check(item_from_self, ao).
        By default, that method just returns (item_from_self == ao).
        But subclasses may override it.
        It is preferred to override that _get_index_equality_check, rather than get_index, if possible.
        '''
        if isinstance(i_or_ao, AbstractOperation):
            i = tools.find(self, i_or_ao, default=None, equals=self._get_index_equality_check)
            if i is None:
                raise ValueError(f"value not found: {i_or_ao}")
            else:
                return i
        else:
            return i_or_ao

    def __contains__(self, b):
        '''returns whether b is equal to one of the terms of self.'''
        #return b in list(iter(self))  # < this doesn't work when b is a numpy array;
        #   it gives "ValueError: The truth value of an array with more than one element is ambiguous."
        return (tools.find(self, b, default=None, equals=equals) is not None)

    def _contains_deep_(self, b):
        '''returns whether b is contained (via _contains_deep_) of self.
        i.e. (b == self) or (b in self) or (term._contains_deep_(b) for term in self)

        if CACHING_CD, check cache before checking _contains_deep_ for each term in self.
        '''
        # do the simplest checks first:
        if self == b:
            return True
        if b in self:
            return True
        # check cache:
        caching = CACHING_CD
        if caching:
            if not hasattr(self, '_cd_cache'):
                self._cd_cache = []
            i = tools.find(self._cd_cache, b, default=None,
                         equals=lambda cachei, x: (cachei[0] is x))
                    # note: use 'is', not '=='. '==' is slower than 'is'; with '==' the cache checking is too slow.
            if i is not None:
                return self._cd_cache[i][1]
        # check _contains_deep_ for each term in self.
        for term in self:
            if apply(term, '_contains_deep_', b, default=False):
                result = True  # found b in at least one term, so the answer is True.
                break
        else: # didn't break
            result = False
        # cache, then return result.
        if caching:
            self._cd_cache.append((b, result))
        return result

    def _contains_deep_subscript_(self, s):
        '''returns whether self contains s in any subscripts.'''
        # do the simplest checks first:
        if s in getattr(self, 'subscripts', []):
            return True
        # check cache:
        caching = CACHING_CD
        if caching:
            if not hasattr(self, '_cds_cache'):
                self._cds_cache = []
            i = tools.find(self._cds_cache, s, default=None,
                         equals=lambda cachei, x: (cachei[0] is x))
                    # note: use 'is', not '=='. '==' is slower than 'is'; with '==' the cache checking is too slow.
            if i is not None:
                return self._cds_cache[i][1]
        # check _contains_deep_subscript_ for each term in self.
        for term in self:
            if apply(term, '_contains_deep_subscript_', s, default=False):
                result = True  # found s in subscript of at least one term, so the answer is True.
                break
        else: # didn't break
            result = False
        # cache, then return result.
        if caching:
            self._cds_cache.append((s, result))
        return result

    def _contains_deep_anywhere_(self, x):
        '''returns whether self contains x anywhere, including possibly as a subscript.'''
        return self._contains_deep_subscript_(x) or self._contains_deep_(x)

    def _contains_deep_inside_(self, x, inside):
        '''returns whether x is contained (via _contains_deep_inside_) of self,
        inside of an object which is_opinstance of inside.
        E.g. ((u x B).k + 7)._contains_deep_inside_(u, ...)
            inside=CrossProduct --> True
            inside=DotProduct   --> True
            inside=Power        --> False
            inside=Equation     --> False
        Edge case note:
            if x == self, return False, even if self is_opinstance of inside.
        '''
        if is_opinstance(self, inside):
            return self._contains_deep_(x)
        for term in self:
            if apply(term, '_contains_deep_inside_', x, inside, default=False):
                return True
        return False

    @property
    def layers(self):
        '''return number of layers in self.'''
        # caching
        if hasattr(self, '_layers'):
            return self._layers
        # result
        try:
            L = len(self)
        except TypeError:   # self has no length
            return 1
        if L == 0:
            return 1
        result = 1 + max(getattr(x, 'layers', 0) for x in self)
        # caching
        self._layers = result
        # result
        return result

    def get_symbols(self):
        '''returns list of Symbol objects in self (or in terms of self)'''
        symbols = [s for term in self for s in apply(term, 'get_symbols', default=[])]
        unique_symbols = list(tools.Set(symbols))
        return unique_symbols

    def get_symbols_with(self, func):
        '''return list of Symbol objects x in self for which bool(func(x)) evaluates to True.'''
        return [s for s in self.get_symbols() if func(s)]

    def get_symbols_in(self, func):
        '''return list of Symbols objects in parts of self with func(part)==True if func(self)==True, else [].'''
        if func(self):
            return list(tools.Set([s for term in self for s in apply(term, 'get_symbols_in', func, default=[])]))
        else:
            return []

    def split_numeric_component(self):
        '''returns (numeric component, non-numeric component) of self.'''
        raise NotImplementedError("{}.split_numeric_component".format(type(self)))

    def is_constant(self):
        '''returns whether self is constant.'''
        return all(is_constant(t) for t in self)

    ## REPR ##
    def __repr__(self, **kw):
        fmt = '{}({{}})'.format(type(self).__name__)  # e.g. 'Product({})'
        return fmt.format(', '.join(self._repr_contents(**kw)))

    def _repr_contents(self, **kw):
        return [_repr(t, **kw) for t in self]

    def structure(self, layers=10, tab='  ', _layer=0, i=None):
        '''returns string for structure of self, cutting off at layer < layers.'''
        istr = '' if i is None else ', i={}'.format(i)
        result = '{}(L{:d}{}) {}'.format(tab * _layer, _layer, istr, type(self).__name__)
        self_layers = self.layers
        if self_layers > 1:
            result += ' with len=({:d}), and ({:d}) internal layers'.format(len(self), self.layers, self_layers - 1)
        if _layer < layers:
            internals = [apply(t, 'structure', layers=layers, tab=tab, _layer=_layer+1, i=i, default=None) for i, t in enumerate(self)]
            internals = [t for t in internals if t is not None]
            if len(internals) > 0:
                result += '\n' + '\n'.join(internals)
        return result

    def print_structure(self, *args, **kw):
        print(self.structure(*args, **kw))

    structure_print = property(lambda self: self.print_structure)  # alias

    @property
    def printsize(self):
        return len(str(self))

    ## CONVENIENCE ##
    def view(self, *args__str, **kw__str):
        '''does view(self)'''
        return view(self, *args__str, **kw__str)

    def _ipython_display_(self):
        '''this is the method jupyter notebook calls to figure out how to show self.'''
        return self.view()

    def simplified(self, loop=False, LMAX=50, timeout=TIMEOUT_DEFAULT, print_freq=2, **kw):
        '''returns self.expand().simplify().
        if loop, repeat until self stops changing or LMAX iterations have been performed.
        '''
        updater = tools.ProgressUpdatePrinter(print_freq=print_freq)
        tstart = time.time()
        def time_remaining():
            return None if timeout is None else (timeout - (time.time() - tstart))
        orig_self = self
        self = apply(self, 'expand', timeout=time_remaining(), **kw)
        self = apply(self, 'simplify', timeout=time_remaining(), **kw)
        if loop:
            i = 1
            while (i < LMAX) and (not equals(self, orig_self)):
                updater.print('Looping during simplified. At iteration i={}'.format(i), print_time=True)
                i += 1
                orig_self = self
                self = apply(self, 'expand', timeout=time_remaining(), **kw)
                self = apply(self, 'simplify', timeout=time_remaining(), **kw)
            if i >= LMAX:
                warnmsg_max_iterations = ('stopped simplified() for object <{}> after LMAX(={}) iterations;'
                                          'result might be not in simplest form.').format(hex(id(self)), LMAX)
                warnings.warn(warnmsg_max_iterations)
        updater.finalize(min_time=print_freq*2)
        return self

    def complexity(self):
        '''returns a number which tells complexity of self. larger --> more complicated.
        (Presently, just does len(repr(self)))
        '''
        return len(repr(self))


''' ---------------- Simplifiable ---------------- '''

def _combined_ops(x, bases, attr):
    '''combines lists (x.attr and base.attr for base in bases),
    in reverse order, removing duplicates.

    The idea is, for example:
        if we defined _simplify_1 in scalars.Sum, and _simplify_2 in vectors.Sum,
        since vectors.Sum is a subclass of scalars.Sum, vectors.Sum should have
        _simplify_1 and _simplify_2 as _SIMPLIFY_OPS; not JUST _simplify_2.
    '''
    result = tools.Set([])
    for c in itertools.chain(bases[::-1], [x]):
        result += getattr(c, attr, [])
    return result

def _op2op(_opstr, lenient=False):
    '''asserts _opstr starts with '_' and returns _opstr without the leading underscore.'''
    if lenient and _opstr[0] != '_':
        return _opstr
    else:
        assert _opstr[0] == '_', "_opstr {} missing leading '_'. ".format(repr(_opstr)) + \
                        "(_opstr probably defined in _SIMPLIFY_OPS or _EXPAND_OPS.)"
        return _opstr[1:]

def _op_maker(_opstr):
    '''makes a function which does _opstr at all layers of self.
    TODO: make op a class with __call__ and __repr__ instead, to make prettier repr.
    '''
    def simplifying_or_expanding_all_layers_op(self, *args, **kw):
        return self._f_all_layers(_f=_opstr, *args, **kw)
    op = simplifying_or_expanding_all_layers_op
    op.__doc__ = 'does {} at all layers of self'.format(_opstr)
    return op

class SimplifiableMaker(tools.CustomNewDoesntInit):
    '''metaclass for SimplifiableOperation.

    when class with metaclass=SimplifiableMaker is created, add bound methods to that class, for:
        - each op in _SIMPLIFY_OPS  (e.g. ['_flatten', '_collect'])
        - each op in _EXPAND_OPS

    also, combine lists of _OPS from bases and the created class.
    '''
    def __new__(cls, clsname, bases, attrs):
        x = super(SimplifiableMaker, cls).__new__(cls, clsname, bases, attrs)
        x._SIMPLIFY_OPS = _combined_ops(x, bases, '_SIMPLIFY_OPS')
        x._EXPAND_OPS = _combined_ops(x, bases, '_EXPAND_OPS')
        x._DEFAULT_FALSE_OPS = _combined_ops(x, bases, '_DEFAULT_FALSE_OPS')
        for _opstr in x._SIMPLIFY_OPS + x._EXPAND_OPS:
            op = _op_maker(_opstr)
            opstr = _op2op(_opstr)
            op.__name__ = opstr
            setattr(x, opstr, op)
        return x


class SimplifiableOperation(AbstractOperation, metaclass=SimplifiableMaker):

    ## SIMPLIFICATION PROPERTIES ##
    #    (to maintain compatibility with SimplifiableMaker, don't use @property.)
    # The ops to use in _simplify. List of strings with leading underscore.
    _SIMPLIFY_OPS = []
    # The ops to use in _expand. List of strings with leading underscore.
    _EXPAND_OPS = []
    # The ops to not do, by default. Set of strings with leading underscore.
    _DEFAULT_FALSE_OPS = {}
    # Whether to do caching. None --> use abstract_operations.CACHING_OPS
    _CACHING = None
    # TODO: put a property which tells the order in which to apply the OPS...

    ## APPLY FUNC TO ALL LAYERS OF SELF ##
    def _f_all_layers(self, _f, *args__f, timeout=TIMEOUT_DEFAULT, _top=True, bottom_up=False,
                      _firsttime=True, max_depth=None, **kw__f):
        '''Applies _f to all layers of self.
        does top layer first, then works down to inner-most layer.
        repeats on current layer until _f stops changing self.

        does caching too, unless self._CACHING is False.
            (if self._CACHING is None, check abstract_operations.CACHING_OPS)

        timeout after timeout seconds have passed.
        '''
        __tracebackhide__ = TRACEBACKHIDE
        if max_depth == 0:
            return self
        if _top:
            _timeout_warned[0] = False
        if timeout is not None and timeout <= 0:
            if TIMEOUT_WARNINGS and not _timeout_warned[0]:
                warnings.warn('Timed out during {} for self with id=({})'.format(_f, hex(id(self))))
                _timeout_warned[0] = True
            return self 
        tstart = time.time()
        def time_remaining():
            return None if timeout is None else (timeout - (time.time() - tstart))
        # check for caching
        caching = False
        # check cache for existing result; return if found.
        caching = getattr(self, '_CACHING', None)
        if caching is None:
            caching = CACHING_OPS
        if caching:
            kw__cache = dict(**kw__f, max_depth=max_depth, bottom_up=bottom_up)
            if not hasattr(self, '_ops_cache'):
                self._ops_cache = collections.defaultdict(list)
            _ops_cache = self._ops_cache
            contents   = _ops_cache.get(_f, [])
            for (args, kwargs), result in contents:
                if list_equals(args, args__f) and dict_equals(kwargs, kw__cache):
                    return result
        # kw management:
        kw_internal = dict(_top=False, bottom_up=bottom_up, max_depth=None if max_depth is None else max_depth - 1)
        # apply at top layer
        if bottom_up and _firsttime:
            new_self = self
        else:
            prev_self = None
            new_self = self
            while prev_self is not new_self:
                prev_self = new_self
                new_self = apply(new_self, _f, *args__f, timeout=time_remaining(), **kw_internal, **kw__f)
        # if we get a non-AbstractOperation, stop and return result now.
        if not isinstance(new_self, AbstractOperation):
            result = new_self
        else:  # otherwise, apply to terms (one layer down)
            terms = []
            changed_any_terms = False
            for t in new_self:
                t_prev = t
                t_new = apply(t, '_f_all_layers', _f=_f, *args__f, timeout=time_remaining(), **kw_internal, **kw__f)
                terms.append(t_new)
                if t_new is not t_prev:
                    changed_any_terms = True
            if changed_any_terms:
                result = new_self._new(*terms)
            else:
                result = new_self  # return new_self, exactly, to indicate no terms were changed.
            # if we changed anything, call at top layer again.
            if changed_any_terms or (bottom_up and _firsttime):
                result = apply(result, '_f_all_layers', _f=_f, *args__f, timeout=time_remaining(), **kw_internal, _firsttime=False, **kw__f)
        # cache then return result
        if caching:
            _ops_cache[_f].append( ((args__f, kw__cache), result) )
        return result

    apply_internal = internal_apply = _f_all_layers   # aliases

    def apply(self, f, *args__f, **kw__f):
        '''Applies f to all layers of self.
        same as _f_all_layers, except that we first impose f starts with '_'.
        If it doesn't start with '_' yet, put a '_' in front.
        E.g. self.apply('distribute') == self.apply('_distribute')
        '''
        _f = '_' + f if f[0]!='_' else f
        return self._f_all_layers(_f, *args__f, **kw__f)


    ## SIMPLIFY (USING ALL SIMPLICATION HELPER FUNCTIONS) ##
    def _simplify(self, only=None, debug=False,
                  stop_if=lambda x: not isinstance(x, AbstractOperation),
                  _top=True, timeout=TIMEOUT_DEFAULT,
                  **kwargs):
        '''simplify this layer of self, using all _SIMPLIFY_OPS.
        (skips ops in _DEFAULT_FALSE_OPS, by default.)
        ops can be turned on/off via opname=True/False in kwargs.
        E.g. to turn off _collect, do _simplify(..., collect=False, ...)

        only: None (default) or list of strings (operation names)
            only do the operations listed here. (insensitive to leading underscore.)
        if stop_if(self) (or stop_if(result of doing a simplification)),
            stop immediately and return the result.
            Default is to stop if we get to something that is not an AbstractOperation.
        '''
        # debug options
        MSG_START = ' -> starting _simplify with <- '
        def print_if_debug(msg, append=' (<id={i}>) | self={s}'):
            if debug:
                msg = '{{:{}s}}'.format(len(MSG_START)).format(msg)  # e.g. '{:30s}'
                s = repr(self) if debug > 1 else str(self)
                print((msg + append).format(s=s, i=hex(id(self))))
        print_if_debug(MSG_START)
        # timeout stuff
        tstart = time.time()
        def time_remaining():
            return None if timeout is None else (timeout - (time.time() - tstart))
        # figure out which ops to do. (figure this out beforehand, since the loop might change self.)
        if only is not None:
            _only = ['_' + _op2op(_opstr, lenient=True) for _opstr in only]  # 'be insensitive to leading underscore'
        ops_to_do = []
        for _opstr in self._SIMPLIFY_OPS:
            opstr = _op2op(_opstr)
            try:
                do_op = kwargs[opstr]
            except KeyError:
                do_op = _opstr not in self._DEFAULT_FALSE_OPS
            if do_op:
                if (only is None) or (_opstr in _only):
                    ops_to_do.append(_opstr)
        # apply simplifying ops
        for _opstr in ops_to_do:
            # >> actually do op here <<
            self = apply(self, _opstr, _top=False, timeout=time_remaining(), **kwargs)
            print_if_debug('after {}'.format(_opstr))
            # if stop_if, stop.
            if stop_if(self):
                break
        return self

    def simplify(self, _loop=True, **kw__simplify):
        '''simplify all layers of self. if _loop, keep looping up and down until there are no changes.'''
        result = self._f_all_layers(_f='_simplify', _top=False, **kw__simplify)
        if (result is self) or (not _loop):
            return result
        # if _loop, keep looping until there are no changes.
        rprev = None
        r = result
        while r is not rprev:
            rprev = r
            r = apply(r, '_f_all_layers', _f='_simplify', _top=False, **kw__simplify)
        return r

    ## EXPAND (USING ALL EXPANSION HELPER FUNCTIONS) ##
    def _expand(self, only=None, debug=False,
                stop_if=lambda x: not isinstance(x, AbstractOperation),
                _top=True, timeout=TIMEOUT_DEFAULT,
                **kwargs):
        '''expand this layer of self.
        TODO: encapsulate instead of copy-pasting from _simplify.

        only: None (default) or list of strings (operation names)
            only do the operations listed here. (insensitive to leading underscore.)
        if stop_if(self) (or stop_if(result of doing a simplification)),
            stop immediately and return the result.
            Default is to stop if we get to something that is not an AbstractOperation.
        '''
        MSG_START = ' <-- starting _expand with --> '
        def print_if_debug(msg, append=' (<id={i}>) | self={s}'):
            if debug:
                msg = '{{:{}s}}'.format(len(MSG_START)).format(msg)  # e.g. '{:30s}'
                s = repr(self) if debug > 1 else str(self)
                print((msg + append).format(s=s, i=hex(id(self))))
        print_if_debug(MSG_START)
        # timeout stuff
        tstart = time.time()
        def time_remaining():
            return None if timeout is None else (timeout - (time.time() - tstart))
        # figure out which ops to do. (figure this out beforehand, since the loop might change self.)
        if only is not None:
            _only = ['_' + _op2op(_opstr, lenient=True) for _opstr in only]  # 'be insensitive to leading underscore'
        ops_to_do = []
        for _opstr in self._EXPAND_OPS:
            opstr = _op2op(_opstr)
            try:
                do_op = kwargs[opstr]
            except KeyError:
                do_op = _opstr not in self._DEFAULT_FALSE_OPS
            if do_op:
                if (only is None) or (_opstr in _only):
                    ops_to_do.append(_opstr)
        # apply expanding ops
        for _opstr in ops_to_do:
            # >> actually do op here <<
            self = apply(self, _opstr, _top=False, timeout=time_remaining(), **kwargs)
            print_if_debug('after {}'.format(_opstr))
            # if stop_if, stop.
            if stop_if(self):
                break
        return self

    def expand(self, **kw__expand):
        '''expands at all layers of self.'''
        return self._f_all_layers(_f='_expand', _top=False, **kw__expand)

    ## EXPAND AND SIMPLIFY ##
    def _anneal(self, **kw):
        '''expand and simplify this layer of self.
        (doesn't collect when simplifying.)
        '''
        self = apply(self, '_expand', **kw)
        self = apply(self, '_simplify', collect=False, **kw)
        return self

    def anneal(self, **kw):
        '''expand then simplify at each layer, for all layers of self.'''
        return self._f_all_layers(_f='_anneal', _top=False, **kw)

    ## CONVENIENCE ##
    def _simple_simplify(self):
        '''simplifies top layer of self using _flatten then _simplify_id.'''
        self = apply(self, '_flatten')
        self = apply(self, '_simplify_id')
        self = apply(self, '_evaluate_numbers', evaluate_abstract_only=True)
        return self

    def evaluate_constants(self):
        '''evaluates "abstract" constants, at all layers of self.
        E.g. converts Rational objects to floats.

        equivalent to self.apply('evaluate_numbers', evaluate_constants=True, evaluate_abstract_only=True)
        '''
        return self.apply('evaluate_numbers', evaluate_constants=True, evaluate_abstract_only=True)

''' ---------------- Multiple, Commutative, Associative, Binary ---------------- '''

class MultipleOperation(SimplifiableOperation):
    def __init__(self, *terms):
        self.terms = terms

    def __eq__(self, b):
        if b is self:
            return True
        if not is_opinstance(self, type(b)):
            return False
        return list_equals(self.terms, b.terms)

    ## TERMS ##
    def __iter__(self):
        return iter(self.terms)

    def __list__(self):
        return self.terms

    def __len__(self):
        return len(self.terms)

    ## SIMPLIFICATIONS ##
    # all simplification operations are handled by SimplifiableOperation.


class CommutativeOperation(MultipleOperation):
    def __eq__(self, b):
        if b is self:
            return True
        if not is_opinstance(self, type(b)):
            return False
        return tools.equal_sets(self, b) #tools.Set(self) == tools.Set(b)

    ## SIMPLIFYING ##
    _SIMPLIFY_OPS = ['_simplify_id', '_evaluate_numbers']

    def _simplify_id(self, **kw__None):
        '''removes any identity operations at top level of self.'''
        keep_terms = [term for term in self if not equals(term, self.IDENTITY)]
        if len(keep_terms) == 1 and type(keep_terms[0]) == type(self):
            return keep_terms[0]
        elif len(keep_terms) == len(self):
            return self   # return self, exactly, to help indicate we made no changes.
        else:
            return self._new(*keep_terms)

    def _evaluate_numbers(self, evaluate_abstract_only=False, **kw):
        '''evaluates non-AbstractOperation pieces by doing self.OPERATION on terms which are numbers.
        Uses Commutative property to provide a result in which the numbers are combined as much as possible.

        if evaluate_abstract_only, only evaluate numbers which are AbstractOperations.
        '''
        IDENTITY = getattr(self, 'IDENTITY', None)
        OPERATION = getattr(self, 'OPERATION', None)
        if OPERATION is None or IDENTITY is None:
            return self
        # group terms
        n0_terms = []   # numbers which are NOT abstract operations
        nA_terms = []   # numbers which ARE abstract operations
        ao_terms = []   # abstract operations which are not numbers
        for term in self:
            if is_number(term):
                if isinstance(term, AbstractOperation):
                    nA_terms.append(term)
                else:
                    if evaluate_abstract_only:
                        ao_terms.append(term)
                    else:
                        n0_terms.append(term)
            else:
                ao_terms.append(term)
        N_nums = len(n0_terms) + len(nA_terms)
        if N_nums <= 1:   # at most 1 number which could be evaluated --> nothing to do.
            return self   # return self, exactly, to help indicate we made no changes.
        # evaluate numbers which are NOT abstract operations.
        n0 = IDENTITY
        for num in n0_terms:
            n0 = OPERATION(n0, num)
        # evaluate numbers which ARE abstract operations.
        nA = IDENTITY
        for num in nA_terms:
            num = apply(num, '_evaluate_numbers', evaluate_abstract_only=evaluate_abstract_only, **kw)
            nA = OPERATION(nA, num)
        # try to put n0 and nA together (might fail, e.g. if doing float times ComplexRational)
        try:
            n = [OPERATION(n0, nA)]
        except TypeError:
            if N_nums == 2:  # we only had n0 and nA to start; we didn't simplify anything.
                return self  # return self, exactly, to help indicate we made no changes.
            # else
            n = [n0, nA]
        result = self._new(*n, *ao_terms)
        result = apply(apply(result, '_flatten', **kw), '_simplify_id', **kw)
        return result


class AssociativeOperation(MultipleOperation):

    def split_numeric_component(self):
        '''returns (numeric component, non-numeric component) of self.'''
        n_terms = []
        a_terms = []
        for t in self:
            if is_number(t):
                n_terms.append(t)
            else:
                a_terms.append(t)
        n_new = self._new(*n_terms)
        a_new = self._new(*a_terms)
        return (n_new, a_new)

    ## SIMPLIFYING
    _SIMPLIFY_OPS = ['_flatten']

    def _flatten(self, **kw__None):
        '''flattens self. E.g. Product(x, Product(y, z)) --> Product(x, y, z).
        (Assumes associative operation.)'''
        terms = []
        flattened_any = False
        returntype = type(self)
        for t in self.terms:
            if is_opinstance(t, type(self)):
                if issubclass(type(t), returntype):
                    returntype = type(t)   # typecast result to type of t.
                terms += t._flatten().terms
                flattened_any = True
            else:
                terms += [t]
        if not flattened_any:
            return self   # return self, exactly, to help indicate we made no changes.
        return returntype(*terms)


# subclasses should inherit in this order: Associative, Commutative, Multiple.

class BinaryOperation(MultipleOperation):
    def __init__(self, t1, t2):
        self.t1 = t1
        self.t2 = t2

    @property
    def terms(self):
        return [self.t1, self.t2]

    @terms.setter
    def terms(self, values):
        assert len(values) == 2
        self.t1 = values[0]
        self.t2 = values[1]


''' --------------------- Symbol --------------------- '''

class Symbol(SimplifiableOperation):
    OPTYPE = 'Symbol'

    __new__ = object.__new__   # overwrite the __new__ from AbstractOperation.

    def __init__(self, s, subscripts=[], constant=False):
        self.s = s
        self.subscripts = subscripts
        self.constant = constant   # whether to treat this symbol as a constant.

    def _init_properties(self):
        '''returns dict for initializing another symbol like self.'''
        kw = dict()
        kw['subscripts'] = self.subscripts
        kw['constant'] = self.constant
        return kw

    def _new(self, *args, **kw):
        '''makes a new symbol with same s as self,
        and same properties as self, except any which are overwritten by kw.
        '''
        init_props = self._init_properties()
        init_props.update(kw)
        result = type(self)(self.s, *args, **init_props)
        self._transmit_genes(result)
        return result

    def __str__(self):
        subs_str = ', '.join([str(s) for s in self.subscripts])
        if len(subs_str) > 0:
            subs_str = '_{' + subs_str + '}'
        return self.s + subs_str

    def _repr_contents(self):
        '''returns contents to put inside 'Symbol()' in repr for self.'''
        contents = [_repr(self.s)]
        if len(self.subscripts) > 0:
            contents.append('subscripts={}'.format(repr(self.subscripts)))
        return contents

    def __eq__(self, b):
        '''return self == b'''
        if b is self:
            return True
        if not isinstance(b, Symbol):
            return False
        if self.s != b.s:
            return False
        if self.subscripts != b.subscripts:
            return False
        return True

    def _equals0(self):
        '''return self == 0  (i.e., always return False).'''
        return False

    def __iter__(self):
        '''returns an empty iterator.'''
        return iter([])

    def num_subs(self):
        '''returns (subables, unsubables).'''
        return ([self], [])

    def get_symbols(self):
        return [self]

    def get_symbols_in(self, func):
        return [self] if func(self) else []

    def is_constant(self):
        '''returns whether self is a constant.'''
        return self.constant

    def as_constant(self):
        '''returns copy of self with constant=True.'''
        return self._new(constant=True)

Symbols = _symbol_maker(Symbol)


''' --------------------- OperationContainer --------------------- '''

class OperationContainer(MultipleOperation):
    '''class for containing AbstractOperation objects,
    when it is desireable for operations on self to be
    applied to all objects contained by self.

    E.g. (LHS = RHS) + x --> LHS + x = RHS + x
    '''
    def apply_operation(self, operation, _prevent_new=False):
        '''return self with operation applied to objects contained in self.
        if _prevent_new, and all resulting objects are unchanged (via 'is'), return self.
        '''
        __tracebackhide__ = TRACEBACKHIDE
        opped = [operation(t) for t in self]
        if _prevent_new and all(opped[i] is self[i] for i in range(len(self))):
            return self
        else:
            return self._new(*opped)

    @property
    def op(self):
        '''alias for apply_operation.
        (points to self.apply_operation, rather than OperationContainer.apply_operation)
        '''
        return self.apply_operation

    ## ARITHMETIC
    def __add__(self, b):      return self.op(lambda x: x + b)
    def __radd__(self, b):     return self.op(lambda x: b + x)
    def __mul__(self, b):      return self.op(lambda x: x * b)
    def __rmul__(self, b):     return self.op(lambda x: b * x)
    def __truediv__(self, b):  return self.op(lambda x: x / b)
    def __rtruediv__(self, b): return self.op(lambda x: b / x)
    def __pow__(self, b):      return self.op(lambda x: x ** b)