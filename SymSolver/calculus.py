#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 7 2021

@author: Sevans

File purpose: (symbolic) derivatives.

inherits mainly from: linearizing.py

TODO (high priority)
    - allow for advective derivative of a vector...

TODO (medium priority)
    - _collect dy/dx + dz/dx --> d(y+z)/dx   ??

TODO (low priority - out of scope of original project purpose)
    - evaluating derivatives (symbolically and/or numerically)
    - integrals (symbolic)
    - evaluating integrals (symbolically and/or numerically)
    - limits (haha that would be impressive)
"""

''' --------------------- Imports --------------------- '''

import warnings

from .abstract_operations import (
    equals, _equals0,
)
from . import scalars as sc
from .scalars import (
    view, _repr, _str, apply, _simplyfied, _symbol_maker,
    is_constant
)
from . import vectors as vt
from .vectors import (
    is_vector, same_rank,
)
from . import linearizing as lz
from .linearizing import (
    get_order,
)
from . import tools
from .tools import (
    RENDER_MATH,
)

''' --------------------- Defaults --------------------- '''

DEFAULT_OPERATIONS = lz.DEFAULT_OPERATIONS + \
    ['Derivative', 'DerivativeCross', 'DerivativeDot', 'DotDerivative']

DERIV_DOC = \
        '''returns derivative of y
        symbolic only; does not evaluate.

        y: object
            we are taking the derivative of this object. dy / dx.
        x: None (default) or any other object
            None --> use alt instead   (NotImplemented yet)
            else --> d / dx
        partial: False (default) or True
            False --> d / dx
            True --> ∂ / ∂x
        '''

X = vt.Symbol(r'x', vector=True)  # ∂/∂x --> nabla
TIME = vt.Symbol(r't')

''' --------------------- Convenience Functions --------------------- '''

# TODO: fill in derivative functions here,
#  so that you can do y.derivative(x) or derivative(y, x).

''' --------------------- Basic Objects --------------------- '''

OperationCollection = sc._operation_collection(locals(), DEFAULT_OPERATIONS)

class AbstractOperation(OperationCollection, lz.AbstractOperation):  # TODO: inherit from vectors.py instead
    HIERARCHY = 3
    def derivative(y, x, partial=False):
        '''returns derivative of self. See DERIV_DOC for documentation.
        self is used as y, here.
        '''
        DFUNCTION = y.Derivative   # get the function to use for taking derivatives, from y.
        return DFUNCTION(y, x, partial=partial)
    derivative.__doc__ = DERIV_DOC

    dd = derivate = derivative  # aliases

    def partial_derivative(y, x):
        '''returns partial derivative of self. Equivalent to y.derivative(x, partial=True)'''
        return y.derivative(x, partial=True)

    pd = partial = partial_derivative  # aliases

    def derivative_dot(y, x, partial=False):
        '''returns derivative w.r.t. x   dot   (self)'''
        DFUNCTION = y.DerivativeDot   # get the function to use for taking derivatives, from y.
        return DFUNCTION(y, x, partial=partial)
    derivative_dot.__doc__ = DERIV_DOC

    dddot = derivate_dot = derivative_dot

    def partial_derivative_dot(y, x):
        '''Equivalent to y.derivative_dot(x, partial=True)'''
        return y.derivative_dot(x, partial=True)

    pddot = partial_dot = partial_derivative_dot

    def derivative_cross(y, x, partial=False):
        '''returns derivative w.r.t. x   cross   (self)'''
        DFUNCTION = y.DerivativeCross  # get the function to use for taking derivatives, from y.
        return DFUNCTION(y, x, partial=partial)
    derivative_dot.__doc__ = DERIV_DOC

    dcross = derivate_cross = derivative_cross  # aliases

    def partial_derivative_cross(y, x):
        '''Equivalent to y.derivative_cross(x, partial=True)'''
        return y.derivative_cross(x, partial=True)

    pdcross = partial_cross = partial_derivative_cross

    def gradient(y):
        '''Equivalent to y.partial(X), where X is the spatial coordinate vector.'''
        return y.partial(X)

    grad = gradient

    def divergence(y):
        '''Equivalent to y.partial_dot(X), where X is the spatial coordinate vector.'''
        return y.partial_dot(X)

    div = divergence

    def curl(y):
        '''Equivalent to y.partial_cross(X), where X is the spatial coordinate vector.'''
        return y.partial_cross(X)

    def dot_derivative(y, u, x, partial=True):
        '''return (u dot d/dx)(y)'''
        DFUNCTION = y.DotDerivative
        return DFUNCTION(u, x, y, partial=partial)

    def dot_partial(y, u, x):
        '''return (u dot ∂/∂x)(y)'''
        return y.dot_derivative(u, x, partial=True)

    def advective_derivative(y, u=None):
        '''advective derivative of y.'''
        if u is None:
            u = Symbol('u', vector=True, subscripts=getattr(y, 'subscripts', []))
        return y.pd(TIME) + y.dot_derivative(u, X)

    ddt = total_derivative = advective_derivative


class Symbol(lz.Symbol, AbstractOperation):
    pass

class MultipleOperation(AbstractOperation, lz.MultipleOperation):
    pass

Symbols = _symbol_maker(Symbol)

# define Sum, Product, Power, Equation in this module.
sc.define_operations(globals(), vars(lz),
                     subclasses=[MultipleOperation], operations=lz.DEFAULT_OPERATIONS)


''' --------------------- Derivative --------------------- '''

class DerivativeSymbol(sc.Symbol):   # maybe should be just Symbol?
    '''the Symbol representing a derivative function. e.g. d/dt.'''
    XNABLA=X  # ∂y/∂(XNABLA) --> nabla(y)

    def __init__(self, x, partial=False, product='', ):
        self.x = x
        self.partial = partial
        self.product = product    # use e.g. ' \cdot ' for DerivativeDot(y)
        self.s = str(self)

    def _init_properties(self):
        '''returns dict for initializing another symbol like self.'''
        kw = dict(x=self.x, partial=self.partial, product=self.product)
        return kw

    def __str__(self, **kw):
        if self.partial and self.x == self.XNABLA:
            return r'\nabla{}'.format(self.product)
        # else:
        d = 'd' if not self.partial else r'\partial'
        return r'\frac{{{}}}{{{} {}}}{}'.format(d, d, _str(self.x, **kw), self.product)

    def _repr_contents(self, _include_product=True):
        '''returns contents to put inside 'DerivativeSymbol()' in repr for self.'''
        contents = [repr(self.x)]
        if self.partial:
            contents.append('partial=True')
        if _include_product and self.product != '':
            contents.append('product={}'.format(repr(self.product)))
        return contents

    def __eq__(self, b):
        if b is self:
            return True
        if not isinstance(b, DerivativeSymbol):
            return False
        if (self.x != b.x) or (self.partial != b.partial):
            return False
        return True

    def is_constant(self):
        return False

    @property
    def is_vector(self):
        return is_vector(self.x)


class Derivative(lz.LinearOperation, MultipleOperation):
    '''dy/dx. self[0] --> DerivativeSymbol(x). self[1] --> y.'''
    OPTYPE = 'Derivative'

    def __init__(self, y, x, partial=False):
        '''returns (symbols for) dy/dx. See DERIV_DOC for documentation.'''
        f = DerivativeSymbol(x=x, partial=partial)
        self._x_symbol = x
        self._y_symbol = y
        self.partial = partial
        lz.LinearOperation.__init__(self, f, y)
    __init__.__doc__ = DERIV_DOC

    @property
    def is_vector(self):
        return is_vector(self._x_symbol) or is_vector(self._y_symbol)

    def _new(self, y, partial=None):
        '''create a new instance of self.
        (use self._x_symbol for x at __init__)
        (use self.partial if partial is None)
        '''
        if partial is None:
            partial = self.partial
        result = type(self)(y, self._x_symbol, partial=partial)
        self._transmit_genes(result)
        return result

    def replace_derivative(self, value):
        '''Replaces the DerivativeSymbol from self, using value.
        E.g. replace_derivative(dy/dx, k) --> k y
        '''
        return self.Product(value, self._y_symbol)

    def _repr_contents(self, **kw):
        '''returns contents to put inside 'Derivative()' in repr for self.'''
        contents = [_repr(self._y_symbol, **kw), _repr(self._x_symbol, **kw)]
        if self.partial:
            contents.append('partial=True')
        return contents

    def __str__(self, internal=False, enc='[]', **kw):
        '''string of self. Put in enc if internal.'''
        parens_implied = isinstance(self._y_symbol, sc.Symbol)
        return lz.LinearOperation.__str__(self, internal=internal, enc=enc, parens_implied=parens_implied, **kw)

    def __eq__(self, b):
        '''tells whether self equals b.'''
        if b is self:
            return True
        if _equals0(b):
            return _equals0(self._y_symbol)
        if not isinstance(b, Derivative):
            return False
        if (self._x_symbol != b._x_symbol):
            return False
        if (self._y_symbol != b._y_symbol):
            return False
        return True

    ## SIMPLIFYING
    _SIMPLIFY_OPS = ['_simplify_id']

    def _simplify_id(self, **kw__None):
        '''converts d/dx (constant) --> 0. Also converts dx/dx --> 1.'''
        if is_constant(self._y_symbol):
            return 0
        elif self._x_symbol == self._y_symbol:
            return 1
        else:
            return self

    ## SUBSTITUTIONS ##
    def num_subs(self):
        '''returns (subables, unsubables).'''
        if is_vector(self):
            return ([], [self])
        else:
            return ([self], [])

    ## EXPANDING
    _EXPAND_OPS = ['_expand_operations']

    def _expand_operations(self, **kw__None):
        '''expands the derivative by applying rules of calculus.
        d(z*y) --> d(z)*y + z*d(y)
        d(z**n) --> n*z**(n-1)*dz     (only if n is constant)
        Anything else will be unaffected.
        (Sums are handled by self._distribute since this is a LinearOperation.)
        '''
        y = self._y_symbol
        if isinstance(y, self.Product):
            yterms = [t for t in y]
            result = self.Sum.IDENTITY
            for i, yi in enumerate(yterms):
                iresult = self.Product.IDENTITY
                for j, yj in enumerate(yterms):
                    if j==i:
                        iresult *= self._new(yi)  # d(yi)/dx
                    else:
                        iresult *= yj             # yj
                result += iresult
            result = apply(result, '_expand_operations')
            return result
        elif isinstance(y, self.Power):
            z = y[0]
            n = y[1]     # y == z**n
            if not is_constant(n):
                return self   # return self, exactly, to help indicate we made no changes.
            # else
            return n * (z ** (n - 1)) * self._new(z)
        else:
            return self   # return self, exactly, to help indicate we made no changes.


''' --------------------- Derivative with Vectors --------------------- '''


class DerivativeCross(Derivative,):# lz.CrossProduct):
    '''derivative cross contents.'''
    OPTYPE = 'Derivative'

    def __init__(self, y, x, partial=False):
        '''returns (symbols for) d/dx dot y. See DERIV_DOC for documentation.'''
        if not (is_vector(y, count_0=True) and is_vector(x, count_0=True)):
            raise TypeError("DerivativeCross requires two vectors but got {} and {}".format(y, x))
        f = DerivativeSymbol(x=x, partial=partial, product=r' \times ')
        self._x_symbol = x
        self._y_symbol = y
        self.partial = partial
        lz.LinearOperation.__init__(self, f, y)
    __init__.__doc__ = DERIV_DOC

    def _repr_contents(self, **kw):
        return Derivative._repr_contents(self, **kw, _include_product=False)

    def replace_derivative(self, value):
        '''Replaces the DerivativeSymbol from self, using value.
        E.g. replace_derivative((d/dx) cross y, k) --> k cross y
        '''
        return self.CrossProduct(value, self._y_symbol)

    @property
    def is_vector(self):
        return True


class DerivativeDot(Derivative,):# lz.DotProduct):
    '''derivative dot contents.'''
    OPTYPE = 'Derivative'

    def __init__(self, y, x, partial=False):
        '''returns (symbols for) d/dx dot y. See DERIV_DOC for documentation.'''
        if not (is_vector(y, count_0=True) and is_vector(x, count_0=True)):
            raise TypeError("DerivativeDot requires two vectors but got {} and {}".format(y, x))
        f = DerivativeSymbol(x=x, partial=partial, product=r' \cdot ')
        self._x_symbol = x
        self._y_symbol = y
        self.partial = partial
        lz.LinearOperation.__init__(self, f, y)
    __init__.__doc__ = DERIV_DOC

    def _repr_contents(self, **kw):
        return Derivative._repr_contents(self, **kw, _include_product=False)

    def replace_derivative(self, value):
        '''Replaces the DerivativeSymbol from self, using value.
        E.g. replace_derivative((d/dx) dot y, k) --> k dot y
        '''
        return self.DotProduct(value, self._y_symbol)

    @property
    def is_vector(self):
        return False


class DotDerivative(lz.DotProduct, Derivative):
    '''(u dot (d/dx))(y)
        symbolic only; does not evaluate.

        u: object (must have is_vector(u))
            we are dotting this object into the (vector) derivative
            must be a vector (or 0)
        y: object
            we are taking the derivative of this object. (u dot (d / dx))(y).
            can be a vector or scalar.
        x: object (must have is_vector(x))
            d / dx.
            must be a vector.
        partial: True (default) or False
            True --> ∂ / ∂x

    if y is not a vector, returns DotProduct(u, Derivative(y, x)) instead.
    '''
    def __new__(cls, u, x, y, partial=True):
        if _equals0(u) or is_constant(y):
            return 0
        elif not (is_vector(u, count_0=True) and is_vector(x, count_0=True)):
            raise TypeError("DotDerivative requires vectors u, x but got {} and {}".format(u, x))
        elif not is_vector(y):
            DOT_PRODUCT = y.DotProduct   # get DotProduct from y in case u or x is predefined
            # e.g. for ddt, u and x are defined here. But we want to use subclass's DotProduct.
            return DOT_PRODUCT(u, y.derivative(x, partial=partial))
        else:
            instance = object.__new__(cls)
            instance.__init__(u, x, y, partial=partial)
            return instance

    OPTYPE = 'Derivative'

    def __init__(self, u, x, y, partial=True):
        ddx = DerivativeSymbol(x=x, partial=partial)
        uddx = self.DotProduct(u, ddx)
        self._u = u
        self._x = x
        self._y = y
        self.partial = partial
        lz.LinearOperation.__init__(self, uddx, y)

    @property
    def _x_symbol(self): return self._x
    @property
    def _y_symbol(self): return self._y

    @property
    def terms(self):
        return [self._u, self._y]

    def replace_derivative(self, value):
        '''Replaces the DerivativeSymbol from self, using value.
        E.g. replace_derivative(u dot dy/dx, k) --> (u dot k) * y.
        '''
        return self.Product(self.DotProduct(self._u, value), self._y)

    @property
    def is_vector(self):
        return is_vector(self._y)

    def _new(self, *args, partial=None):
        '''create a new instance of self.
        two options for call signature:
            self._new(y, partial=...)
            self._new(u, y, partial=...)
        (use self._x for x at __init__)
        (if u is not provided, use self._u for u at __init__)
        (if partial is None, use self.partial for partial at __init__)
        '''
        if len(args) == 1:
            u, y = self._u, args[0]
        elif len(args) == 2:
            u, y = args
        else:
            wrong_number_args_errmsg = ("Wrong number of args for {}._new(). "
                                        "Expected 1 or 2 but got {}").format(type(self), len(args))
            raise TypeError(wrong_number_args_errmsg)
        if partial is None:
            partial = self.partial
        result = type(self)(u, self._x, y, partial=partial)
        self._transmit_genes(result)
        return result

    def _repr_contents(self, **kw):
        '''returns contents to put inside 'Derivative()' in repr for self.'''
        contents = [_repr(self._u), _repr(self._x, **kw), _repr(self._y, **kw)]
        if self.partial:
            contents.append('partial=True')
        return contents

    def __str__(self, **kw):
        parens_implied = isinstance(self._y, sc.Symbol)
        return lz.LinearOperation.__str__(self, parens_implied=parens_implied, **kw)

    def __eq__(self, b):
        '''tells whether self equals b.'''
        if b is self:
            return True
        if _equals0(b):
            return _equals0(self._u) or _equals0(self._y)
        if not isinstance(b, DotDerivative):
            return False
        if any(getattr(self, _t) != getattr(b, _t) for _t in ('_u', '_x', '_y')):
            return False
        return True

    def split_numeric_component(self):
        '''returns (1, self)'''
        return (1, self)

    ## LINEARIZING
    # order, linearize, and o0 are handled by DotProduct.

    ## SIMPLIFYING
    _SIMPLIFY_OPS = ['_simplify_id']

    def _simplify_id(self, **kw__None):
        '''converts (u dot d/dx)(constant) --> 0; (0 dot d/dx)(y) --> 0.'''
        if is_constant(self._y) or is_constant(self._u):
            return 0
        else:
            return self

    ## EXPANDING
    _EXPAND_OPS = ['_distribute']
    # _expand_operations is handled by Derivative.

    def _distribute(self, **kw__None):
        return lz.LinearOperation._distribute(self, **kw__None)

