#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 25 2021

@author: Sevans

File purpose: "Constants". Objects which will
    - be treated like "numbers" by the other AbstractOperations.
    - have some of the convenience methods of other AbstractOperations.
        - e.g. view, is_constant
"""
# import built-in packages
import functools
import math

# import internal modules
from . import tools
from .tools import (
    _str, _repr, apply, is_integer, view,
    RENDER_MATH,
)
from . import abstract_operations as ao
from .abstract_operations import (
    is_constant, is_number,
    is_opinstance,
    equals,
)


''' --------------------- Defaults --------------------- '''

TRACEBACKHIDE = tools.TRACEBACKHIDE
DEFAULT_OPERATIONS = []
FLOAT_CONVERSION = True   # whether to convert to float (or complex) if operation with float is performed.
                          # when False, instead raise TypeError.
                          # before March 24, 2022, the default behavior was as if FLOAT_CONVERSION=False.

''' --------------------- Convenience --------------------- '''

def convert_if_not_implemented(f):
    '''wrap function f(self, x) to evaluate self as a constant first, if f(self, x) gives NotImplemented.
    if FLOAT_CONVERSION = False, just return f(self, x)   (without catching any errors)
    [TODO][REF] implementation here is a bit hacky, using f.__name__ if we have to evaluate self.
    '''
    @functools.wraps(f)
    def f_but_convert_if_typeerror(self, x):
        __tracebackhide__ = TRACEBACKHIDE
        result = f(self, x)
        if FLOAT_CONVERSION and (result is NotImplemented):
            op = f.__name__    # e.g. '__add__'
            return getattr(self.evaluate_constants(), op)(x)
        else:
            return result
    return f_but_convert_if_typeerror

def rational_arithmetic(f):
    '''wrap arithmetic functions f(self, x) where x must be type(self) or integer.

    The wrapped function will do the following:
        if x is an integer, convert it to type(self)
        if type(x) == type(self), return f(self, x)
        else, return NotImplemented
    '''
    @convert_if_not_implemented
    @functools.wraps(f)
    def f_rational_arithmetic_form(self, x):
        __trackebackhide__ = TRACEBACKHIDE
        if is_integer(x):
            x = self._new(int(x))
        if is_opinstance(x, type(self)):
            return f(self, x)
        else:
            return NotImplemented
    return f_rational_arithmetic_form

def is_rational(x):
    return is_integer(x) or apply(x, 'is_rational', default=False)

def complex_arithmetic(f):
    '''wrap arithmetic functions f(self, x) where x must be type(self) or Rational.

    The wrapped function will do the following:
        if x is a rational number, convert it to type(self)
        if type(x) == type(self), return f(self, x)
        else, return NotImplemented
    '''
    @convert_if_not_implemented
    @functools.wraps(f)
    def f_complex_arithmetic_form(self, x):
        __trackebackhide__ = TRACEBACKHIDE
        if is_rational(x) and (not is_opinstance(x, type(self))):
            x = self._new(Rational(x))
        if is_opinstance(x, type(self)):
            result = f(self, x)
            result = apply(result, '_simplify_id')
            return result
        else:
            return NotImplemented
    return f_complex_arithmetic_form

''' --------------------- Abstract Constant --------------------- '''

class AbstractConstant(ao.SimplifiableOperation):
    OPTYPE = 'Constant'

    __new__ = object.__new__   # overwrite the __new__ from AbstractOperation.

    def is_constant(self):
        return True

    def is_number(self):
        return True

    def __iter__(self):
        '''returns an empty iterator.'''
        return iter([])

    def num_subs(self):
        '''returns (subables, unsubables).
        (Constants should be ignored completely when determining what to subsitute to make numbers;
        constants can be turned into numbers via evaluate_numbers().)'''
        return ([], [])


''' --------------------- Rational Numbers --------------------- '''

class Rational(AbstractConstant):
    '''rational number.'''
    OPTYPE = 'Rational'
    def __new__(cls, numer, denom=1):
        __tracebackhide__ = True
        if type(numer) == cls:
            return numer / denom
        else:
            instance = object.__new__(cls)
            instance.__init__(numer, denom)
            return instance

    def __init__(self, numer, denom=1):
        self.numer = numer
        self.denom = denom

    def _repr_contents(self, **kw):
        contents = [_repr(self.numer, **kw)]  # numerator
        if not equals(self.denom, 1):
            contents.append(_repr(self.denom, **kw))  # denominator
        return contents

    def __str__(self, **kw):
        numer_str = _str(self.numer, **kw)
        if equals(self.denom, 1):
            return numer_str
        else:
            denom_str = _str(self.denom, **kw)
            return r'\frac{{{}}}{{{}}}'.format(numer_str, denom_str)

    def is_constant(self):
        return is_constant(self.numer) and is_constant(self.denom)

    def is_rational(self):
        return is_rational(self.numer) and is_rational(self.denom)

    ## COMPARISONS ##
    def __eq__(s, x):       # s == x
        if x is s:
            return True
        if equals(x, 0):
            return equals(s.numer, 0)
        s = s._simplify_id()   # take out the greatest common divisor.
        if is_integer(x):
            return equals(s.denom, 1) and equals(s.numer, int(x))
        if is_opinstance(x, type(s)):
            x = x._simplify_id()
            return equals(s.numer, x.numer) and equals(s.denom, x.denom)
        return False

    @rational_arithmetic
    def __gt__(s, x):       # s > x
        if x == 0:
            return (s.numer > 0) == (s.denom > 0)   # True when s is postive.
        else:
            if (s > 0) == (x > 0):
                return s.numer * x.denom > x.numer * s.denom
            else:
                return s.numer * x.denom < x.numer * s.denom

    @rational_arithmetic
    def __lt__(s, x):       # s < x
        if x == 0:
            return (s.numer < 0) != (s.denom < 0)   # True when s is negative.
        else:
            if (s < 0) == (x < 0):
                return s.numer * x.denom < x.numer * s.denom
            else:
                return s.numer * x.denom > x.numer * s.denom


    ## ARITHMETIC BETWEEN RATIONALS AND/OR INTEGERS ##
    @rational_arithmetic
    def __add__(s, x):      # s + x
        return s._new(s.numer * x.denom + x.numer * s.denom, s.denom * x.denom)

    @rational_arithmetic
    def __radd__(s, x):     # x + s
        return s._new(x.numer * s.denom + s.numer * x.denom, x.denom * s.denom)

    @rational_arithmetic
    def __mul__(s, x):      # s * x
        return s._new(s.numer * x.numer, s.denom * x.denom)

    @rational_arithmetic
    def __rmul__(s, x):     # x * s
        return s._new(x.numer * s.numer, x.denom * s.denom)

    @rational_arithmetic
    def __pow__(s, n):
        if not equals(n.denom, 1):
            return super().__pow__(s, n)
        elif n.numer > 0:
            return s._new(s.numer ** n.numer, s.denom ** n.numer)
        else:
            return s._new(s.denom, s.numer) ** (-1 * n.numer)

    @rational_arithmetic
    def __sub__(s, x):      # s - x
        return s + (-1 * x)
    @rational_arithmetic
    def __rsub__(s, x):     # x - s
        return x + (-1 * s)
    @rational_arithmetic
    def __truediv__(s, x):  # s / x
        return s * (x ** -1)
    @rational_arithmetic
    def __rtruediv__(s, x): # x / s
        return x * (s ** -1)

    ## SIMPLIFYING ##
    _SIMPLIFY_OPS = ['_simplify_id', '_evaluate_numbers']

    def _simplify_id(self, **kw__None):
        '''divides numerator and denominator by gcd(numerator, denominator).
        also multiplies numerator and denominator by -1, if denominator is less than 0.
        '''
        if (self.denom < 0):
            self = self._new(self.numer * -1, self.denom * -1)
        if isinstance(self.numer, int) and isinstance(self.denom, int):
            gcd = math.gcd(self.numer, self.denom)
        else:
            gcd = 1
        if gcd == 1:
            return self    # return self, exactly, to help indicate we made no changes.
        else:
            return self._new(self.numer // gcd, self.denom // gcd)  # // is for integer division.

    def _evaluate_numbers(self, evaluate_constants=False, **kw__None):
        '''evaluates self as a number if evaluate_constants; else returns self.'''
        if not evaluate_constants:
            return self
        else:
            return self.numer / self.denom


''' --------------------- Complex Rationals --------------------- '''

class ComplexRational(AbstractConstant):
    '''complex number with Rational real and imaginary parts.'''
    OPTYPE = 'ComplexRational'

    def __init__(self, real, imag=0, i='i'):
        self.real = Rational(real)
        self.imag = Rational(imag)
        self.i    = i

    @property
    def real(self):
        return self._real

    @real.setter
    def real(self, value):
        self._real = value

    @property
    def imag(self):
        return self._imag

    @imag.setter
    def imag(self, value):
        self._imag = value

    def complex_conjugate(self):
        return self._new(self.real, -1 * self.imag)

    @property
    def cc(self):
        return self.complex_conjugate

    def __str__(self, **kw):
        real_str = _str(self.real, **kw)
        imag_str = _str(self.imag, **kw)
        if equals(self.real, 0):
            real_str = ''
            if equals(self.imag, 0):
                return '0'
        if self.imag in (0, 1):
            imag_str = ''
        elif equals(self.imag, -1):
            imag_str = '-'
        if not equals(self.imag, 0):
            imag_str += self.i
        if len(imag_str)>0 and len(real_str)>0 and (not imag_str.startswith('-')):
            imag_str = '+' + imag_str
        return real_str + imag_str

    def _repr_contents(self, **kw):
        contents = [_repr(self.real, **kw)]  # real part
        if not equals(self.imag, 0):
            contents.append(_repr(self.imag, **kw))  # imag part
        return contents

    def is_constant(self):
        return is_constant(self.real) and is_constant(self.imag)

    def is_rational(self):
        return equals(self.imag, 0) and is_rational(self.real)

    ## COMPARISONS ##
    def __eq__(s, x):
        if x is s:
            return True
        if equals(x, 0):
            return equals(0, s.real) and equals(0, s.imag)
        elif is_opinstance(x, type(s)):
            return equals(s.real, x.real) and equals(s.imag, x.imag)
        elif is_rational(x):
            return equals(s.imag, 0) and equals(s.real, x)
        else:
            return False

    @complex_arithmetic
    def __gt__(s, x):       # s > x
        if (not equals(s.imag, 0)) or (not equals(x.imag, 0)):
            compare_complex_errmsg = ("Can't do > for Complex numbers with nonzero imaginary parts:"
                                      "\n{}, {}".format(s, x))
            raise TypeError(compare_complex_errmsg)
        return s.real > x.real

    @complex_arithmetic
    def __lt__(s, x):       # s < x
        if (not equals(s.imag, 0)) or (not equals(x.imag, 0)):
            compare_complex_errmsg = ("Can't do > for Complex numbers with nonzero imaginary parts:"
                                      "\n{}, {}".format(s, x))
            raise TypeError(compare_complex_errmsg)
        return s.real < x.real

    ## ARITHMETIC BETWEEN COMPLEXRATIONALS AND/OR RATIONALS ##
    @complex_arithmetic
    def __add__(s, x):      # s + x
        return s._new(s.real + x.real, s.imag + x.imag)

    @complex_arithmetic
    def __radd__(s, x):     # x + s
        return s._new(x.real + s.real, x.imag + s.imag)

    @complex_arithmetic
    def __mul__(s, x):      # s * x
        return s._new(s.real * x.real - s.imag * x.imag,
                      s.real * x.imag + s.imag * x.real)

    @complex_arithmetic
    def __rmul__(s, x):     # x * s
        return s._new(x.real * s.real - x.imag * s.imag,
                      x.real * s.imag + x.imag * s.real)

    def __pow__(s, n):
        if n < 0:
            return 1 / (s ** (-1 * n))
        elif equals(n, 0):
            return s._new(1)
        elif not is_integer(n):
            raise NotImplementedError
        else:
            # TODO: make more efficient
            result = 1
            for i in range(n):
                result *= s
            return result

    @complex_arithmetic
    def __sub__(s, x):      # s - x
        return s + (-1 * x)

    @complex_arithmetic
    def __rsub__(s, x):     # x - s
        return x + (-1 * s)

    @complex_arithmetic
    def __truediv__(s, x):  # s / x
        if equals(x.imag, 0):
            real = (s.real / x.real)._simplify_id()
            imag = (s.imag / x.real)._simplify_id()
            return s._new(real, imag)
        else:
            return (s * x.cc()) / (x * x.cc())

    @complex_arithmetic
    def __rtruediv__(s, x): # x / s
        if equals(s.imag, 0):
            return s._new(x.real / s.real, x.imag / s.real)
        else:
            return (x * s.cc()) / (s * s.cc())

    ## SIMPLIFYING ##
    _SIMPLIFY_OPS = ['_simplify_id', '_evaluate_numbers']

    def _simplify_id(self, **kw):
        real = apply(self.real, '_simplify_id', **kw)
        imag = apply(self.imag, '_simplify_id', **kw)
        if (real is self.real) and (imag is self.imag):
            return self    # return self, exactly, to help indicate we made no changes.
        else:
            return self._new(real, imag, **kw)

    def _evaluate_numbers(self, evaluate_constants=False, **kw__None):
        '''evaluates self as a number if evaluate_constants; else returns self.'''
        if not evaluate_constants:
            return self
        else:
            kw = dict(evaluate_constants=True, **kw__None)
            real = apply(self.real, '_evaluate_numbers', **kw)
            imag = apply(self.imag, '_evaluate_numbers', **kw)
            return real + 1j * imag


def ImaginaryUnit(i='i'):
    return ComplexRational(real=0, imag=1, i=i)
