#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 5 2021

@author: Sevans

File purpose: import the most up-to-date versions of each class.
E.g. Symbol from linearizing.py is more up to date than Symbol from scalars.py.

The inheritance looks like:
        scalars
           |
        vectors
           |
      linearizing
           |
        calculus
           |
      plane_waves
"""

''' --------------------- Imports --------------------- '''

from .tools import (
    RENDER_MATH  # use RENDER_MATH.on() to turn it on, or off() to turn it off.
)

## BASICS ##
from .constants import (
    Rational,
    ComplexRational,
    ImaginaryUnit,
)
from .scalars import (
    view, _repr, _str, apply, _simplyfied,
    is_constant, is_number,
    is_opinstance,
    get_base_and_power, get_factors, get_summands,
    solubility_list,
    gcf,
)
from .vectors import (
    is_vector, same_rank,
    scalar_vector_split,
    is_basis_vector, Basis, OrthonormalBasis, OrthonormalBasis3D,
)
from .linearizing import (
    get_order,
)
from .plane_waves import (
    Symbol, Symbols, 
    Power, Product, Sum,
    MultiSum,
    Equation, EquationSystem, Inequality,
    DotProduct, CrossProduct,
    Derivative, DerivativeDot, DotDerivative
)

## EXTRAS ##
from .polynomials import (
    Polynomial, PolySymbol, PolyConstant,
    PolyArray,
    PolyFrac, PolyFracSymbol, PolyFracConstant,
)


''' --------------------- Convenient Symbols --------------------- '''

# scalar variables
x, y, z, t   = Symbols(['x', 'y', 'z', 't'])
kmag, omega  = Symbols(['k', r'\omega'])
n,   rho,   P,   T   = Symbols(['n', r'\rho', 'P', 'T'])
n_s, rho_s, P_s, T_s = Symbols(['n', r'\rho', 'P', 'T'], subscripts=['s'])
phi, theta   = Symbols([r'\phi', r'\theta'])

# vector variables
xhat, yhat, zhat = Symbols(['x', 'y', 'z'], hat=True, constant=True)
xvec, k = Symbols(['x', 'k'], vector=True)
E, B, J = Symbols(['E', 'B', 'J'], vector=True)
u,   v   = Symbols(['u', 'v'], vector=True)
u_s, v_s = Symbols(['u', 'v'], vector=True, subscripts=['s'])

# coordinate systems
CartesianCoordinates = OrthonormalBasis3D(xhat, yhat, zhat)
Cartesian   = CartesianBasis   = CartesianCoordinates               # alias
Rectangular = RectangularBasis = RectangularCoordinates = Cartesian # alias
XYZ         = CartesianCoordinates                                  # alias


# scalar constants (for a given fluid)
q, m,     = Symbols(['q', 'm'], constant=True)
q_s, m_s, = Symbols(['q', 'm'], constant=True, subscripts=['s'])

# scalar constants (fundamental)
eps0, mu0, c    = Symbols([r'\epsilon_0', r'\mu_0', 'c'], constant=True)
kB, gamma       = Symbols(['k_B', r'\gamma'], constant=True)
e,              = Symbols(['e'], constant=True)
i = IUNIT       = ImaginaryUnit()

# vector constants
g,           = Symbols(['g'], vector=True, constant=True)


''' --------------------- Convenient Referencing --------------------- '''

GG = globals()
def PreDefinedSymbols(syms_to_get, cast_to_list=False):
    '''returns the requested pre-defined symbols (from expressions.py).
    syms_to_get: list of strings, or string
        string --> convert to list via split()
        list --> returns a list of these symbols.
    cast_to_list: False (default), or True
        how to handle when len(syms_to_get) == 1.
        False --> return single symbol.
        True  --> return [single symbol].
    '''
    ss = syms_to_get.split() if isinstance(syms_to_get, str) else syms_to_get
    result = []
    for key in ss:
        try:
            val = GG[key]
        except KeyError:
            raise KeyError("{} is not a pre-defined symbol in expressions.py".format(repr(key)))
        result.append(val)
    if not cast_to_list and len(result) == 1:
        return result[0]
    else:
        return result

preexisting = predefined = symbols = PreDefinedSymbols

