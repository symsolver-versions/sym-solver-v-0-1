#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 5 2021

@author: Sevans

File purpose: linearize things.

inherits mainly from: scalars.py

TODO:
    - vec(g) * n(1) = vec(u)(1) --> can't solve for n(1); can solve for u(1).
    - focus only on linear terms during collection for sum.
    - put equations with defined terms, to be more concise.
        - for example, n(1) = (complicated_stuff) * P(1)
          Then, put equations "A = complicated_stuff", n(1) = A * P(1).
    - use solve() instead of linsolve()
    - automatically input the o0() solution as an assumption to the o1 steps.
"""

''' --------------------- Imports --------------------- '''

import math   # for math.nan, for MIXED_ORDER. Also math.inf for convenience in a loop.
import warnings   # for warning when there is extra info in the EquationSystem.
import functools

from . import scalars as sc
from .scalars import (
    view, _repr, _str, apply, _simplyfied,
    is_constant,
    _symbol_maker, _operation_collection,
    is_opinstance,
    SystemTooComplicatedError,
)
from . import vectors as vt
from .vectors import (
    is_vector
)
from . import tools
from .tools import (
    RENDER_MATH,
)

''' --------------------- Defaults --------------------- '''

from .vectors import DEFAULT_OPERATIONS
PARENT = vt   # subclass from operations in this module.

MIXED_ORDER = math.nan   # order will be this value if there are 0th and 1st order terms.

''' --------------------- Convenience Functions --------------------- '''

def get_order(x, default=None):
    '''returns the order of x.'''
    result = getattr(x, 'order', None)
    if result is None:
        result = default
    return result


''' --------------------- Basic Objects --------------------- '''

OperationCollection = _operation_collection(locals(), DEFAULT_OPERATIONS)

class AbstractOperation(OperationCollection, PARENT.AbstractOperation):
    HIERARCHY = 2
    ## LINEARIZING ##
    def linearize(self, drop0=True):
        raise NotImplementedError("{}.linearize()".format(type(self)))

    def o1(self):
        '''returns 1'st order version of self.
        equivalent to linearize(self, drop0=True)
        '''
        return self.linearize(drop0=True)

    @property
    def oI(self):
        return self.o1()

    @property
    def oZ(self):
        return self.o0()

    def list_o1_symbols(self):
        '''lists o1 symbols inside self.'''
        if isinstance(self, sc.Symbol):
            if get_order(self) == 1:
                return [self]
            else:
                return []
        else:
            result = []
            for t in self:
                result += apply(t, 'list_o1_symbols', default=[])
            # remove duplicates and return result
            return list(tools.Set(result))

    def list_o1_unsolvables(self, solvable_ops=None):
        '''lists o1 symbols inside self which we cannot solve for.'''
        candidates = self.list_o1_symbols()
        solvables  = self.list_o1_solvables()
        return list(tools.Set(candidates) - tools.Set(solvables))

    def list_o1_solvables(self, solvable_ops=None):
        '''lists o1 symbols inside self which we CAN solve for.'''
        candidates = self.list_o1_symbols()
        solvables  = [can for can in candidates if self.solvable(can)]
        return solvables

    def assume_o0_constant(self, exceptions=[]):
        '''assumes ALL o0 quants in self are constant.
        Replaces all o0 symbols with constant version of themselves.
        E.g. Symbol('n', order=0) --> Symbol('n', order=0, constant=True).

        (Doesn't apply to any quants in exceptions.)
        '''
        if is_opinstance(self, Symbol):
            if (self.order == 0) and not any(x.o0()==self for x in exceptions):
                return self._new(constant=True)
            else:
                return self
        else:
            return self._new(*[apply(t, 'assume_o0_constant', exceptions) for t in self])

    def _o1_componentize_targets(self):
        '''returns list of o1 vector symbols in self.'''
        return [t for t in self.list_o1_symbols() if is_vector(t)]

    def o1_componentize(self, basis, **kw):
        '''substitutes o1 vectors from self with their components in the given basis.'''
        return self.componentize(basis, self._o1_componentize_targets(), **kw)


class Symbol(PARENT.Symbol, AbstractOperation):
    '''linearizable symbol.
    __mro__ is (linearizing.Symbol, scalars.Symbol,
    linearizing.AbstractOperation, scalars.AbstractOperation, object)
        (this ensures e.g. we use ARITHMETIC_FORBIDDEN_TYPES from linearizing.AbstractOperation.)
    '''
    def __init__(self, s, *args, order=None, **kwargs):
        super().__init__(s, *args, **kwargs)
        self.order = order
        # if self.order == 0: self.constant = True    # maybe do this?

    def _init_properties(self):
        '''returns dict for initializing another symbol like self.'''
        kw = super()._init_properties()
        kw['order'] = self.order
        return kw

    def __str__(self, internal=False, enc='()', **kw):
        '''internal tells type of enclosing function.'''
        s = _str(super(), internal=internal, enc=enc, **kw)
        if self.order is not None:
            s += r'^{{({})}}'.format(self.order)  # self looks like s^{(0)}, for example.
            if internal and issubclass(internal, sc.Power):  # looks like (s^{(0)})^{3}, for example.
                s = r'\left{} {} \right{}'.format(enc[0], s, enc[1])
        return s

    def _repr_contents(self):
        '''returns contents to put inside 'Symbol()' in repr for self.'''
        contents = super()._repr_contents()
        if self.order is not None:
            contents.append('order={}'.format(self.order))
        return contents

    def __eq__(self, b):
        '''return self == b'''
        if b is self:
            return True
        if not super().__eq__(b):
            return False
        if self.order != getattr(b, 'order', None):
            return False
        return True

    ## IMPLEMENT LINEARIZE
    def linearize(self, drop0=True):
        '''return linear form of self.'''
        if self.constant:
            if drop0:
                return 0
            else:
                return self
        if self.order is not None:
            raise ValueError("Was not expecting to linearize() a symbol with existing order:", repr(self))
            #maybe we should return 0 instead?
        #else
        o1 = self._new(order=1)
        if drop0:
            return o1
        else:
            o0 = self._new(order=0)
            return o0 + o1

    def o0(self):
        '''returns 0'th order version of self.'''
        if self.constant:
            return self
        return self._new(order=0)


Symbols = _symbol_maker(Symbol)


''' --------------------- Operations - Abstract --------------------- '''

class MultipleOperation(AbstractOperation, PARENT.MultipleOperation):
    def o0(self):
        '''returns 0'th order version of self.'''
        o0t = [t if is_constant(t) else apply(t, 'o0') for t in self]
        return self._new(*o0t)

    @property
    def order(self):
        '''returns order of self. 0, 1, None (order undefined), or MIXED_ORDER (math.nan).'''
        raise NotImplementedError("order property for {}".format(type(self)))


class BinaryOperation(PARENT.BinaryOperation, MultipleOperation):
    pass


# define Sum, Product, Power, Equation, ..., in this module.
sc.define_operations(globals(), vars(PARENT),
                     subclasses=[MultipleOperation], operations=sc.DEFAULT_OPERATIONS)


''' --------------------- Basic Arithmetic --------------------- '''

class Sum(PARENT.Sum, MultipleOperation):
    def linearize(self, drop0=True):
        '''return linear form of self.
        if drop0, drop all 0th order terms.
        '''
        terms = [apply(t, 'linearize', drop0=drop0) for t in self]
        return _simplyfied(self._new(*terms))

    @property
    def order(self):
        '''tells the order of self.
        order will be:
            N if all terms in self are order N or None
            MIXED_ORDER (math.nan) if any two terms with non-None order in self have different order
            None if all terms in self are order None
        '''
        itterms = iter(self)
        order = get_order(next(itterms))
        for t in itterms:
            ot = get_order(t)
            if ot != order:
                if order is None:
                    order = ot
                elif order == 0 and ot is None:
                    pass   # this case is allowed.
                else:
                    return MIXED_ORDER
        return order

    def drop0(self):
        '''drops 0'th order (and constant) terms from self.'''
        t = [t for t in self if (not get_order(t) in (None, 0))]
        return self._new(*t)

    def COLLECTION_PRIORITIZER(self, x):
        '''returns collection priority number for x.
        highest means "collect first".
        if x is None, return the maximum possible value.
        '''
        CHILD_MAX = PARENT.Sum.MAXIMUM_COLLECTION_PRIORITY
        if get_order(x) == 1:
            return CHILD_MAX + 10
        else:
            return PARENT.Sum.COLLECTION_PRIORITIZER(self, x)

    MAXIMUM_COLLECTION_PRIORITY = PARENT.Sum.MAXIMUM_COLLECTION_PRIORITY + 10


class Product(PARENT.Product, MultipleOperation):
    def linearize(self, drop0=True):
        '''return linear form of self.
        if drop0, drop all 0'th order terms.
        '''
        N = len(self)
        lto0 = [apply(t, 'o0') for t in self]
        lto1 = [apply(t, 'o1', default=0) for t in self]
        result = Sum.IDENTITY
        # add 0'th order term
        if not drop0:
            result += self._new(*lto0)
        # add 1st order terms
        for i in range(N):
            # use the first order contribution from the i'th term.
            term = self.IDENTITY
            for j in range(N):
                if i == j:
                    term *= lto1[i]
                else:
                    term *= lto0[j]
            result += term
        return result

    @property
    def order(self):
        '''tells the order of self.
        equals sum of orders of terms in self, treating order=None as order=0.
        (if all terms in self have order None, return None)
        '''
        return tools.default_sum(*(get_order(t) for t in self), default=(None, 0))

    def _str_terms_order(self):
        '''returns lists of terms of self in order for str representation of self.
        returns:
            list of non-AbstractOperations (e.g. numbers),
            list of constants (which are also AbstractOperation objects)
            list of AbstractOperation objects with order != 0 and order != 1
            list of order 0 terms,
            list of order 1 terms.
        '''
        numbers, constants, terms, hats = super()._str_terms_order()
        result = [numbers, constants, [], [], [], hats]   # numbers, constants, mixed_order, o0, o1, hats
        for term in terms:
            o = get_order(term)
            if o == 0:
                result[3].append(term)
            elif o == 1:
                result[4].append(term)
            else:
                result[2].append(term)
        return result

class Power(PARENT.Power, BinaryOperation):
    def linearize(self, drop0=True):
        '''return linear form of self.
        if drop0, drop all 0'th order terms.

        linear form comes from taylor expansion:
            x^n ~= x0^n + n*x0^(n-1) x1
        (which assumes x0 commutes with x1 under multiplication)

        currently not implemented for non-constant exponents.
        '''
        if not is_constant(self.t2):
            raise NotImplementedError("linearize with non-constant exponent: {}".format(repr(self)))
        # self == x^n
        x0 = apply(self.t1, 'o0')
        x1 = apply(self.t1, 'o1')
        n  = self.t2
        # calculate result
        result = Sum.IDENTITY
        # add 0'th order term.
        if not drop0:
            result += self._new(x0, n)
        # add 1st order term.
        result += n * (x0 ** (n - 1)) * x1
        return result

    @property
    def order(self):
        '''tells the order of self.'''
        if get_order(self.t2, default=0) != 0:
            return MIXED_ORDER   # we don't know how to find order of x^{y^{(1)}}...
        o = get_order(self.t1, default=0)
        return o * self.t2


''' --------------------- LinearOperation, MultiSum --------------------- '''

class LinearOperation(PARENT.LinearOperation, MultipleOperation):
    def o0(self):
        '''return 0'th order form of self.'''
        terms = [apply(t, 'o0') for t in self]
        return self._new(*terms)

    @property
    def order(self):
        '''tells the order of self.
        order will be:
            N if all terms in self are order N or None
            MIXED_ORDER (math.nan) if any two terms with non-None order in self have different order
            None if all terms in self are order None
        '''
        itterms = iter(self)
        order = get_order(next(itterms))
        for t in itterms:
            ot = get_order(t)
            if ot != order:
                if order is None:
                    order = ot
                else:
                    return MIXED_ORDER
        return order

    def linearize(self, drop0=True, distribute=True):
        '''return linear form of self.'''
        if self.is_constant():
            return self
        terms = []
        for t in self:
            if hasattr(t, 'linearize'):
                tlin = t.linearize(drop0=drop0)
            else:
                warnings.warn("Linearizing term without linearize() in {}: {}".format(type(self), t))
                tlin = t
            terms.append(tlin)
        result = self._new(*terms)
        if distribute:
            result = result._distribute()
        return result


class MultiSum(PARENT.MultiSum, LinearOperation):
    pass


''' --------------------- Equations --------------------- '''

class Equation(PARENT.Equation, MultipleOperation):
    def linearize(self, drop0=True):
        return self.op(lambda x: apply(x, 'linearize', drop0=drop0))

    def o1solve_simple(self, show_work=False, basis=None, **kw__simplified):
        '''solves self if self looks like x(1) * A = 0.
        Otherwise raises NotImplementedError
        '''
        o1s = self.list_o1_symbols()
        if len(o1s) != 1:
            too_many_o1s_errmsg = ("o1solve_simple only knows how to handle equations "
                                   "with one o1 symbol, but got equation: {}".format(self))
            raise SystemTooComplicatedError(too_many_o1s_errmsg)
        q1 = o1s[0]
        return self.eliminate(q1, show_work=show_work, basis=basis, **kw__simplified)


def _componentize_if_too_complicated(f):
    '''if f raises a SystemTooComplicatedError, try componentize() before giving up.'''
    @functools.wraps(f)
    def f_but_componentize_if_too_complicated(self, show_work=False, simplified=True, basis=None, **kw):
        __tracebackhide__ = True
        try:
            return f(self, show_work=show_work, simplified=simplified, basis=basis, **kw)
        except SystemTooComplicatedError:
            # equation system is too complicated to solve in its current form.
            if (basis is not None):
                # break equations into components, and solve the new system.
                explain = tools.debug_viewer(show_work)
                if getattr(self, '_has_been_o1_componentized', False):
                    explain('System is too complicated to solve, and already broke all o1 terms into components.')
                    raise
                explain('System is too complicated to solve using vector equations.\nAttempting to ' +\
                        'break the unsolved equations ({}) into components '.format(self.unsolved) +\
                        'using the basis provided:', view=basis)
                old_system = self
                new_system = self.o1_componentize(basis, unsolved_only=True, keep_hats=False)
                if new_system is old_system:
                    explain('Breaking into components changes nothing. Raising the error...')
                    raise
                explain('Simplifying the componentized system:')
                kw = dict(hats_id=kw.pop('hats_id', True), magnitude_id=kw.pop('magnitude_id', True), **kw)
                new_system = new_system.simplified(**kw)
                if show_work:
                    explain(view=_str(new_system, index_from=0))
                new_system.set_gene('_has_been_o1_componentized', True)
                return new_system
            else:
                raise
    return f_but_componentize_if_too_complicated

class EquationSystem(PARENT.EquationSystem, MultipleOperation):
    def linearize(self, drop0=True):
        return self.op(lambda x: apply(x, 'linearize', drop0=drop0))

    def o1_componentize(self, basis, unsolved_only=True, **kw):
        '''substitutes o1 vectors from self with their components in the given basis;
        also breaks vector equations in self into their components.
        '''
        targets = self._o1_componentize_targets()
        result = self.componentize(basis, targets=targets, unsolved_only=unsolved_only, **kw)
        return result

    def _o1solvestep_choices(self, choice_mode=0):
        '''returns (index (in self) of equation to solve, (o1) quant to solve for) from self.
        Goal:
            - always try to solve for vectors before trying to solve for scalars.
            - prioritize solving the least complex equation.
            - prioritize the quant which also appears in the {fewest / most} other equations
                (based on choice_mode. choice_mode=0 --> 'fewest'. choice_mode=1 --> 'most')
        '''
        # (preprocessing)
        unsolved     = self.unsolved
        complexities = [self[i].complexity() for i in unsolved]          # "complexity" of each unsolved equation.
        o1_quants    = [self[i].list_o1_symbols() for i in unsolved]     # o1 quants which exist,      in each unsolved equation
        sol_quants   = [self[i].list_o1_solvables() for i in unsolved]   # o1 quants we can solve for, in each unsolved equation
        # if any equation has only 1 o1 quant, we are almost at the dispersion relation! Choose that equation.
        for i, o1_quants_i in enumerate(o1_quants):
            if len(o1_quants_i) == 1:
                return (unsolved[i], o1_quants_i[0])
        # if we can solve for any vectors, ignore the solvable non-vectors for now.
        sol_vecs     = [[q1 for q1 in sols if is_vector(q1)] for sols in sol_quants]
        if any(len(svs)>0 for svs in sol_vecs):
            sol_quants = sol_vecs   # there is at least one vector which we can solve for.
        # argsort for complexities.
        argsorted    = tools.argsort(complexities)
        # loop through equations, starting with the least complicated;
        #   solve for the quant in that equation which appears in the fewest number of other equations.
        for a in argsorted:
            if len(sol_quants[a]) == 0:
                continue     # can't solve for anything in this equation; skip it.
            elif len(sol_quants[a]) == 1:
                return (unsolved[a], sol_quants[a][0])   # only one choice for what to solve for in this equation; choose it.
            # else: choose the solvable quant from this equation which appears in the fewest unsolved equations.
            tallies = []
            for x1 in sol_quants[a]:   # loop through *solvable* o1 quants from this equation
                tally = 0
                for b in filter(lambda b_: a!=b_, argsorted):
                    if x1 in o1_quants[b]:  # check against *all* o1 quants from other equations.
                        tally += 1
                tallies.append(tally)
                assert tally > 0, "x1 'appears only 1 time' should have been handled in the 'has only 1 o1 quant' case. at x={}".format(x1)
            chooser = min if (choice_mode in (0, '0', 'fewest')) else max
            t = chooser(range(len(tallies)), key=lambda i: tallies[i])   # t = argmin(tallies)
            return (unsolved[a], sol_quants[a][t])
        # if we got this far, we don't know how to solve this system (nobody has any sol_quants)
        raise SystemTooComplicatedError("System too hard to solve:\n{}".format(self))

    @_componentize_if_too_complicated
    def o1solvestep(self, show_work=False, simplified=True, basis=None, choice_mode=0, **kw__solve):
        '''steps equation system towards being solved (for o1 terms).
        if show_work, also "shows the steps".

        if basis is provided,
            and we get stuck with unsolvable vector equation(s),
            use basis to break things into components.
            To generate basis, see e.g. Basis() or OrthonormalBasis3D().
        choice_mode: 0 or 1
            0 --> 'fewest'; prioritize solving for quants which appear in the fewest number of unsolved equations.
            1 --> 'most'; prioritize solving for quants which appear in the greatest number of unsolved equations.
        '''
        disprel_eq = None
        explain = tools.debug_viewer(show_work)
        eqns = list(self)
        # choose which equation to solve
        i, selected_q1 = self._o1solvestep_choices(choice_mode=choice_mode)
        # if we are at the last equation, go to Ax = 0 then report A = 0 as the solution.
        if len(self[i].list_o1_symbols()) == 1:
            eqns[i] = eqns[i].o1solve_simple(show_work=show_work, basis=basis, **kw__solve)
            explain('Getting the dispersion relation from equation ({}):'.format(i), view=eqns[i])
            disprel_eq = i
        else:
            # solve i_eq for selected_q1
            explain('Solving equation ({}) for {}', format=(i, selected_q1), end='', flush=True)
            eqns[i] = eqns[i].solve(selected_q1, **kw__solve)
            if simplified:
                explain(' (then simplifying equation ({}))'.format(i), end='', flush=True)
                eqns[i] = eqns[i].simplified(assumptions=self.assumptions, **kw__solve)
            explain(':', view=eqns[i])
            # substitute that solution into all the other equations
            for j, eqn in enumerate(eqns):
                if (j != i) and (j not in self.solved) and (eqn._contains_deep_(selected_q1)):
                    eqns[j] = eqn.substitute(eqns[i])
                    explain('Plugging into equation ({}) the value from equation ({}) for {}', format=(j, i, selected_q1))
                    if simplified:
                        eqn_orig = eqns[j]
                        explain('Then simplifying equation ({})'.format(j))
                        eqns[j] = eqn_orig.simplified(assumptions=self.assumptions, **kw__solve)
                        #if eqns[j] is not eqn_orig:   # simplified() changed at least 1 thing.
                        #    explain('Then simplifying equation ({}).'.format(j))
                        #  # << changed to print before simplifying instead,
                        #  #    mainly in case we stall while simplifying.
        # track which eqns are "solved"
        solved = self.solved + [i]
        # return new EquationSystem
        result = self._new(*eqns, solved=solved)
        if show_work:
            explain('--- The updated list of equations is ---', view=_str(result, index_from=0, **kw__solve))
        if disprel_eq is not None:
            result._disprel_equation_index = disprel_eq
        return result

    def o1solve(self, show_work=False, keep_all=False, basis=None, print_freq=2, **kw__solvestep):
        '''solves self for dispersion relation. Eliminates o1 quants.'''
        # check if we have enough constraints compared to the number of quants to solve for.
        updater = tools.ProgressUpdatePrinter(print_freq, wait=True)  # wait until print_freq before first print.
        Ndim       = 3 if basis is None else len(basis)
        self.warn_if_trapped(self.list_o1_symbols(), Ndim)
        # begin solving
        self._solvesteps = []
        orig_self = self
        while (len(self.solved) != len(self)):
            # if we already know the dispersion relation, warn there is too much info, and exit loop.
            updater.print('Still working.. solved {} of {}'.format(len(self.solved), len(self)), print_time=True)
            if (getattr(self, '_disprel_equation_index', None) is not None):
                extra_info_warning = (
                    'EquationSystem (id=<{}>) contains unnecessary equations. '
                    'I found the dispersion relation without ever using equation(s): {}'
                    ).format(hex(id(self)), self.unsolved)
                warnings.warn(extra_info_warning)
                break
            # otherwise, solve one more equation and plug results into the unsolved equations.
            else:
                self = self.o1solvestep(show_work=show_work, basis=basis, **kw__solvestep)
                orig_self._solvesteps.append(self)
        updater.finalize()
        if keep_all:
            return self
        else:
            return self[self._disprel_equation_index]

    def check_o1_freedom(self, Ndim=3, unsolved_only=True):
        '''returns number of free dof in o1 quants, number of constraints from self'''
        o1_free = self.list_o1_symbols(unsolved_only=unsolved_only)
        return self.check_freedom(o1_free, Ndim, unsolved_only=unsolved_only)

    def list_o1_symbols(self, unsolved_only=True):
        '''lists o1 symbols from the unsolved equations of self.'''
        unsolved = self.unsolved if unsolved_only else range(len(self))
        return list(tools.Set([sym for i in unsolved for sym in self[i].list_o1_symbols()]))


class Inequality(PARENT.Inequality, BinaryOperation):
    pass

''' --------------------- Vector Operations --------------------- '''

class BinaryVectorProduct(PARENT.BinaryVectorProduct):
    def linearize(self, drop0=True):
        lto0 = [apply(t, 'o0') for t in self]
        lto1 = [apply(t, 'o1', default=0) for t in self]
        result = Sum.IDENTITY
        # add 0'th order term
        if not drop0:
            result += self._new(*lto0)
        # add 1st order terms
        result += self._new(lto0[0], lto1[1])
        result += self._new(lto1[0], lto0[1])
        return result

    @property
    def order(self):
        '''tells the order of self.
        equals sum of orders of terms in self, treating order=None as order=0.
        (if all terms in self have order None, return None)
        '''
        return tools.default_sum(*(get_order(t) for t in self), default=(None, 0))


class DotProduct(PARENT.DotProduct, BinaryVectorProduct):
    pass

class CrossProduct(PARENT.CrossProduct, BinaryVectorProduct):
    pass