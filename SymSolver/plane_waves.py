#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 7 2021

@author: Sevans

File purpose: assuming plane waves

inherits mainly from: calculus.py
"""

''' --------------------- Imports --------------------- '''

import warnings

from . import scalars as sc
from .scalars import (
    view, _repr, _str, apply, _simplyfied, _symbol_maker,
    is_constant,
    is_opinstance,
)
from . import vectors as vt
from .vectors import (
    is_vector
)
from . import linearizing as lz
from .linearizing import (
    get_order,
)
from . import calculus as cc
from . import tools
from .tools import (
    RENDER_MATH,
)

''' --------------------- Defaults --------------------- '''

from .calculus import DEFAULT_OPERATIONS

OMEGA = cc.Symbol(r'\omega')
TIME  = cc.Symbol(r't')
#K     = cc.Symbol(r'k')      # TODO: convert to vector
#X     = cc.Symbol(r'x')      # TODO: convert to vector
K     = cc.Symbol(r'k', vector=True)
X     = cc.Symbol(r'x', vector=True)

IUNIT = sc.ImaginaryUnit()  #cc.Symbol(r'i')
#IUNIT = sc.Constant(r'i')  # TODO: use 'i' to represent the number 1j.
#IUNIT._evaluate_numbers = lambda: 1j



''' --------------------- Convenience Functions --------------------- '''


''' --------------------- Basic Objects --------------------- '''

OperationCollection = sc._operation_collection(locals(), DEFAULT_OPERATIONS)

class AbstractOperation(OperationCollection, cc.AbstractOperation):  # TODO: inherit from vectors.py instead
    HIERARCHY = 4
    pass

class Symbol(cc.Symbol, AbstractOperation):
    pass

Symbols = _symbol_maker(Symbol)

class MultipleOperation(AbstractOperation, cc.MultipleOperation):
    def assume_plane_waves(self, omega=OMEGA, t=TIME, k=K, x=X):
        '''assumes first order quantities look like plane waves:
        f^{(1)} --> f^{(1)} exp[i(kx - ωt)]
        '''
        if is_opinstance(self, self.Derivative):
            y = self._y_symbol
            y = apply(y, 'assume_plane_waves')
            y_order = get_order(y)
            if y_order == 1:
                self = self._new(y)
                if not self.partial:
                    raise NotImplementedError("assume_plane_waves for non-partial derivative: {}".format(self))
                if self._x_symbol == x:
                    return self.Product(IUNIT, self.replace_derivative(k))._flatten()
                elif self._x_symbol == t:
                    return self.Product(-1, IUNIT, self.replace_derivative(omega))._flatten()
            elif y_order == lz.MIXED_ORDER:
                warnings.warn("Got mixed order inside derivative. You may want to use .expand(), " +
                              "then try assume_plane_waves() again. For term = \n{}".format(y))
            else:
                return self._new(y)  # return dy/dx.
        #else:
        terms = [apply(term, 'assume_plane_waves') for term in self]
        return self._new(*terms)

# define Sum, Product, Power, Equation, Derivative in this module.
sc.define_operations(globals(), vars(cc),
                     subclasses=[MultipleOperation], operations=DEFAULT_OPERATIONS)



