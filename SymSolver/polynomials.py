#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 28 2021

@author: Sevans

File purpose: Polynomials with arbitrary coefficients.

The polynomials are outside of the "AbstractOperation Hierarchy".
But the AbstractOperations may generate polynomials.

[TODO]
    - make it easier to input parameter values for all the parameters (except e.g. omega)
"""

import functools
import collections   # for defaultdict
import multiprocessing as mp   # for PolyArray.apply

from . import tools
from .tools import (    # put into this namespace for convenience
    view, _str, _repr, is_integer, equals,
    RENDER_MATH,
)

try:
    import numpy as np
except ImportError:
    tools.generate_import_warning('numpy')


''' --------------------- Defaults --------------------- '''

PRINT_FREQ = 2           # number of seconds between updates. (Use -1 to prevent printing.)
TOL_ROOT_CHECK = 1e-2    # root of PolyFrac is BAD if |polyfrac(root)| > TOL_ROOT_CHECK.

''' --------------------- Polynomial class --------------------- '''

def poly_arithmetic(f):
    '''decorates f so that it does some checks before doing the math. for f(s, p):
    - p must be a Polynomial, too
    - s.x and p.x must be compatible; result.x will have the implied x from s and p.
    '''
    @functools.wraps(f)
    def f_poly_arithmetic_form(s, p):
        __tracebackhide__ = True
        assert isinstance(p, Polynomial), "Polynomial can only do math with other Polynomials"
        try:
            x = s.matching_x(p)
        except ValueError:
            raise   # TODO: make clearer message
        result = f(s, p)
        result.x = x   # we need to do this in case s is a constant and p is not.
        return result
    return f_poly_arithmetic_form

class Polynomial():
    '''coeff_dict is a dict of coefficients, with items (power: coefficient).
    E.g. x^2 + 3 x + 7 --> {2: 1, 1: 3, 0: 7}

    x (optional) is the symbol for x. Can be any object.

    Only knows how to do arithmetic with other Polynomial objects.

    TODO: sort by power when showing.
    '''
    ## CREATING ##
    def __init__(self, coef_dict, x=None):
        self.coef = coef_dict.copy()
        self.x = x
        self._remove_coef_0s()

    def _remove_coef_0s(self):
        '''removes all coefs equal to 0 from self.'''
        to_remove = []
        for power, coef in self.coef.items():
            if equals(coef, 0):
                to_remove.append(power)
        for power in to_remove:
            del self.coef[power]

    def _new(self, coef_dict):
        return type(self)(coef_dict, self.x)

    def create_a_constant(self, value):
        '''makes a constant Polynomial.'''
        return self._new({0: value})

    @property
    def MUL_IDENTITY(self):
        return PolyConstant(1)

    ## VISUALIZING ##
    def __repr__(self, **kw):
        xrepr = '' if self.x is None else ', x=' + _repr(self.x, **kw)
        coef_repr = _repr(dict(self.coef))
        return 'Polynomial({}{})'.format(coef_repr, xrepr)

    def __str__(self, enc_coef='()', enc_x='[]', **kw):
        xstr = r' \cdot ' if self.x is None else _str(self.x, **kw)
        if ' ' in xstr:
            xstr = r'\left{} {} \right{}'.format(enc_x[0], xstr, enc_x[1])
        coef_strs = {power: _str(coef, **kw) for power, coef in self.ordered_items(**kw)}
        term_fmt  = r'\left{} {} \right{} {}^{{{}}}'    # [ coef ] x^power
        term_strs = [term_fmt.format(enc_coef[0], cstr, enc_coef[1], xstr, power) for power, cstr in coef_strs.items()]
        return ' + '.join(term_strs)

    def view(self, *args, **kw):
        return view(self, *args, **kw)

    def _ipython_display_(self):
        '''this is the method jupyter notebook calls to figure out how to show self.'''
        return self.view()

    ## PROPERTIES ##
    @property
    def IDENTITY(self):
        return self.create_a_constant(1)

    @property
    def degree(self):
        '''largest power in self. (0 if self is empty)'''
        keys = self.keys()
        if len(keys) == 0:
            return 0
        return max(keys)

    @property
    def inv_degree(self):
        '''smallest power in self. (0 if self is empty)'''
        keys = self.keys()
        if len(keys) == 0:
            return 0
        return min(keys)

    def is_a_constant(self):
        '''returns whether self is a constant (has 0 terms, or only a term with x^0)'''
        keys = list(self.keys())
        return (len(keys) == 0) or ((len(keys) == 1) and (keys[0] == 0))

    @property
    def type(self):
        return type(self)

    def matching_x(s, p):
        '''returns the x which matches self (==s) and p.
        if both aren't constant, requires self.x == p.x.
        if one is constant, return x from the other.
        if both are constant, return x from self.
        if x doesn't match in self and p, raise ValueError.
        '''
        s_const = s.is_a_constant()
        p_const = p.is_a_constant()
        if s_const and p_const:
            return s.x
        elif s_const and (not p_const):
            return p.x
        elif (not s_const) and p_const:
            return s.x
        else: #s_const and p_const
            if s.x == p.x:
                return s.x
            else:
                raise ValueError("s.x != p.x.\ns.x={} \np.x={}".format(s.x, p.x))

    ## CONVENIENT BEHAVIOR ##
    def __iter__(self):
        return iter(self.coef)

    def __len__(self):
        return len(self.coef)

    def __getitem__(self, key):
        return self.coef[key]

    def items(self):
        return self.coef.items()

    def keys(self):
        return self.coef.keys()

    def values(self):
        return self.coef.values()

    def ordered_items(self, descending=True, **kw__None):
        '''return (power, coef) pairs in self, in descending order of power.'''
        argsort = tools.argsort(list(self.keys()), reverse=descending)
        itemlist = list(self.items())
        return [itemlist[i] for i in argsort]

    def sorted(self, descending=True, **kw__None):
        '''sorts self in descending order of power.'''
        keys, values = list(self.keys()), list(self.values())
        argsort = tools.argsort(keys, reverse=descending)
        return self._new({keys[i]:values[i] for i in argsort})

    def __eq__(s, p):
        if p is s:
            return True
        if s.degree != p.degree:
            return False
        if len(s) != len(p):
            return False
        if s.is_a_constant():
            if p.is_a_constant():
                return s.values()[0] == p.values()[0]
            else:
                return False
        if s.x != p.x:
            return False
        for power in s.keys():
            if s[power] != p[power]:
                return False
        return True

    ## ARITHMETIC ##
    @poly_arithmetic
    def __add__(s, p):
        newcoefs = collections.defaultdict(lambda: 0)
        for poly in (s, p):
            for power, coef in poly.items():
                newcoefs[power] = newcoefs[power] + coef
        return s._new(newcoefs)

    @poly_arithmetic
    def __mul__(s, p):
        newcoefs = collections.defaultdict(lambda: 0)
        for ppower, pcoef in p.coef.items():
            for spower, scoef in s.coef.items():
                newcoefs[ppower + spower] = newcoefs[ppower + spower] + pcoef * scoef
        return s._new(newcoefs)

    def __pow__(self, n):
        newcoefs = collections.defaultdict(lambda: 0)
        if len(self) == 1:
            (power, coef), = self.items()
            newcoefs[power * n] = coef ** n   # (C x^p)^n --> C^n x^(p * n)
            return self._new(newcoefs)
        if not isinstance(n, int) or n < 0:
            flipping_multidegree_errmsg = ("Can't raise multi-degree polynomial to a negative power." 
                                           "\nWas trying to raise to {} this polynomial: {}".format(n, self))
            raise TypeError(flipping_multidegree_errmsg)
        elif n == 0:
            return self.IDENTITY
        else:
            # TODO: make more efficient via pascal's triangle, or something like that.
            result = self.IDENTITY
            for i in range(n):
                result *= self
            return result

    def __sub__(s, p):
        return s + s.create_a_constant(-1) * p

    def __truediv__(s, p):
        return s * p**(-1)

    ## EVALUATE ##
    def evaluate(self, at=None):
        '''does the additions, multiplications, and exponentiations implied by self.
        e.g. Polynomial({0:1, 2:3}, x=z) --> 1 + 3 * z**2

        if at is provided, evaluate at the value provided. (use that value instead of self.x)
        '''
        result = 0
        # do the evaluation work
        x = at if at is not None else self.x
        for power, coef in self.items():
            if power == 0:
                result += coef
            else:
                result += coef * (x ** power)
        # bookkeeping - try to set result._polynomial to self.
        try:
            result._polynomial = self
        except AttributeError:
            pass
        return result

    def __call__(self, value):
        '''plugs value in for self.x.'''
        return self.evaluate(at=value)

    def to_numpy(self, **kw):
        '''converts self into an array of numpy polynomials.
        if all coefs are scalars, the output will have shape ().
            Note: to get the scalar from an array with shape (), use [()]. E.g. np.array(7)[()] --> 7
        otherwise, the output will have the shape implied by numpy broadcasting rules.
            (The coefficients must be broadcastable with each other)

        TODO: debug: using broadcast_arrays and broadcast_shape instead of broadcast. broadcast can only handle up to 32 inputs.
        '''
        coefs = self.numpy_coef_list()
        # broad = np.broadcast(*coefs)
        # result = np.empty(broad.shape, dtype=object)
        # result.flat = [np.polynomial.Polynomial(cfs, **kw) for cfs in broad]
        result_shape = np.broadcast_shapes(*[np.shape(c) for c in coefs])
        result = np.empty(result_shape, dtype=object)
        coef_arrays = np.broadcast_arrays(coefs)
        result.flat = [np.polynomial.Polynomial(c, **kw) for c in coef_arrays]
        return result

    def numpy_coef_list(self):
        '''returns list of coefficients which could be passed to numpy.polynomial.Polynomial.
        There, the convention is [c0, c1, ..., cN], where the polynomial is c0 + c1 x + ... + cN x^n.
        '''
        # ensure that powers are ok.
        def power_is_ok(power):
            return is_integer(power) and (power >= 0)
        try:
            badpower = next(power for power in self.keys() if not power_is_ok(power))
        except StopIteration:
            pass
        else:  # there was a bad power, so crash.
            bad_power_errmsg = ("All powers must be integers >= 0 to convert to numpy polynomial, "
                                "but at least one power doesn't follow these rules: {}".format(badpower))
            raise ValueError(bad_power_errmsg)
        # create coef list.
        result = [0 for i in range(self.degree + 1)]
        for power, coef in self.items():
            result[power] = coef
        return result

    def to_polyarray(self):
        return PolyArray(self.to_numpy())

    def roots(self):
        '''returns roots of self, by converting to numpy polynomial.
        Only works if all coefficients of self are numerical
            (e.g. not AbstractOperation objects, not arrays).
        '''
        nparr = self.to_numpy()
        if nparr.ndim > 0:
            raise TypeError("Don't know how to find roots of a {}-dimensional polynomial (only know 0-dim)".format(nparr.ndim))
        else:
            return nparr[()].roots()

    def sorted_roots(self, key=lambda roots: roots):
        '''returns roots of self, sorted by key.'''
        roots = self.roots()
        return roots[np.argsort(key(roots))]

    def imag_sorted_roots(self):
        '''returns roots of self, sorted by imaginary part (with largest first).'''
        return self.sorted_roots(key=lambda roots: -np.imag(roots))

    ## MANIPULATE ##
    def increment_degree(self, increment):
        '''increase degree of ALL terms in self by increment.'''
        return self._new({power + increment: coef for power, coef in self.items()})

    def monicize(self):
        '''converts self to a polynomial proportional to self but with coefficient = 1 for largest degree term.
        returns (resulting monic polynomial, previous coefficient of leading term).
        '''
        coef_lead = self[self.degree]
        divide_by = self.create_a_constant(coef_lead)
        monic_poly = self / divide_by
        return (monic_poly, coef_lead)


def PolySymbol(x):
    '''returns the polynomial: 1 x^1.'''
    return Polynomial({1: 1}, x)

def PolyConstant(c):
    '''returns the polynomial: c x^0'''
    return Polynomial({0: c})

IDENTITY = PolyConstant(1)
POLY_MUL_IDENTITY = PolyConstant(1)


''' --------------------- Solving an array of polynomials --------------------- '''

class PolyArray():
    '''class for holding and solving an array of polynomials.
    Can handle numpy.polynomial.Polynomial, or any other object;
        the "solve" methods are not implemented here,
        but should instead be passed as an argument to PolyArray.apply,
        which will apply the "solve" method to each polynomial in self,
        using multiprocessing when possible/reasonable, and returning the array of solutions.
    '''
    ## CREATION ##
    def __init__(self, polyarr):
        self.polyarr = polyarr

    def _new(self, *args, **kw):
        return type(self)(*args, **kw)

    ## MISC. PROPERTIES ##
    arr = polyarray = property(lambda self: self.polyarr, doc='alias to polyarr attribute')

    ## ARRAY PROPERTIES ##
    shape = property(lambda self: np.shape(self.polyarr), doc='np.shape of polyarr')
    size  = property(lambda self: np.size(self.polyarr),  doc='np.size of polyarr')
    ndim  = property(lambda self: np.ndim(self.polyarr),  doc='np.ndim of polyarr')
    dtype = property(lambda self: np.dtype(self.polyarr), doc='np.dtype of polyarr')

    def __iter__(self):
        '''iterates through self.polyarr, yielding (value, multi_index)'''
        return tools.itarrayte(self.polyarr)

    def __getitem__(self, i):
        result = self.polyarr[i]
        if np.ndim(result) > 0:
            return self._new(result)
        else:
            return result

    def empty(self, **kw):
        return np.empty(self.shape, **kw)

    ## SOLVING / APPLY (WITH MULTIPROCESSING) ##
    def ncpuse_mp(self, ncpu=None):
        '''returns number of cpu to use for multiprocessing,
        or False if we should not use multiprocessing.
        '''
        if self.size == 1:
            return False
        if ncpu is None:
            ncpu = mp.cpu_count() // 2
        if (not ncpu) or (ncpu == 1):
            return False
        return ncpu

    def apply(self, f, *args__f, ncpu=None, dtype='object', print_freq=PRINT_FREQ, **kw__f):
        '''apply f individually to each of the polynomials in self.

        f: function or string
            apply this function to each polynomial, p, in self.
            function --> evaluate f(p, *args__f, **kw__f)
            string   --> evaluate p.f(*args__f, **kw__f)
                (Note: not duck-typed. We check isinstance(f, str).)
        ncpu: None (default) or number
            number of cpus to use. Special cases:
            ncpu == None   --> ncpu = mp.cpu_count() // 2
            ncpu == 1      --> don't use multiprocessing
            self.size == 1 --> don't use multiprocessing
        dtype:
            dtype for return array

        returns: an array with same shape as self, containing the results.
        '''
        # preprocessing - f convenience
        _f = _Pickleable_call__f_or_getattr_f(f)
        # preprocessing - bookkeeping
        result  = self.empty(dtype=dtype)
        size    = self.size
        updater = tools.ProgressUpdatePrinter(print_freq=print_freq)
        ncpu    = self.ncpuse_mp(ncpu=ncpu)
        # SINGLE PROCESSOR CASE #
        if ncpu is False:
            for i, (p, midx) in enumerate(self):   # midx stands for 'multi-index'
                updater.print('Applying {}(); {} of {}.'.format(_f.__name__, i+1, size), print_time=True)
                result[midx] = _f(p, *args__f, **kw__f)
        # MULTIPROCESSING CASE #
        else:
            with mp.Pool(processes=ncpu) as pool:
                pool_array = self.empty(dtype='object')
                # set up the pool to run asynchronously
                for i, (p, midx) in enumerate(self):
                    updater.print('Setting up pool; {} of {}.'.format(i+1, size), print_time=True)
                    args_i = (p, *args__f)
                    pool_array[midx] = pool.apply_async(_f, args=args_i, kwds=kw__f)
                # tell the pool workers to do their jobs.
                for i, (p, midx) in enumerate(self):
                    updater.print('Calculating {}(); {} of {}.'.format(_f.__name__, i+1, size), print_time=True)
                    result[midx] = pool_array[midx].get()
        updater.finalize(process_name=_f.__name__)
        return result


class _Pickleable_call__f_or_getattr_f():
    '''returns a pickleable object which will call f(p, ...) (if function) or getattr(p, f)(...) (if string).

    (multiprocessing requires the input function to be pickleable, and
        locally-defined functions are NOT pickleable,
        but objects defined at top level of module ARE pickleable.)

    This helper "function" is utilized by PolyArray.apply().
    '''
    def __init__(self, f):
        self.f = f
        self.strmode = isinstance(f, str)
        self.__name__ = f if self.strmode else f.__name__

    def __call__(self, p, *args, **kw):
        if self.strmode:
            return getattr(p, self.f)(*args, **kw)
        else:
            return self.f(p, *args, **kw)

def array_output_format(result):
    '''returns result if result is not 0-d. Else, result[()].'''
    try:
        shape = result.shape
    except AttributeError:
        return result
    else:
        if len(shape) > 0:
            return result
        else:
            return result[()]

def val_with_max_imag(roots):
    '''returns the root with the largest imaginary part.
    returns np.nan if roots is empty.
    '''
    imag   = np.imag(roots)
    if np.size(imag) == 0:
        return np.nan
    idxmax = np.unravel_index(imag.argmax(), roots.shape)
    return roots[idxmax]

def omega_maxgrow(p):
    '''return the root from p with the largest imaginary part.'''
    return val_with_max_imag(p.roots())

def root_max_real(p):
    '''return the root from p with the largest real part'''
    roots = p.roots()
    return roots[np.argmax(np.real(roots))]

def root_min_real(p):
    '''return the root from p with the smallest real part'''
    roots = p.roots()
    return roots[np.argmin(np.real(roots))]


''' --------------------- PolyFrac class --------------------- '''

def polyfrac_arithmetic(f):
    '''decorates f so that it does some checks before doing the math. for f(s, p):
    - p must be a PolyFrac, too
    '''
    @functools.wraps(f)
    def f_polyfrac_arithmetic_form(s, p):
        __tracebackhide__ = True
        assert isinstance(p, PolyFrac), "PolyFrac can only do math with other PolyFracs"
        result = f(s, p)
        return result
    return f_polyfrac_arithmetic_form

class PolyFrac():
    '''fraction of two polynomials.'''
    def __init__(self, numer, denom=POLY_MUL_IDENTITY, _tol_root=TOL_ROOT_CHECK):
        self.numer = numer
        self.denom = denom
        # bookkeeping
        self._tol_root = _tol_root

    def _new(self, *args, **kw):
        kw_new = dict(_tol_root=self._tol_root)
        kw_new.update(kw)
        return type(self)(*args, **kw_new)

    def create_a_constant(self, value):
        '''makes a constant PolyFrac'''
        return self._new(PolyConstant(value))

    @property
    def MUL_IDENTITY(self):
        return self.create_a_constant(1)

    ## VISUALIZING ##
    def __repr__(self, **kw):
        return 'PolyFrac(numer={}, denom={})'.format(self.numer, self.denom)

    def __str__(self, *args, **kw):
        return r'\frac{{{}}}{{{}}}'.format(_str(self.numer, *args, **kw), _str(self.denom, *args, **kw))

    def view(self, *args, **kw):
        return view(self, *args, **kw)

    def _ipython_display_(self):
        '''this is the method jupyter notebook calls to figure out how to show self.'''
        return self.view()

    ## CONVENIENT BEHAVIOR ##
    def __eq__(s, p):
        '''return (s==p). may produce false negatives, but never produces false positives.'''
        if b is self:
            return True
        return s.numer == p.numer and s.denom == p.denom

    @property
    def degree(self):
        '''return (degree of self.numer, degree of self.denom)'''
        return self.numer.degree, self.denom.degree

    ## ARITHMETIC ##
    @polyfrac_arithmetic
    def __add__(s, p):
        numer = s.numer * p.denom + p.numer * s.denom
        denom = s.denom * p.denom
        return s._new(numer, denom)

    @polyfrac_arithmetic
    def __mul__(s, p):
        numer = s.numer * p.numer
        denom = s.denom * p.denom
        return s._new(numer, denom)

    def __pow__(s, n):
        if n > 0:
            numer = s.numer**n
            denom = s.denom**n
        elif n == 0:
            numer = Polynomial.MUL_IDENTITY
            denom = Polynomial.MUL_IDENTITY
        else: # n < 0
            numer = s.denom**(-n)  # negative n --> flip then raise to |n|.
            denom = s.numer**(-n)
        return s._new(numer, denom)

    def __sub__(s, p):
        return s + s.create_a_constant(-1) * p

    def __truediv__(s, p):
        return s * p**(-1)

    ## EVALUATE ##
    def evaluate(self, at=None):
        '''does the operations implied by self.'''
        numer = self.numer.evaluate(at=at)
        denom = self.denom.evaluate(at=at)
        return numer / denom

    def __call__(self, value):
        '''plugs value into self.numer and self.denom, and divides the two.'''
        return self.evaluate(at=value)

    def to_array(self, print_freq=2):
        '''converts self into an array of PolyFracs.
        if all coefs are scalars, the output will have shape ().
            Note: to get the scalar from an array with shape (), use [()]. E.g. np.array(7)[()] --> 7
        otherwise, the output will have the shape implied by numpy broadcasting rules.
            (The coefficients must be broadcastable with each other)

        NOTE: not really compatible with masked arrays.
        Instead of using masked arrays, it is recommended to index the input arrays by the mask.
        '''
        updater = tools.ProgressUpdatePrinter(print_freq, wait=True)
        # bookkeeping related to broadcasting
        updater.print('doing bookkeeping / broadcasting for to_array()')
        nkeys, ncoefs = zip(*self.numer.items())
        dkeys, dcoefs = zip(*self.denom.items())
        len_n         = len(ncoefs)
        broad_arrays = np.broadcast_arrays(*ncoefs, *dcoefs, subok=True)
        ncoefs_broad = np.asarray(broad_arrays[:len_n])
        dcoefs_broad = np.asarray(broad_arrays[len_n:])
        broad_shape  = np.broadcast_shapes(*(c.shape for c in ncoefs_broad), *(d.shape for d in dcoefs_broad))
        assert len(broad_arrays) > 0, "numer and denom both with len 0 is not implemented for PolyFrac.to_array()"
        # get result
        result = np.empty(broad_shape, dtype=object)
        size = result.size
        for ii, (_, idx) in enumerate(tools.itarrayte(broad_arrays[0])):
            updater.print(f'Converting to PolyFrac; {ii:5d} of {size:5d}', print_time=True)
            # put result[idx] equal to the (idx)'th polynomial
            slicer = tuple((slice(None), *idx))
            numer = self.numer._new({key: coef for key, coef in zip(nkeys, ncoefs_broad[slicer])})
            denom = self.denom._new({key: coef for key, coef in zip(dkeys, dcoefs_broad[slicer])})
            result[idx] = self._new(numer, denom)
        updater.finalize(process_name='to_array()')
        return result

    def to_polyarray(self):
        return PolyArray(self.to_array())

    ## MANIPULATE ##
    def rescale(self, value):
        '''multiplies numerator and denominator by value.'''
        if not isinstance(value, Polynomial):
            value = PolyConstant(value)
        numer = self.numer * value
        denom = self.denom * value
        return self._new(numer, denom)

    def shrinkcoef(self):
        '''divide numerator and denominator by min(largest coef in numer, largest coef in denom).

        'largest' is determined by absolute value of the coeffients.
        '''
        cnumer = tools.array_max(list(self.numer.values()), key=np.abs)
        cdenom = tools.array_max(list(self.denom.values()), key=np.abs)
        divideby = tools.array_min([cnumer, cdenom], key=np.abs)
        return self.rescale(1 / divideby)

    def shrinkdegree(self):
        '''divide numerator and denominator by x^(mininum degree from numer and denom)
        E.g. (x^7 + x^3) / (x^6 + x^5) --> (x^4 + x^0) / (x^3 + x^2)
        '''
        deg  = min(self.numer.inv_degree, self.denom.inv_degree)
        pdiv = self.numer._new({deg: 1})   # 1 x^deg
        numer = self.numer / pdiv
        denom = self.denom / pdiv
        return self._new(numer, denom)

    ## FIND ROOTS ##
    def roots(self):
        '''return roots of self; only checks roots of numerator.
        Does NOT check denominator. See root_bad() for checking denominator.

        Note: to evaluate the denominator for an array of roots, use self.denom(roots).
        '''
        return self.numer.roots()

    def sorted_roots(self, key=lambda roots: roots):
        '''returns roots of self, sorted by key.'''
        roots = self.roots()
        return roots[np.argsort(key(roots))]

    def imag_sorted_roots(self):
        '''returns roots of self, sorted by imaginary part (with largest first).'''
        return self.sorted_roots(key=lambda roots: -np.imag(roots))

    def root_bad(self, root, tol=None):
        '''check whether self(root) is greater than tol (in absolute value).
        bool(result) gives:
            False <-- root is "okay",     i.e. self(root) IS close to zero
            True  <-- root is "not okay", i.e. self(root) is NOT close to zero.

        root: any value.
            plugged in to self.
        tol: None (default) or any value.
            None -> use self._tol_root (default = polymanip.TOL_ROOT_CHECK == 1e-8).
            value -> use the value provided

        Returns False if |self(root)| < tol; else returns self(root).
        '''
        if tol is None:
            tol = self._tol_root
        val = self(root)
        if np.isnan(val) or (abs(val) > tol):
            return val  # abs(self(root)) too big. (Sidenote bool(nan)==True.)
        else:
            return False   # abs(self(root)) < tol, so we accept root as a root.

    def growth_root(self, tol=None):
        '''returns the root of self with the largest imaginary part.
        (Only considers 'good' roots.)

        Equivalent to self.first_good_root(tol=tol, sortby=lambda roots: -np.imag(roots))
        '''
        try:
            return self.first_good_root(tol=tol, sortby=lambda roots: -np.imag(roots))
        except numpy.linalg.LinAlgError:
            return np.nan + np.nan * 1j

    def first_good_root(self, tol=None, sortby=lambda roots: roots):
        '''returns first root of self which is a good root.
        "good root" means "absolute value of denominator(root) is greater than tol".

        "first" is with respect to the list of sorted roots.

        sortby: function(roots). Default: roots --> roots.
            function of roots which returns array which numpy will argsort.
            Example: to sort by imaginary part, with largest first, use
                lambda roots: - np.imag(roots)

        Returns root. If no found roots are good, instead returns None.
        '''
        roots = self.roots()
        root_argsort = np.argsort(sortby(roots))
        for i in root_argsort:
            root = roots[i]
            if not self.root_bad(root, tol=tol):
                return root
        return None

    def first_decent_root(self, tol=None, sortby=lambda roots: roots):
        '''returns first root of self which is a good root.
        "good root" means "absolute value of denominator(root) is greater than tol".

        "first" is with respect to the list of sorted roots.

        sortby: function(roots). Default: roots --> roots.
            function of roots which returns array which numpy will argsort.
            Example: to sort by imaginary part, with largest first, use
                lambda roots: - np.imag(roots)

        Returns root.
        If no found roots are good, instead returns "best" root candidate,
            and sets self._fdr_allowance = |self(root_candidate)|.
        '''
        roots = self.roots()
        root_argsort = np.argsort(sortby(roots))
        least_bad_badness = np.inf
        for i in root_argsort:
            root = roots[i]
            badness = self.root_bad(root, tol=tol)
            if not badness:
                return root
            else:
                if badness < least_bad_badness:
                    least_bad_badness = badness
                    least_bad_root    = root
        # if we make it here it means all the roots were bad.
        self._fdr_allowance = np.abs(least_bad_badness)
        return least_bad_root

    def _roots_subset(self, roots, good=True, tol=None, return_idx=False):
        '''tells list of which roots are {strgood}.
        This is the list of roots where bool(root_good) is {boolgood}.

        result = np.array(list of {strgood} roots).
        if return_idx, return (result, idx (in roots list) of roots in result).
        Otherwise (default) return result.
        '''
        result_roots = []
        result_idx   = []
        for i, root in enumerate(roots):
            if (bool(self.root_bad(root, tol=tol)) == (not good)):
                result_roots += [root]
                result_idx   += [i]
        result_roots = np.array(result_roots)
        if return_idx:
            return (result_roots, result_idx)
        else:
            return result_roots

    def roots_good(self, roots, tol=None, return_idx=False):
        return self._roots_subset(roots, good=True, tol=tol, return_idx=return_idx)
    roots_good.__doc__ = _roots_subset.__doc__.format(strgood='good', boolgood='True')

    def roots_bad(self, roots, tol=None, return_idx=False):
        return self._roots_subset(roots, good=False, tol=tol, return_idx=return_idx)
    roots_bad.__doc__  = _roots_subset.__doc__.format(strgood='bad', boolgood='False')

    def roots_careful(self, tol=None):
        '''return roots of self, removing roots where denominator(root) is close to 0.
        "close to 0" means "absolute value is less than tol".

        This is more computationally expensive than self.roots(), since each root
        must be plugged in to the denominator.

        If you only care about finding one root, use self.first_good_root() instead.
        '''
        return self.roots_good(self.roots(), tol=tol, return_idx=False)


def PolyFracSymbol(x):
    '''returns the PolyFrac: 1 x^1 / 1 x^0'''
    return PolyFrac(PolySymbol(x))

def PolyFracConstant(c):
    '''returns the PolyFrac: c x^0 / 1 x^0'''
    return PolyFrac(PolyConstant(c))