#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 28 2021

@author: Sevans

File purpose: Scalars.

In general, use explicit class instantiation to make un-simplified form; use arithmetic to _simplyfy.
    Sum(0, x, y) --> Sum with terms 0, x, y
    0 + x + y --> Sum with terms x, y

TODO:
    - _factor for Sum. (E.g. (x**2 + 2x - 3) --> (x + 3)(x - 1))
    - is_linear_in can give incorrect results.
        - E.g. ((x + x**2) / (x-1)**2).is_linear_in(x) == True  (should be False)
    - ((x + 1) - (x + 1)).simplify() should equal 0. (improve collect in Sum)
        - Note: "smash_zero_pairs" will do this, and is "cheap".
            - still need to test if it is cheap enough to leave "on" by default, in simplify.
        - Note: "collect_associates" will do this, but is costly. e.g. ((x + 1) - (x + 1)).apply("collect_associates")
        - Note: ((x + 1) - (x + 1)).expand().simplify() == 0. And, that's probably good enough!
        - maybe can do a _reassemble simplification operation, which looks for combinations of terms
            who, when put together, have non-1 gcf with other terms in the Sum.
            But, is there any way to do this in a way that wouldnt be 3^n complexity?
            (3^n because for each term, it's either in group A, group B, or neither.)
            (Okay, maybe the complexity is a bit less bad than 3^n... not sure.)
            We could just only try to do such groupings if n is not too large, perhaps.
    - simplify() should probably multiply numerical values FIRST, then factor.
        - e.g. (Product(5,7)*x + Product(5,8)*y).simplify() --> 5 * (7x + 8y).
          but maybe it should go to 35x + 40y, instead.
        For now, the workaround is to run obj.expand().apply('evaluate_numbers').simplify()
        The reason for this issue is simplify calls all ops one tier at a time.
        Since Sum is top tier, it will look like:
            Sum._evaluate_numbers --> doesn't do anything; summands are Product objects, not numbers.
            Sum._collect --> factors out the common "5".
        Meanwhile, apply('evaluate_numbers') will evaluate at all tiers before running any other ops.
"""

''' --------------------- Imports --------------------- '''
# import built-in packages
import functools
import warnings
import math   # currently, for nan in case of (x^y).degree(y), and gcd() for Rational.
import collections # currently, just for namedtuple for define_operations
import itertools

# import internal modules
from . import tools
from .tools import (    # put into this namespace for historical reasons
    view, _str, _repr, apply,
    RENDER_MATH,
)
from . import abstract_operations as ao
from .abstract_operations import (
    _simplyfied, is_constant, is_number,
    is_opinstance,
    equals, _equals0,
    # for historical reasons, also put into this namespace:
    _operation_collection, define_operations,
    _symbol_maker,
)
from . import constants as cn
from .constants import (   # for historical reasons:
    ImaginaryUnit, Rational,
)

from . import polynomials as poly

''' --------------------- DEFAULTS --------------------- '''

TRACEBACKHIDE = tools.TRACEBACKHIDE
DEFAULT_OPERATIONS = ao.DEFAULT_OPERATIONS + ['LinearOperation', 'MultiSum',
                                              'Equation', 'EquationSystem', 'Inequality']
CACHING_PROPERTIES = True  # whether to cache simple properties:
                           #   _is_fraction, _internal_fraction_layers, ...

DIV0_TRACKING = False    # Whether to track the assumption that we are not dividing by 0.


''' --------------------- Convenience Functions --------------------- '''

def get_base_and_power(x):
    '''returns base, power for x.
    if x is a Power object, returns x[0], x[1]. Else, return x, 1.
    '''
    if is_opinstance(x, Power):
        return x[0], x[1]
    else:
        return x, 1

def get_factors(x):
    '''returns list of factors for x.
    if x is a Product object, returns list(x). Else, return [x].
    '''
    if is_opinstance(x, Product):
        return list(x)
    else:
        return [x]

def get_summands(x):
    '''returns the list of summands for x.
    if x is a Sum object, returns list(x). Else, return [x].
    '''
    if is_opinstance(x, Sum):
        return list(x)
    else:
        return [x]

def gcf(x, y):
    '''returns (the gcf between x and y, x/gcf, y/gcf).
    Prefers to use x.gcf() if it exists; otherwise tries y.gcf().
    If x and y both do not have 'gcf' attribute,
        if x == y, return (x, 1, 1),
        otherwise, return (1, x, y).
    '''
    if hasattr(x, 'gcf'):
        f, x_over_f, y_over_f = x.gcf(y)
    elif hasattr(y, 'gcf'):
        f, y_over_f, x_over_f = y.gcf(x)
    elif equals(x, y):
        f, x_over_f, y_over_f = (x, 1, 1)
    else:
        f, x_over_f, y_over_f = (1, x, y)
    return (f, x_over_f, y_over_f)

def _list_contains_deep_(l, x):
    '''tells whether any term in l equals to x, or _contains_deep_ x.'''
    for elem in l:
        if equals(elem, x):
            return True
        if apply(elem, '_contains_deep_', x, default=False):
            return True
    return False

def _solubility_listing(f):
    '''wraps solubility_list function(self, x) to make some quick checks first:
        self==0 --> return []
        self==x --> return [x]
        not self._contains_deep_(x) --> return [None]
    '''
    @functools.wraps(f)
    def solubility_list__wrapped(self, x, *args, **kw):
        __tracebackhide__=TRACEBACKHIDE
        if self==0:
            return []
        elif self==x:
            return [x]
        elif not self._contains_deep_(x):
            return [None]
        else:
            return f(self, x, *args, **kw)
    return solubility_list__wrapped

def solubility_list(s, x):
    '''returns the list of least-soluble-in-x terms from self.
    nonzero terms which do not contain x will be represented as None.
    '''
    result = apply(s, 'solubility_list', x, default=None)
    if result is None:
        if equals(s, 0):
            return []
        else:
            return [None]
    else:
        return result


''' --------------------- AbstractOperation --------------------- '''

OperationCollection = ao._operation_collection(locals(), DEFAULT_OPERATIONS)

class AbstractOperation(OperationCollection, ao.AbstractOperation):
    '''AbstractOperation. Basically all SymSolver objects should inherit from this class.
    Contains rules for +, -, *, /, ^, __iter__, __call__, and more.
    '''
    # many of the rules are inherited from ao.AbstractOperation.
    HIERARCHY = 0

    ## CONVENIENCE ##
    def degree(self, x, upper=True):
        '''returns degree of x in self. if upper, return largest degree; else return smallest degree.'''
        raise NotImplementedError("{}.degree".format(type(self)))

    def is_linear_in(self, x):
        '''returns whether self is linear in x.'''
        deg_upper = self.degree(x, upper=True)
        deg_lower = self.degree(x, upper=False)
        return (deg_upper in (0,1)) and (deg_lower in (0,1))

    def _internal_fraction_layers(self):
        '''returns max number of layers of fractions contained in self.
        (Main purpose is for displaying a nice amount of fractions in __str__.)
        '''
        # caching
        if CACHING_PROPERTIES:
            try:
                return self._cached_internal_fraction_layers
            except AttributeError:
                pass  # didn't have cached value.
        # result
        if len(list(self)) == 0:
            return 0
        result = max(apply(t, '_internal_fraction_layers', default=0) for t in self)
        if self._is_fraction():
            result += 1
        # caching
        if CACHING_PROPERTIES:
            self._cached_internal_fraction_layers = result
        # result
        return result

    def _is_fraction(self):
        '''returns whether self is a fraction.
        (i.e. a Product with at least one term which is a Power with a negative exponent).
        '''
        # caching
        if CACHING_PROPERTIES:
            try:
                return self._cached_is_fraction
            except AttributeError:
                pass  # didn't have cached value
        # result
        result = is_opinstance(self, Product) and any(isinstance(t, Power) and t[1] < 0 for t in self)
        # caching
        if CACHING_PROPERTIES:
            self._cached_is_fraction = result
        # result
        return result

    ## MULTISUM ##
    def multisum(self, *args, **kw):
        '''shorthand for MultiSum(self, *args, **kw)'''
        return self.MultiSum(self, *args, **kw)

    ## POLYNOMIALS ##
    def polynomial(self, x):
        '''returns self as Polynomial in x'''
        if self._contains_deep_(x):
            raise NotImplementedError("{}.polynomial()".format(type(self)))
        else:
            return poly.PolyConstant(self)

    def polyfrac(self, x):
        '''returns self as PolyFrac in x.'''
        if self._contains_deep_(x):
            raise NotImplementedError("{}.polyfrac()".format(type(self)))
        else:
            return poly.PolyFracConstant(self)

    def polynomialize(self, x, descending=True, monic=False, _monic_internal=False, **kw):
        '''convert self to Polynomial in x, then back to AbstractOperation.
        if monic, return a product of k * polynomial, where k is the leading coefficient,
            (and all the other coefficients are divided by k).
            if _monic_internal, return (k, polynomial), instead.
        '''
        p = self.polynomial(x)
        if descending is not None:
            p = p.sorted(descending=descending, **kw)
        if monic:
            p, k = p.monicize()
            result = p.evaluate()
            if _monic_internal:
                return (k, result)
            else:
                return self.Product(k, result)
        else:
            result = p.evaluate()
            return result

    def polyfractize(self, x, shrinkcoef=False, shrinkdegree=False):
        '''convert self to PolyFrac in x, then back to AbstractOperation.
        if shrinkcoef, run PolyFrac.shrinkcoef().
        if shrinkdegree, run PolyFrac.shrinkdegree().
        '''
        p = self.polyfrac(x)
        if shrinkcoef:
            try:
                p = p.shrinkcoef()
            except TypeError:  # (can't determine 'min' or 'max' of coefs. Maybe they contain an AbstractOperation.)
                pass
        if shrinkdegree:
            try:
                p = p.shrinkdegree()
            except TypeError:  # (can't determine 'min' or 'max' of powers. Maybe they contain an AbstractOperation.)
                pass
        result = p.evaluate()
        return result

    def polysimplified(self, x, factor_smallest_degree=True, **kw):
        '''make polynomial with simplified coefficients.
        if factor_smallest_degree, divides all terms by x^n,
        where n is the degree of the smallest term in the polynomial.
        '''
        nomial = self.polynomial(x)
        nomial = nomial._new({power: apply(coef, 'simplified', **kw) for power, coef in nomial.items()})
        result = None
        if factor_smallest_degree:
            n = nomial.inv_degree
            if n != 0:
                nomial = nomial.increment_degree(-1 * n)
                result = x**n
        p = nomial.evaluate()
        if result is None:
            result = p
        else:
            result = self.Product(p, result)
        return result

    ## SOLVING ##
    @_solubility_listing
    def solubility_list(self, x):
        '''returns the list of solubilities for self.
        If self contains no Sums, this list will have length 0 or 1.
        This function (implemented in AbstractOperation) is a placeholder, mostly,
        except that it will make the following checks first:
            self==x --> return [x]
            self==0 --> return [].
            not self._contains_deep_(x) --> return [None].
        '''
        raise NotImplementedError('{}.solubility_list()'.format(type(self)))

    def solvable(self, x):
        '''tells whether (or, how) we can solve for x.
        CAN solve for x   --> bool(result) == True
        CAN'T solve for x --> bool(result) == False

        The method from the AbstractOperation class will just return True or False.
        Subclasses may overwrite it to give other values, see e.g. the Equation class.
        '''
        solub = self.solubility_list(x)
        solvables = (tools.Set([x, None]), tools.Set([x]))   # we can solve for x in either of these cases.
        return ( tools.Set(solub) in solvables )

    def soluble(self, x):
        '''returns whether self does not prevent solving for x.
        When self _contains_deep_ x, this is equivalent to bool(self.solvable(x)).
        When self doesn't contain x, return True.
        '''
        return (not self._contains_deep_(x)) or bool(self.solvable(x))

    def least_soluble_in(self, x):
        '''returns the least complicated term in self containing x which is still soluble.
        Examples:
            7 x + y --> x.
            y * (x dot k) + 7 --> x dot k.
            y + 2 --> None    # (y + 2 doesn't contain x, so we return None)
        '''
        if self == x:
            return self
        candidate = None
        if self._contains_deep_(x):
            if not self.soluble(x):
                return self
            for t in self:
                s = apply(t, 'least_soluble_in', x, default=None)
                if s is None:
                    continue   # x not in s, so we don't care about s.
                else:
                    if candidate is None:
                        candidate = s
                    elif candidate == s:
                        continue
                    else:
                        raise NotImplementedError("Differing candidates for least_soluble_in: {}, {}".format(candidate, s))
        return candidate


''' --------------------- Basic Objects --------------------- '''

class Symbol(ao.Symbol, AbstractOperation):

    def degree(self, x, upper=True):
        '''returns degree of x in self. if upper, return largest degree; else return smallest degree.
        (1 if x == self; 0 otherwise)
        '''
        return int(x == self)

    def polynomial(self, x):
        if self == x:
            return poly.PolySymbol(self)
        else:
            return poly.PolyConstant(self)

    def polyfrac(self, x):
        if self == x:
            return poly.PolyFracSymbol(self)
        else:
            return poly.PolyFracConstant(self)

Symbols = _symbol_maker(Symbol)


class MultipleOperation(ao.MultipleOperation, AbstractOperation):
    def _first_denominator_appearance(self, x):
        '''returns first denominator in self in which x appears.'''
        for t in self:
            result = apply(t, '_first_denominator_appearance', x, default=None)
            if result is not None:
                return result
        return None

class CommutativeOperation(ao.CommutativeOperation, MultipleOperation):
    pass

class AssociativeOperation(ao.AssociativeOperation, MultipleOperation):
    pass

class BinaryOperation(ao.BinaryOperation, MultipleOperation):
    pass


''' --------------------- Operations - Power --------------------- '''

class Power(BinaryOperation):
    OPTYPE = 'Power'

    @property
    def OPERATION(self):
        return lambda base, exp: base ** exp

    def __str__(self, internal=False, enc='()', **kw):
        '''string of self. Put in enc if internal.'''
        if internal and not issubclass(internal, (Sum, Product)):
            return r'\left{} {} \right{}'.format(enc[0], _str(self, internal=False, **kw), enc[1])
        else:
            if isinstance(self.t1, AbstractOperation):
                t1str = _str(self.t1, internal=type(self), **kw)
            else:
                t1str = r'\left( {} \right)'.format(self.t1)  # put numbers in parens, e.g. (7)^2
            t2str = _str(self.t2, internal=False, **kw)
            return r'{}^{{{}}}'.format(t1str, t2str)

    def _equals0(self):
        '''returns whether self == 0.'''
        return _equals0(self.t1)

    def gcf(self, b):
        '''return (gcf= the "greatest" common factor of self and b, self/gcf, b/gcf).
        gcf(x**2, x**-3) == (x**-3, x**5, 1)    # if the exponents can be compared (via '<'),
        gcf(x**-3, x**2) == (x**-3, 1, x**2)    #   use the term with the smallest exponent as the gcf.
        gcf(x**y, x**z) == (x**y, 1, x**(z-y))  # if '<' is not supported between the exponents,
        gcf(x**z, x**y) == (x**z, 1, x**(y-z))  #   use the first term as the gcf.
        '''
        if equals(self, b):
            return (self, 1, 1)
                                                         # example corresponds to gcf((x * y)**5, (k * y)**2)
        bbase, bpower = get_base_and_power(b)            # e.g. ((k * y), 2)
        sbase, spower = self.t1, self.t2                 # e.g. ((x * y), 5)
        gbase, sbaserem, bbaserem = gcf(sbase, bbase)    # e.g. (y, x, k)
        if gbase == 1:
            return (1, self, b)   # gcf is 1; we can't do any good simplifications.
        # gcf is not 1. Deal with exponents.
        try:
            gexp = min(spower, bpower)               # e.g. 2
        except TypeError:   # can't evaluate "spower < bpower"
            gexp = spower   # so we just choose spower.
        sexp = spower - gexp                         # e.g. 3
        bexp = bpower - gexp                         # e.g. 0
        gg = gbase ** gexp                           # e.g. y**2
        ss = (sbaserem ** spower) * (gbase ** sexp)  # e.g. x**5 * y**3
        bb = (bbaserem ** bpower) * (gbase ** bexp)  # e.g. k**2 * y**0
        return (gg, ss, bb)                          # e.g. (y**2, x**5 * y**3, k**2)

    def degree(self, x, upper=True):
        '''returns degree of x in self. if upper, return largest degree; else return smallest degree.'''
        if apply(self.t2, '_contains_deep_', x, default=False):
            return math.nan   # x in exponent.
        elif apply(self.t1, '_contains_deep_', x, default=False):
            return self.t1.degree(x, upper=upper) * self.t2
        else:
            return 0

    def polynomial(self, x):
        '''return self as a Polynomial'''
        if self == x:
            return poly.PolySymbol(x)
        elif hasattr(self.t1, 'polynomial'):
            result = self.t1.polynomial(x)
        else:
            result = poly.PolyConstant(self.t1)
        return result ** self.t2

    def polyfrac(self, x):
        '''return self as a PolyFrac'''
        if self == x:
            return poly.PolyFracSymbol(x)
        elif hasattr(self.t1, 'polyfrac'):
            result = self.t1.polyfrac(x)
        else:
            result = poly.PolyFracConstant(self.t1)
        return result ** self.t2

    def _first_denominator_appearance(self, x):
        '''returns first denominator in self in which x appears.'''
        if self.t2 < 0:
            if apply(self.t1, '_contains_deep_', x, default=False):
                if equals(self.t2, -1):
                    return self.t1
                else:
                    return self.t1 ** (-1 * self.t2)
            else:
                return None
        else:
            result = apply(self.t1, '_first_denominator_appearance', x, default=None)
            if result is None:
                return None
            elif equals(self.t2, 1):
                return result
            else:
                raise NotImplementedError("_first_denominator_appearance for Power(y, n={})".format(self.t2)) 

    ## SIMPLIFYING ##
    _SIMPLIFY_OPS = ['_flatten', '_simplify_id', '_evaluate_numbers']

    def _flatten(self, **kw__None):
        '''flattens, as appropriate. Power(Power(x, y), z) --> Power(x, y * z)'''
        if isinstance(self.t1, Power):                   # {x^y}^{z}
            powerbase = self.t1
            self = self.Power(powerbase.t1, powerbase.t2 * self.t2)  # --> x^{y * z}
        return self

    def _simplify_id(self, **kw__None):
        '''converts 1^x --> 1, x^0 --> 1, x^1 --> x.'''
        if equals(self.t2, 1):
            return self.t1
        elif equals(self.t1, 1) or equals(self.t2, 0):
            return 1
        else:
            return self

    def _evaluate_numbers(self, **kw__None):
        '''evaluates numbers.'''
        if not (is_number(self.t1) and is_number(self.t2)):
            return self   # return self, exactly, to help indicate no changes were made.
        result = self.OPERATION(self.t1, self.t2)
        if isinstance(result, type(self)):
            if equals(result.t1, self.t1) and equals(result.t2, self.t2):
                return self   # return self, exactly, to help indicate no changes were made.
        return result

    ## EXPANDING ##
    _EXPAND_OPS = ['_distribute', '_expand_exponents']

    def _distribute(self, **kw__None):
        '''distributes at top layer of self. (x y)^n --> x^n y^n'''
        factors = get_factors(self.t1)
        if len(factors) <= 1:
            return self   # return self, exactly, to help indicate nothing changed.
        result = self.Product(*(self.Power(factor, self.t2) for factor in factors))
        return result

    def _expand_exponents(self, **kw__None):
        '''applies (integer) exponents at the top layer of self.
        E.g. (x + 1)**2 --> (x+1)*(x+1).
        '''
        if not isinstance(self.t1, Sum):
            return self
        if not isinstance(self.t2, int):
            return self
        if self.t2 > 0:
            result = self.t1
            for i in range(1, self.t2):
                result *= self.t1
            return _simplyfied(result)
        elif equals(self.t2, -1):
            return self     # return self, exactly, to help indicate we are making no changes.
        else: #self.t2 < -1
            x = self.t1 ** (-1 * self.t2)
            x = apply(x, '_expand_exponents')
            return x ** -1

    ## SOLVING ##
    @_solubility_listing
    def solubility_list(self, x):
        '''returns the list of solubilities for self.'''
        if apply(self[1], '_contains_deep_', x):
            return [self]    # we haven't implemented how to solve, e.g. 2^x
        elif equals(self[1], 0):
            return [None]
        elif equals(self[1], 1):
            return apply(self[0], 'solubility_list', x, default=[None])
        else:
            return [self]    # we haven't implemented how to solve, e.g. x^7


''' --------------------- Operations - Product, Sum --------------------- '''

class Product(AssociativeOperation, CommutativeOperation, MultipleOperation):
    IDENTITY = 1
    OPTYPE = 'Product'

    @property
    def OPERATION(self):
        return lambda x, y: x * y

    def __str__(self, internal=False, enc='()', fracs_enabled=1, **kw):
        '''string of self. Put in enc if internal.
        fracs_enabled is number of layers of fractions (aside from Rational objects) allowed.
        Either the whole thing will be shown as a fraction (e.g. (x y) / z) or none of it will (e.g. x y z^-1)

        # TODO: better str when mixing rational and fraction. e.g. Rational(3, 2) / y --> (3/2) (1/y), but could be 3/(2y).
        '''
        if internal and not issubclass(internal, Sum):
            return r'\left{} {} \right{}'.format(enc[0], _str(self, internal=False, **kw), enc[1])
        else:
            kw['fracs_enabled'] = fracs_enabled
            result = ''
            termlists = self._str_terms_order()
            numerical = termlists[0]
            termlists = termlists[1:]
            # include numerical terms
            ## do pretty things if -1 is in the list of numerical terms.
            try:
                i_minus_1 = numerical.index(-1)
            except ValueError:  # -1 not in list.
                pass
            else:
                numerical = [n for i, n in enumerate(numerical) if i != i_minus_1]
                result += '- '
            ## include the other numerical terms.
            numerical_str = ''
            if len(numerical) == 1:
                numerical_str += '{}'.format(numerical[0]) + ' '
            elif len(numerical) > 1:
                numerical_str += ' '.join([r'\left( {} \right)'.format(n) for n in numerical]) + ' '
            # include other terms:
            remaining = [term for termlist in termlists for term in termlist]
            if fracs_enabled < self._internal_fraction_layers():
                # no fractions. simply join all the terms together.
                result += numerical_str
                result += ' '.join([_str(t, internal=type(self), **kw) for t in remaining])
                return result
            else:
                # fractions. do some fancy stuff to make pretty fractions.
                rationals_ = []
                numer_strs = []
                denom_strs = []
                for t in remaining:
                    if is_opinstance(t, Power) and t[1] < 0:
                        # put in denominator
                        dterm = apply(t._new(t[0], -1 * t[1]), '_simplify_id')
                        dstr = _str(dterm, internal=type(self), **kw)
                        denom_strs.append(dstr)
                    elif isinstance(t, Rational):
                        # put out front
                        rationals_.append(_str(t, internal=type(self), **kw))
                    else:
                        # put in numerator
                        nstr = _str(t, internal=type(self), **kw)
                        numer_strs.append(nstr)
                # handle Rationals
                if len(rationals_) == 0:
                    rational_str = ''
                elif len(rationals_) == 1:
                    rational_str = rationals_[0] + ' '
                else:
                    rational_str = ' '.join([r'\left( {} \right)'.format(r) for r in rationals_]) + ' '
                # handle non-Rational fraction
                full_numer_str = ' '.join(numer_strs)
                full_denom_str = ' '.join(denom_strs)
                if len(denom_strs) == 0:
                    return result + numerical_str + rational_str + full_numer_str
                if len(numer_strs) == 0 and len(denom_strs) > 0:  # nothing in numerator, something in denominator.
                    if len(numerical) == 0:
                        full_numer_str = '1'
                    else:
                        full_numer_str = numerical_str
                    result += rational_str
                else:
                    result += numerical_str + rational_str
                if len(result) > 0:
                    result += ' '
                return r'{}\frac{{{}}}{{{}}}'.format(result, full_numer_str, full_denom_str)

    def _str_terms_order(self):
        '''returns lists of terms of self in order for str representation of self.
        returns:
            list of non-AbstractOperations (e.g. numbers),
            list of constants,
            list of AbstractOperation objects.
        '''
        result = [[], [],[]]
        for term in self:
            if not isinstance(term, AbstractOperation):
                result[0].append(term)
            elif is_constant(term):
                result[1].append(term)
            else:
                result[2].append(term)
        return result

    def __eq__(self, x):
        if x is self:
            return True
        if _equals0(x):
            return self._equals0()
        return CommutativeOperation.__eq__(self, x)

    def _equals0(self):
        '''returns whether self == 0.'''
        # caching
        if CACHING_PROPERTIES:
            try:
                return self._cached_equals0
            except AttributeError:
                pass  # result not stored in cache.
        # result
        result = any(_equals0(t) for t in self)
        # caching
        if CACHING_PROPERTIES:
            self._cached_equals0 = result
        # result
        return result

    def gcf(self, b):
        '''return (gcf= the "greatest" common factor of self and b, self/gcf, b/gcf).'''
        if equals(self, b):
            return (self, 1, 1)
        gg = Product.IDENTITY
        s_factors = get_factors(self)   # we will store here the gcf'd forms.
        b_factors = get_factors(b)
        for i in range(len(s_factors)):
            for j in range(len(b_factors)):
                s = s_factors[i]
                t = b_factors[j]
                g, snew, tnew = gcf(s, t)
                gg = gg * g
                s_factors[i] = snew
                b_factors[j] = tnew
        return gg, _simplyfied(self.Product(*s_factors)), _simplyfied(self.Product(*b_factors))

    def degree(self, x, upper=True):
        '''returns degree of x in self. if upper, return largest degree; else return smallest degree.'''
        result = 0
        for term in self:
            if apply(term, '_contains_deep_', x, default=False):
                result += term.degree(x, upper=upper)
        return result

    def polynomial(self, x):
        '''return self as a Polynomial'''
        if self == x:
            return poly.PolySymbol(x)
        result = poly.PolyConstant(self.IDENTITY)   # 1
        for t in self:
            if hasattr(t, 'polynomial'):
                result *= t.polynomial(x)
            else:
                result *= poly.PolyConstant(t)
        return result

    def polyfrac(self, x):
        '''return self as a PolyFrac'''
        if self == x:
            return poly.PolyFracSymbol(x)
        result = poly.PolyFracConstant(self.IDENTITY)   # 1
        for t in self:
            if hasattr(t, 'polyfrac'):
                result *= t.polyfrac(x)
            else:
                result *= poly.PolyFracConstant(t)
        return result

    def without_term(self, x):
        '''returns self without x. (removes one factor of x from self).
        makes ValueError if x not in self.terms.
        '''
        terms = []
        added_x = False
        for t in self:
            if (not added_x) and equals(t, x):
                added_x = True
            else:
                terms.append(t)
        if not added_x:
            raise ValueError('{} not in {}!'.format(repr(x), repr(self)))
        return self._new(*terms)

    without_factor = property(lambda self: self.without_term)   # alias

    def split(self, func):
        '''splits self by func.
        returns product of terms where func is True, product of terms where func is False.
        '''
        ftrue, ffalse = tools.split(self, func)
        return (self._new(*ftrue), self._new(*ffalse))

    ## SIMPLIFYING ##
    _SIMPLIFY_OPS = ['_collect', '_simplify_id']

    def _collect(self, **kw__None):
        '''collects all terms with same base. (E.g. x * x^n --> x^(n+1))'''
        result = self.IDENTITY
        powterms = [self.Power(t,1)._flatten() for t in self]
        collected_any = False
        i = 0
        while i < len(powterms):
            x = powterms[i]
            j = i + 1
            while j < len(powterms):
                y = powterms[j]
                if equals(x.t1, y.t1):
                    x = self.Power(x.t1, x.t2 + y.t2)
                    powterms.pop(j)
                    collected_any = True
                else:
                    j = j + 1
            x_simple = x._simplify_id()  # Power._simplify_id().
            try:
                result = result * x
            except TypeError:
                if is_number(result) and is_number(x_simple):
                    # doing, e.g. float times ComplexRational. [TODO] handle this more graciously.
                    result = self.Product(result, x)
                else:
                    raise
            i = i + 1
        if not collected_any:
            return self   # return self, exactly, to help indicate we made no changes.
        return _simplyfied(result)

    def _simplify_id(self, _sid_debug=False, _simplify_x_over_x=True, **kw__None):
        '''converts 1*x --> x, 0*x --> 0, x * x^-1 --> 1, x^n * x^-n --> 1, at top layer of self.'''
        # handle the 1*x --> x.
        self = CommutativeOperation._simplify_id(self, **kw__None)
        if (not _simplify_x_over_x) or (not isinstance(self, Product)):
            return self
        explain = tools.debug_viewer(_sid_debug)
        explain('_simplify_id for:', view=self)
        if any(equals(t, 0) for t in self):
            return 0
        candidates = [t for t in self]
        simplifided_any = False
        i = 0
        while i < len(candidates):
            t = candidates[i]                            # t <--> i
            tbase, tpower = get_base_and_power(t)
            explain('at candidate i={}, t={}', format=[i, t])
            # check for candidates with power== -1*tpower, and sharing a common factor with tbase.
            j = i + 1
            while j < len(candidates):
                s = candidates[j]                           # s <--> j
                sbase, spower = get_base_and_power(s)
                if equals(spower, -1 * tpower):
                    factor, new_s, new_t = gcf(sbase, tbase)
                    explain(' > candidate s={} has power(s) = -power(t)', format=[s])
                    if not equals(factor, 1):  # sbase and tbase share a common factor.
                        explain(' > > the common factor ({}) will be eliminated', format=[factor])
                        # that shared factor will be eliminated.
                        simplifided_any = True
                        if equals(new_s, 1):   # << new_s == 1
                            candidates.pop(j)
                            j -= 1  # ( we also j++ from loop; net change of 0. Good b/c we popped j. )
                        else:               # << new_s != 1
                            candidates[j] = self.Power(new_s, spower)
                        if equals(new_t, 1):   # << new_t == 1
                            candidates.pop(i)
                            i -= 1  # ( we also i++ from loop; net change of 0. Good b/c we popped i. )
                            break    # time-saving: can't reduce t any further if t==1.
                        else:               # << new_t != 1
                            candidates[i] = self.Power(new_t, tpower)
                        tbase = new_t   # this ensures tbase is updated to (original tbase) / (factor)
                                        #   for the remainder of the loop through j.
                else:
                    explain('---candidate s={} does not have power(s) = -power(t)', format=[s])
                j += 1
            i += 1
        if simplifided_any:
            self = self._new(*candidates)
            if not isinstance(self, Product):
                return self
        return self


    ## EXPANDING ##
    _EXPAND_OPS = ['_distribute']

    def _distribute_like_terms(self, focus_only=None, **kw__None):
        '''distributes like terms at top layer of self.'''
        result = self.IDENTITY
        distributed_any_like_terms = False
        idx_handled = []   # << put here the idx for the terms we have handled.
        for i, candidate in filter(lambda i_t: (i_t[0] not in idx_handled) and (isinstance(i_t[1], Sum)),
                                    enumerate(self)):  # candidate = self[i], for unhandled Sums
            for j, term in filter(lambda j_t: (j_t[0] not in idx_handled) and (j_t[0]!=i),
                                    enumerate(self)):  # term = self[j], for unhandled terms other than candidate
                if (focus_only is not None) and (not equals(term, focus_only)):
                    continue
                if candidate._contains_deep_(term):
                    # distribute term to the summands in candidate
                    multiply_by_term = (lambda t: t * term) if j > i else (lambda t: term * t)
                    cdist_terms = [multiply_by_term(t) for t in candidate]
                    cdist = candidate._new(*cdist_terms)
                    result = result * cdist
                    # do some bookkeeping
                    distributed_any_like_terms = True
                    idx_handled += [i, j]
                    # exit loop for this candidate, since we distributed with it.
                    break
        if distributed_any_like_terms:
            for k, t in filter((lambda k_t: k_t[0] not in idx_handled), enumerate(self)):
                result = result * t
            return result #._distribute_like_terms
        return self   # return self, exactly, to help indicate we made no changes.

    def _distribute(self, check_like_terms=True, focus_only=None,
                    distribute_if=lambda self: True, **kw__None):
        '''distributes at top layer of self.
        if check_like_terms, do some "smart" checks first,
            e.g. (x+2) * (7 + 3/(x+2)) --> 7*(x+2) + 3.
            if any smart checks do something, distribute using only smart checks at this layer.
        if focus_only, only distribute term which equals to focus_only. (only works if check_like_terms.)
        if not distribute_if(self), don't distribute.
        '''
        # check distribute_if
        if not distribute_if(self):
            return self   # distribute_if tells us not to distribute in this case. Return self exactly.
        # check if we contain no sums
        if not any(isinstance(s, Sum) for s in self):
            return self   # nothing to distribute. Return self to be clear that we made no changes.
        # check focus_only
        if focus_only is not None:
            def unfocused(s): return not equals(s, focus_only)   # skip due to lack of focus if s!=focus_only.
        else:
            def unfocused(s): return False   # never skip due to lack of focus, if focus_only is None.
        if all(unfocused(s) for s in self):
            return self   # nothing to distribute. Return self to be clear that we made no changes.
        # check like terms (check if any Sums in self contain any of the other terms in self.)
        if check_like_terms:
            result = self._distribute_like_terms(focus_only=focus_only, **kw__None)
            if result is not self:
                return result
        # initialize result
        result = self.IDENTITY
        # distribute other things, if focus_only.
        if focus_only is not None:
            distributed_anything = False
            for s in self:
                if equals(s, focus_only) and isinstance(result, Sum):
                    scr = result  # save result (until now) to scr.
                    result = Sum.IDENTITY
                    for r in scr:
                        result = result + r * s
                    distributed_anything = True
                elif equals(result, focus_only) and isinstance(s, Sum):
                    scr = result  # save result (until now) to scr.
                    result = Sum.IDENTITY
                    for s_term in s:
                        result = result + scr * s_term
                    distributed_anything = True
                else:
                    result = result * s
            if distributed_anything:
                return result
            else:
                return self  # return self, exactly, to help indicate nothing changed.
        # distribute other things, if not focusing.
        else:
            # note: if there are no sums in self, we would have already returned self, above.
            for s in self:
                scr = result    # (save result (until now) to scr.)
                s   = get_summands(s)
                scr = get_summands(scr)
                result = Sum.IDENTITY
                for r in scr:    # (loop through terms of scr.)
                    for n in s:
                        result = result + r * n
                result = _simplyfied(result)
            return result

    ## SOLVING ##
    @_solubility_listing
    def solubility_list(self, x):
        '''returns the list of solubilities for self.'''
        result = [None]
        for term in self:
            solub = apply(term, 'solubility_list', x, default=[None])
            if solub != [None]:
                if result == [None]:
                    result = solub
                else:  # x in multiple terms of self. Haven't implemented how to treat this.
                    return [self]
        return result


class Sum(AssociativeOperation, CommutativeOperation, MultipleOperation):
    IDENTITY = 0
    OPTYPE = 'Sum'

    @property
    def OPERATION(self):
        return lambda x, y: x + y

    def __str__(self, internal=False, enc='()', **kw):
        '''string of self. Put in enc if internal.'''
        if internal:
            return r'\left{} {} \right{}'.format(enc[0], _str(self, internal=False, **kw), enc[1])
        else:
            s_terms = [_str(t, internal=type(self), **kw) for t in self]
            result = s_terms[0]
            for t in s_terms[1:]:
                if t.lstrip().startswith('-'):
                    result += ' ' + t   # subtraction
                else:
                    result += ' + ' + t   # addition
            return result

    def __eq__(self, x):
        if x is self:
            return True
        if _equals0(x):
            return self._equals0()
        return super().__eq__(x)

    def _equals0(self):
        '''returns whether self == 0.'''
        # caching
        if CACHING_PROPERTIES:
            try:
                return self._cached_equals0
            except AttributeError:
                pass  # result not stored in cache.
        # result
        result = all(_equals0(t) for t in self)
        # caching
        if CACHING_PROPERTIES:
            self._cached_equals0 = result
        # result
        return result

    def gcf(self, b):
        '''return (gcf= the "greatest" common factor of self and b, self/gcf, b/gcf).
        Not guaranteed to find the gcf. E.g. not able to handle polynomial factoring in general.
            but it is guaranteed that self == r[0] * r[1], and b == r[0] * r[2], where r=gcf(self, b).

        Rules implemented here:
            gcf(a x + b x, k x) --> (x, a + b, k)
            gcf(a x + b x, a y + b y) --> (a + b, x, y)    #(This one isn't implemented yet)
        '''
        if equals(self, b):
            return (self, 1, 1)
        """this is commented out for efficiency purposes...
        s_terms = list(self)
        b_terms = get_summands(b)
        if len(b_terms)==1:  # b is not a sum
            return self._gcf_product(b)
        # else, b is a sum.
        #""this is commented out because it's not fully implemented yet.
        if len(b_terms)==len(s_terms):
            assert len(s_terms) > 0
            candidates = []
            for t in b_terms:
                gg = gcf(s_terms[0], t)
                if not equals(gg[0], 1):
                    candidates.append([[gg[0]], (gg[1], gg[2])])
            if len(candidates) == 0:
                return (1, self, b)
            for s in s_terms[1:]:
                for t in b_terms:
                    gg = gcf(s, t)
                    for c in candidates:
                        if (gg[1], gg[2]) == c[1]:
                            c[0].append(gg[0])
                            break
                    else: # didn't break; didn't find any match for this t.
        #"""
        return (1, self, b)

    def _gcf_product(self, b):
        '''gcf between self and b. Assumes b is a product.
        Helper function for gcf. For internal use only.
        '''
        s_terms = list(self)
        # b = b_terms[0]
        # first check if we can find a gcf using b.gcf.
        if hasattr(b, 'gcf'):
            test = b.gcf(self)
            if not equals(test[0], 1):
                return (test[0], test[2], test[1])  # g, s/g, b/g
        # then, loop through terms of self.
        gcfs = []
        common_g = None
        for s in s_terms:
            g = gcf(s, b)
            if g[0] == 1:   # gcf with b is 1.
                return (1, self, b)
            gcfs.append(g)
            if common_g is None:
                common_g = g[0]
            else:
                common_g = gcf(common_g, g[0])[0]
                if common_g == 1:
                    return (1, self, b)
        # update all the gcf combos to common_g, and add them up appropriately.
        result_s_terms = []
        for (g, s, t) in gcfs:
            _, _, mul = gcf(common_g, g)
            result_s_terms.append(s * mul)     # s after dividing by common_g.
        result_s = self.Sum(*result_s_terms)
        result_b = gcf(common_g, b)[-1]        # b after dividing by common_g.
        return (common_g, result_s, result_b)

    def degree(self, x, upper=True):
        '''returns degree of x in self. if upper, return largest degree; else return smallest degree.'''
        term_degrees = [apply(term, 'degree', x, upper=upper, default=0) for term in self]
        if upper:
            return max(term_degrees)
        else:
            return min(term_degrees)

    def polynomial(self, x):
        '''return self as a Polynomial'''
        if self == x:
            return poly.PolySymbol(x)
        result = poly.PolyConstant(self.IDENTITY)
        for t in self:
            if hasattr(t, 'polynomial'):
                result += t.polynomial(x)
            else:
                result += poly.PolyConstant(t)
        return result

    def polyfrac(self, x):
        '''return self as a PolyFrac'''
        if self == x:
            return poly.PolyFracSymbol(x)
        result = poly.PolyFracConstant(self.IDENTITY)
        for t in self:
            if hasattr(t, 'polyfrac'):
                result += t.polyfrac(x)
            else:
                result += poly.PolyFracConstant(t)
        return result

    def split(self, func):
        '''splits self by func.
        returns sum of terms where func is True, sum of terms where func is False.
        '''
        ftrue, ffalse = tools.split(self, func)
        return (self._new(*ftrue), self._new(*ffalse))

    ## SIMPLIFYING ##
    _SIMPLIFY_OPS = [
                    '_simplify_id',
                    # _simplify_x_minus_x,
                    #'_smash_zero_pairs',
                    '_collect',
                    #'_collect_associates',
                    #'_collect_common_power',
                    ]  # (order matters; earlier are applied earlier.)

    def _simplify_id(self, _simplify_x_minus_x=False, **kw__None):
        '''converts 0 + x --> x, x - x --> 0 at top layer of self.
        _simplify_x_minus_x: whether to handle "x - x --> 0"
            False by default because simplify_id gets called often, and this check is somewhat expensive.
        '''
        # handle x + 0 --> x.
        self = CommutativeOperation._simplify_id(self, **kw__None)
        if (not _simplify_x_minus_x) or (not isinstance(self, Sum)):
            return self

    def _simplify_x_minus_x(self, **kw__None):
        '''x - x --> 0.
        If no changes were made, return self, exactly (comparable via 'is').
        '''
        terms = list(self)
        # To improve efficiency, we will compare the numeric components of terms first,
        #   and then compare the AbstractOperation components,
        #   only if one of the numbers is -1 times the other.
        ## first, split terms of self into (numeric, abstract) components.
        numbers   = []
        negnums   = []  # <-- store -1 * the number
        abstracts = []
        def pop_i(i):   # function to pop i'th element from all the lists we are tracking.
            numbers.pop(i)
            negnums.pop(i)
            abstracts.pop(i)
        for t in terms:
            if isinstance(t, Product):
                split = apply(t, 'split_numeric_component', default=(t,1))
            elif is_number(t):
                split = (t, 1)
            else:
                split = (1, t)
            num = apply(split[0], 'evaluate_numbers', **kw__None)
            neg = apply(-1 * num, 'evaluate_numbers', **kw__None)
            numbers.append(num)
            negnums.append(neg)
            abstracts.append(split[1])
        ## next, loop through candidates.
        simplifided_any = False
        i = 0
        while i < len(abstracts):
            j = i + 1
            while j < len(abstracts):
                if equals(numbers[i], negnums[j]):
                    if equals(abstracts[i], abstracts[j]):
                        # i and j cancel out, and we simplified at least 1 thing.
                        simplifided_any = True
                        pop_i(j)  # need to pop j first since j > i.
                        pop_i(i)
                        break
                j += 1
            else: # didn't break; nothing cancels with i'th term. Move to i + 1.
                i += 1
        if simplifided_any:
            terms = [num * abt for num, abt in zip(numbers, abstracts)]
            self = self._new(*terms)
        return self

    def _smash_zero_pairs(self, **kw__None):
        '''smashes any pairs of things which should cancel in self.
        This includes x - x --> 0.
        Also expands top layer of things to check. E.g. (x+1) - (x-y) --> 1 + y.

        TODO: only keep result terms expanded if any changes were made to it.
            E.g. (x+1) - (x-y) + 7*(b+c) --> 1 + y + 7*(b+c)
        '''
        _once_distributed = self._new(*[apply(t, "_distribute") for t in self])
        once_distributed = apply(_once_distributed, "_flatten")
        pairs_simplified = apply(once_distributed, "_simplify_x_minus_x", **kw__None)
        if pairs_simplified is once_distributed:
            return self   # no changes were made.
        else:
            return pairs_simplified

    def associate_indices(self, like, skip=[]):
        '''returns None, or the indices of terms in self such that their sum equals like.
        skips terms with indices in skip.
        '''
        assert isinstance(like, Sum)
        itself = [(i, t) for (i, t) in enumerate(self) if not (i in skip)]
        result = []
        for l in like:
            for j, (i, t) in enumerate(itself):
                if equals(l, t):
                    result.append(i)
                    itself.pop(j)  # for efficiency.
                    break
            else:  # didn't break; i.e. didn't find any match.
                return None
        return result

    def _collect_associates(self, **kw__None):
        '''converts x + y + z * (x + y) --> (1 + z) * (x + y)'''
        for i, t in enumerate(self):
            factors = get_factors(t)
            if len(factors) == 1:
                continue
            for factor in factors:
                if isinstance(factor, Sum):
                    idx = self.associate_indices(factor, skip=[i])
                    if idx is not None:
                        # we found something to do.
                        collected_term = (1 + t.without_factor(factor)) * factor
                        result_terms = [collected_term if j==i else self[j] for j in range(len(self)) if j not in idx]
                        result = self.Sum(*result_terms)
                        result = apply(result, '_collect_associates', **kw__None)
                        return result
        return self

    def _collect_common_power(self, _ccp_debug=False, **kw__None):
        '''converts A x^n + B x^m --> (A x^(n-m) + B) * x^m,  (when m < n, and m, n != 1).
        Attempts this for all Power factors inside terms of self.
        (non-Power factors are handled by _collect.)

        Only collects the smallest common degree, and only if it is nonzero.
            e.g. x^3 + x^2 --> x^2 (x + 1), but x^3 + x^2 + 1 will be unaffected.

        It is assumed that factors with common bases have been combined already.
            e.g. we will never see x^3 * x^2, but instead would see x^5.

        If degrees cannot be compared (via '<'), pretends degree of first appearance is the smallest.
            e.g. x^y + x^z --> x^y (1 + x^(z-y)); x^z + x^y --> x^z (1 + x^(y-z))
        '''
        explain = tools.debug_viewer(_ccp_debug)
        explain('In _collect_common_power for:', view=self)
        # get lists of factors
        summands = list(self)
        factors_lists = []
        bp_lists      = []
        powfactors    = []
        for t in summands:
            bplist  = []   # list of (base, power) tuples for factors in t
            factors = get_factors(t)
            for factor in factors:
                base, power = get_base_and_power(factor)
                bplist.append((base, power))
                if isinstance(factor, Power):
                    powfactors.append(factor)
            bp_lists.append(bplist)
            factors_lists.append(factors)

        for pbase, ppower in powfactors:
            explain('In _collect_common_power at pbase={}', format=[pbase])
            minpower = ppower
            chosen_f = []   # << the indices for the factor in each term which has base == pbase.
            for i, t in enumerate(bp_lists):
                # get power of factor with same base as pbase.
                #   (If none exist, break, and we can't collect_common_power for p.)
                try:
                    j, tpower = next((j, tpower) for (j, (tbase, tpower)) in enumerate(t) if equals(pbase, tbase))
                except StopIteration:
                    break  # t doesn't have any factors with base == pbase.
                if _equals0(tpower):
                    break  # t doesn't have any factors with base == pbase.
                explain('pbase was found in term {}: bp_lists[{}]={}', format=[i, i, t])
                # track min power
                try:
                    newmin = (tpower < minpower)
                except TypeError:   # can't compare powers via '<'.
                    newmin = False
                if newmin:
                    minpower = tpower
                # track factor index (for efficiency)
                chosen_f.append(j)
            else:  # didn't break. That means every term has pbase in it. So, collect.
                result_terms = []
                for factors, bplist, j in zip(factors_lists, bp_lists, chosen_f):
                    term_fs = []
                    for i, (f, bp) in enumerate(zip(factors, bplist)):
                        if i == j:
                            base, power = bp
                            term_fs.append(base ** (power - minpower))
                        else:
                            term_fs.append(f)
                    term = self.Product(*term_fs)
                    result_terms.append(term)
                result = (pbase ** minpower) * self.Sum(*result_terms)
                return result
        return self

    def COLLECTION_PRIORITIZER(self, x):
        '''returns collection priority number for x.
        highest means "collect first".
        '''
        if not isinstance(x, ao.AbstractOperation):
            return 10
        elif isinstance(x, cn.AbstractConstant):
            return 20
        elif apply(x, 'is_constant', default=False):
            return 30
        elif is_opinstance(x, Sum):
            return 50
        elif is_opinstance(x, Power):
            return 60
        else:
            return 40  # Sums and Powers are higher priorities than other things.

    MAXIMUM_COLLECTION_PRIORITY = 60

    def PRIORITY_COMBINER(self, prioritizer=None, priorities=[], poly_priorities=[], collect_only=None,
                          pstart=None, pstep=1, **kw__None):
        '''combines prioritizer with priorities. See _collect.__doc__ for details.

        prioritizer: None, or function.
            None -> use self.COLLECTION_PRIORITIZER, which causes collection order:
                    variables, then constants, then AbstractConstants, then numbers.
            else -> prioritizer should be a function of x (a term) which returns a value,
                    with larger values implying it is more important to collect that term.
                    NOTE: terms with priority of 0 or less will NEVER be collected.

        priorities: list
            AbstractOperation element --> lambda x: x == element
            other elements should be boolean functions of one arg, evaluating 
                to True if arg should be prioritized.
            The order is [highest priority in priorities, ..., lowest priority in priorities].
            If as soon as one priority function here evaluates to True, stop checking the rest.

        poly_priorities: list (default empty list) of AbstractOperation objects.
            prioritize collection of terms with get_base_and_power[0] in poly_priorities.
            <poly_priorities> go after <priorities>.

        collect_only: None (default) or list
            None -> ignore this kwarg.
            list -> like priorities list,
                except we will ignore all other priorities,
                only ever collect things in this list.

        pstart: None (default) or value
            start counting priorities at pstart.
            None --> use self.MAXIMUM_COLLECTION_PRIORITY.

        pstep: 1 (default) or value
            increment by pstep for each priority in priorities and poly_priorities
            The priorities will be pstart+[N*pstep, ..., 2*pstep, 1*pstep],
                where N = len(priorities) + len(poly_priorities)

        returns a prioritizer function.
        '''
        if collect_only:  # if collect_only, smash prioritizer and priorities.
            prioritizer = lambda x: -1
            priorities  = collect_only
            poly_priorities = []
        if prioritizer is None:
            prioritizer = self.COLLECTION_PRIORITIZER
        if len(priorities) + len(poly_priorities) == 0:
            return prioritizer    # in case of no custom priorities, just return prioritizer.
        if pstart is None:
            pstart = self.MAXIMUM_COLLECTION_PRIORITY + pstep
        priorities  = [(lambda x: x==p) if isinstance(p, ao.AbstractOperation) else p
                       for p in priorities]
        priorities += [(lambda x: get_base_and_power(x)[0]==p)
                       for p in poly_priorities]
        def combined_prioritizer(x):
            '''return the priority value for x during this collection procedure.
            higher value indicates higher priority which should be collected earlier.
            '''
            for i, priority_func in enumerate(priorities):
                if priority_func(x):
                    return pstart + (len(priorities) - i) * pstep
            return prioritizer(x)
        return combined_prioritizer

    def _collect(self, collect_sum=True, priorities=[], poly_priorities=[],
                 collect_only=None, collect_aggressively=True, _collect_debug=False, **kw):
        '''collects terms with the same factor at top layer of self.
        if not collect_sum, return self.
        if no changes would be made, return self.

        By default, prioritize collection in this order:
         >> variables, then constants, then AbstractConstants, then numbers <<

        priorities: list (default empty list)
            elements can be AbstractOperation objects
                --> prioritize collection for term equal to element.
            or boolean-outputting functions of a single argument.
                --> prioritize collection for term with bool(function(term)) == True.
            E.g. priorities = [z, lambda x: x < 0] would prioritize:
                z, then any x < 0, then the priority order implied by self (or prioritizer, if entered).

        poly_priorities: list (default empty list) of AbstractOperation objects.
            prioritize collection of terms with get_base_and_power[0] in poly_priorities.
            <poly_priorities> go after <priorities>.

        collect_only: None (default) or list
            None -> ignore this kwarg.
            list -> like priorities list,
                except we will ignore all other priorities, and only ever collect things in this list.

        collect_aggressively: True (default) or False
            False -> ignore this kwarg
            True -> collect as many factors as possible as early as possible,
                    not caring if some terms get left out,
                    e.g. (7x + xy + by --> x(7 + y) + b y) splits the y's;
                        would not be allowed normally (if x and y have the same priority)
        '''
        # bookkeeping
        kw = dict(collect_sum=collect_sum, priorities=priorities,
                  poly_priorities=poly_priorities, collect_only=collect_only, **kw)
        prioritizer = self.PRIORITY_COMBINER(**kw)
        factor_left = None  # << for prettiness only. True --> put f * stuff. False or None --> put stuff * f.
        
        # get lists of factors
        summands = list(self)
        factors_lists = [get_factors(t) for t in summands]

        # count number of times each factor appears.
        #  factors_idx will have elements (f, dict of fi: [list of fj]) with all the f unique, such that factors_lists[fi][fj] == f.
        factors_idx   = tools.counts_sublist_indices(factors_lists)
        # calculate priorities of all the factors
        prioritys     = [(prioritizer(f), len(idx)) for f, idx in factors_idx]
        # stop trying to collect any factors with priority <= 0.  (TODO: make more efficient.)
        factors_idx   = [fidx for i, fidx in enumerate(factors_idx) if prioritys[i][0] > 0]
        prioritys     = [p  for   p   in        prioritys           if     p[0]        > 0]
        # sort by priorities (should not be necessary, but makes sense to do it).
        argsort       = tools.argsort(prioritys, reverse=True)  # reverse=True --> highest first.
        factors_idx   = [factors_idx[a] for a in argsort]
        prioritys     = [  prioritys[a] for a in argsort]

        if _collect_debug:
            raise Exception('boom')
        # collect variables
        for f_idx, (f, indices) in enumerate(factors_idx):
            # "pull f out of factors_lists containing f"
            if len(indices) < 2:    # << this f doesn't appear 2 or more times - no need to try to factor it.
                continue
            for term_j, factor_appears_in_j in indices.items():
                if len(factor_appears_in_j) > 1:  # << this f appears multiple times in the same term; don't try to factor it.
                    continue
            if equals(f, Product.IDENTITY):  # << to avoid getting stuck looping on e.g. Sum(1, 1).
                continue
            collectable = True  # << whether to keep trying to collect f.
            factored = []       # << summands to multiply by factor. E.g. (a+b) after ax+bx --> (a+b)x
            unfactored_pre  = []     # << terms which were untouched by the factoring process.
            unfactored_post = []     # << terms which were untouched by the factoring process.
            for k, factors in enumerate(factors_lists):
                for term_i, factor_i in [(ti, fi) for ti in indices.keys() for fi in indices[ti]]:
                    if term_i == k:
                        # factor term number term_i:
                        factored.append(self.Product(*tools._list_without_i_(factors_lists[term_i], factor_i)))
                        break
                else:  # didn't break; didn't find k in indices. term k will be unfactored.
                    if len(factored) == 0:
                        unfactored_pre.append(summands[k])   # summands[k] == Product(*factors)
                    else:
                        unfactored_post.append(summands[k])  # summands[k] == Product(*factors)
            # "check that the result will not split term appearances"
            #  e.g. yx + bx + 7y --> (y+b)x + 7y   is not allowed, because the y's get split.
            #  exception: if y is a constant, we will allow it to get split.
            # more generally, we allow y to get split to factor x whenever priority(y) < priority(x).
            if (not collect_aggressively):
                for t_idx, (t, indices) in enumerate(factors_idx):
                    if t_idx == f_idx:
                        continue  # we already checked f, above.
                    if (prioritys[t_idx] >= prioritys[f_idx]) and _list_contains_deep_(factored, t):
                        if _list_contains_deep_(unfactored_pre, t) or _list_contains_deep_(unfactored_post, t):
                            collectable = False
                            break
            # if still "collectable", we succeeded in doing a collection!
            #  so, return unfactored stuff + factor * factored stuff
            if collectable:
                sum_factored        = self._new(*factored)
                sum_unfactored_pre  = self._new(*unfactored_pre)
                sum_unfactored_post = self._new(*unfactored_post)

                product_factored    = f * sum_factored if factor_left else sum_factored * f

                sum_unfactored       = unfactored_pre + unfactored_post
                collected_unfactored = apply(sum_unfactored, '_collect', **kw)
                if collected_unfactored is sum_unfactored:
                    return sum_unfactored_pre + product_factored + sum_unfactored_post
                else:
                    return product_factored + collected_unfactored
        return self

    ## SOLVING ##
    @_solubility_listing
    def solubility_list(self, x):
        '''returns the list of solubilities for self.'''
        return [t for term in self for t in apply(term, 'solubility_list', x, default=[None])]


''' --------------------- LinearOperation, MultiSum --------------------- '''

class LinearOperation(MultipleOperation):
    '''holds the operation f(x), knowing that f is linear in x.'''
    def __init__(self, f, x):
        self.f = f
        self.x = x
        self._update_maths_like(x)

    def _new(self, x):
        '''create a new instance of self.
        (use self.f for f at __init__)
        subclasses may want to overwrite this method.
        (e.g. Derivative(t, y), t is a Symbol, uses f=DerivativeSymbol(t))
        '''
        result = type(self)(self.f, x)
        self._transmit_genes(result)
        return result

    @property
    def terms(self):
        return [self.x]

    def _repr_contents(self, **kw):
        return [_repr(self.f, **kw), _repr(self.x, **kw)]

    def __str__(self, internal=False, enc='()', parens_implied=False, enc_inside='()', **kw):
        '''string of self. Put in enc if internal to a Product or Power.
        if parens_implied, write f x. Else, write f(x).
            uses enc_inside to choose parens. E.g. for f[x], use enc_inside='[]'.
        '''
        if internal and issubclass(internal, (Product, Power))  \
                and not issubclass(internal, (BinaryOperation)):   # binary op will handle enc as needed.
            return r'\left{} {} \right{}'.format(enc[0], _str(self, internal=False, enc_inside=enc_inside, **kw), enc[1])
        else:
            fstr = _str(self.f, internal=type(self))
            xstr = _str(self.x, internal=False, **kw)
            parens = ('', '') if parens_implied else (r'\left{} '.format(enc_inside[0]), r' \right{} '.format(enc_inside[1]))
            return r'{} {}{}{}'.format(fstr, parens[0], xstr, parens[1])

    def treats_as_constant(self, x):
        '''return whether self treats x as a constant.
        Subclasses can be more precise about this.
        E.g. sum over j will treat anyone without j subscript as constant.

        The default here is just to treat constants as constant.
        '''
        return is_constant(x)

    ## EXPANDING ##
    _EXPAND_OPS = ['_distribute']

    def _distribute(self, **kw__None):
        '''f(cx) --> c f(x), when f treats c as constant;  f(x) + f(y) --> f(x + y)'''
        # f(x) + f(y) --> f(x + y)
        summands = get_summands(self.x)
        if len(summands) > 1:
            fterms = [self._new(t)._distribute(**kw__None) for t in summands]
            return self.Sum(*fterms)
        # f(cx) --> c f(x)
        factors = get_factors(self.x)
        cprod = Product.IDENTITY
        xprod = Product.IDENTITY
        distributed_any = False
        for term in factors:
            if term is Product.IDENTITY:
                continue    # e.g. if we have f(1), don't turn it into 1 * f(1).
            elif self.treats_as_constant(term):
                cprod = cprod * term
                distributed_any = True
            else:
                xprod = xprod * term
        if distributed_any:
            fx = self._new(xprod)._distribute(**kw__None)
            return cprod * fx
        # <- if we reached this line, we distributed nothing ->
        return self   # return self, exactly, to help indicate nothing was changed.


class MultiSumSymbol(Symbol):
    '''the Symbol representing the sum and indices for a MultiSum, e.g. Σ_{n=1}^{7}.
    See MultiSum doc for kwarg descriptions.

    properties:
        indices: indices of sum. None if the indices are unknown or not fully determined.
        i_min, i_max: start and stop (inclusive) for sum, incrementing by 1.
        origindices: indices of sum, but None when i_min, i_max shorthand is used.
    '''
    def __init__(self, index=None, i_min_or_list=None, i_max=None, i_min=None, i_set=None):
        self.index=index
        self._set_indices(i_min_or_list=i_min_or_list, i_max=i_max, i_min=i_min, i_set=i_set)
        self.s = str(self)

    def _init_properties(self):
        '''returns dict for initializing another MultiSumSymbol like self.'''
        result = dict(index=self.index)
        indices = self.origindices
        if indices is not None:
            key = 'i_set' if self.i_set else 'i_min_or_list'
            result[key] = indices
        else:
            result.update(dict(i_min=self.i_min, i_max=self.i_max))
        return result

    def _new(self, *args, **kw):
        '''makes a new MultiSumSymbol with same index and indices as self,
        except any which are overwritten by kw.
        '''
        init_props = self._init_properties()
        init_props.update(kw)
        result = type(self)(*args, **init_props)
        self._transmit_genes(result)
        return result

    i_min = property(lambda self: getattr(self, '_i_min', None))
    @i_min.setter
    def i_min(self, i):
        self._i_min = i

    i_max = property(lambda self: getattr(self, '_i_max', None))
    @i_max.setter
    def i_max(self, i):
        self._i_max = i

    origindices = property(lambda self: getattr(self, '_indices', None))

    @property
    def indices(self):
        result = self.origindices
        if result is None:
            i_min = self.i_min
            i_max = self.i_max
            if not (None in [i_min, i_max]):
                try:
                    return list(range(i_min, i_max + 1))
                except TypeError:
                    return [i_min, Ellipsis, i_max]
        return result
    @indices.setter
    def indices(self, value):
        self._indices = value

    @property
    def is_expandable(self):
        '''tells whether self can be expanded into a Sum.
        (I.e., tells whether all the indices are known and there are finitely many of them.)
        '''
        result = self.origindices
        if result is None:
            i_min = self.i_min
            i_max = self.i_max
            if not (None in [i_min, i_max]):
                try:
                    range(i_min, i_max + 1)
                except TypeError:
                    return False
                else:
                    return True
        else:
            return True

    def _set_indices(self, i_min_or_list=None, i_max=None, i_min=None, i_set=None):
        '''tells self the indices to sum over.'''
        self.i_set = False  # < by default, 'not using i_set'.
        if i_min_or_list is None:
            if i_set is not None:
                self.indices = i_set
                self.i_set   = True
            else:
                self.i_min = i_min
                self.i_max = i_max
                return
        if isinstance(i_min_or_list, AbstractOperation):
            is_list = False
        else:
            try:
                iter(i_min_or_list)
            except TypeError:
                is_list = False
            else:
                is_list = True
        if is_list:
            self.indices = i_min_or_list
        else:
            self.i_min   = i_min_or_list
            self.i_max   = i_max

    def with_indices(self, i_min_or_list=None, i_max=None, i_min=None, i_set=None):
        '''return MultiSumSymbol with indices set as indicated.'''
        return self._new(i_min_or_list=i_min_or_list, i_max=i_max, i_min=i_min, i_set=i_set)

    def __str__(self, **kw):
        if self.index is None:
            return r'\sum'
        i_min, i_max = self.i_min, self.i_max
        if i_min is None and i_max is None:
            indices = self.indices
            if indices is None:
                return r'\sum_{{{}}}'.format(self.index)
            else:
                try:
                    next(iter(indices))
                except (TypeError, StopIteration):   # indices is not iterable, or is empty.
                    str_indices = str(indices)
                else:
                    str_indices = r'\{' + ', '.join(str(i) for i in indices) + r'\}'
                return r'\sum_{{{} \ \in \ {}}}'.format(self.index, str_indices)  # the \<space> are for spacing.
        else:
            str_min = '' if i_min is None else ' = {}'.format(i_min)
            str_max = '' if i_max is None else '^{{{}}}'.format(i_max)
            return r'\sum_{{{}{}}}{}'.format(self.index, str_min, str_max)

    def _repr_contents(self):
        '''returns contents to put inside MultiSumSymbol() in repr for self.'''
        contents = [repr(self.index)]
        indices = self.origindices
        if indices is not None:
            contents.append(str(indices))
        else:
            for mm in ('min', 'max'):
                i_min = getattr(self, 'i_{}'.format(mm))
                if i_min is not None:
                    contents.append('{}={}'.format(mm, i_min))
        return contents

    def __eq__(self, x):
        if x is self:
            return True
        if not isinstance(x, MultiSumSymbol):
            return False
        if self.index != x.index:
            return False
        s_indices = self.indices
        x_indices = x.indices
        if s_indices != x_indices:
            return False
        if (s_indices is None) and ((self.i_min != x.i_min) or (self.i_max != x.i_max)):
            return False
        return True


class MultiSum(LinearOperation):
    '''class for Sigma notation for sum.
    E.g. MultiSum(a_n, n, 1, 3) == Sigma_{n=1}^{3}(a_n) == a_1 + a_2 + a_3

    MultiSum(summand, index=None, i_min_or_list=None, i_max=None)
        summand: object
            the thing inside the sum.

        Each of the following parameters is ignored if it is None:
            index: Symbol
                the symbol which the sum is taken over.
            i_min_or_list: int, iterable, or None
                iterable --> index takes on these values.
                int --> minimum value for index, incrementing by 1; see i_max.
            i_max: int, or None
                maximum value for index, incrementing by 1.
            i_min: int, or None
                minimum value for index, incrementing by 1.
            i_set: Symbol, or None
                set of indices. Useful if substituting the indices in later.
    '''
    OPTYPE = 'MultiSum'

    __new__ = object.__new__   # overwrite the __new__ from AbstractOperation.

    def __init__(self, summand, index=None, i_min_or_list=None, i_max=None, i_min=None, i_set=None):
        f = MultiSumSymbol(index=index, i_min_or_list=i_min_or_list, i_max=i_max, i_min=i_min, i_set=i_set)
        LinearOperation.__init__(self, f, summand)

    def _init_properties(self):
        return self.f._init_properties()

    def _new(self, summand, **kw):
        '''create a new MultiSum, using the same indices as self.'''
        init_props = self._init_properties()
        init_props.update(kw)
        result = type(self)(summand, **init_props)
        self._transmit_genes(result)
        return result

    summand = property(lambda self: self.x)

    index   = property(lambda self: self.f.index)
    @index.setter
    def index(self, value):   self.f = self.f._new(index=value)

    indices = property(lambda self: self.f.indices)
    @indices.setter
    def indices(self, value): self.f = self.f._new(i_min_or_list=value)

    i_max   = property(lambda self: self.f.i_max)
    @i_max.setter
    def i_max(self, value):   self.f = self.f._new(i_max=value)

    i_min   = property(lambda self: self.f.i_min)
    @i_min.setter
    def i_min(self, value):   self.f = self.f._new(i_min=value)

    is_expandable = property(lambda self: self.f.is_expandable)

    def __str__(self, internal=False, enc_inside='[]',**kw):
        '''string of self. Put summand in enc_inside if it is a Sum'''
        parens_implied = not isinstance(self.summand, Sum)
        return LinearOperation.__str__(self, internal=internal, enc_inside=enc_inside, parens_implied=parens_implied, **kw)

    def _repr_contents(self):
        '''returns contents to put inside 'MultiSum()' in repr for self.'''
        contents = [repr(self.summand), *self.f._repr_contents()]
        return contents

    def _equals0(self):
        if _equals0(self.summand):
            # summand is 0
            return True
        else:
            # if indices are known to be empty, the sum is 0.
            indices = self.indices
            if indices is None:
                return False
            elif isinstance(indices, AbstractOperation):
                return False
            else:
                return len(indices)==0

    def __eq__(self, b):
        '''tells whether self equals b.
        Might miss some cases of equality (some False negatives),
        but if result is True it is guaranteed (never False positives).
        '''
        if b is self:
            return True
        if _equals0(b):
            return _equals0(self)
        if not isinstance(b, MultiSum):
            return False
        if not self.indices == b.indices:
            return False
        if not self.index == b.index:   # relabel dummy index if necessary.
            b = b.relabel(self.index)
        return self.summand == b.summand

    def relabel(self, new_index):
        '''returns self with index relabeled.'''
        result = self.subsall((self.index, new_index))
        result.index = new_index
        return result

    def with_indices(self, i_min_or_list=None, i_max=None, i_min=None, i_set=None):
        '''return MultiSum like self but with indices set as indicated.'''
        result = self._new(self.summand)
        result.f = result.f.with_indices(i_min_or_list=i_min_or_list, i_max=i_max, i_min=i_min, i_set=i_set)
        return result

    ## BEHAVIOR ##
    def treats_as_constant(self, x):
        '''return whether self treats x as constant.'''
        if self.index is None:
            return is_constant(x)
        else:
            return not apply(x, '_contains_deep_anywhere_', self.index, default=False)

    ## SUBSTITUTIONS ##
    def __call__(self, *args, subs=[], subscripts=[], **kw):
        '''substitutions as normal but also check indices'''
        kw_all = dict(subs=subs, subscripts=subscripts, **kw)
        indices = self.indices
        if isinstance(indices, AbstractOperation):
            indices = indices(*args, **kw_all)
            self = self.with_indices(i_set=indices)
        else:
            try:
                next(iter(indices))
            except (TypeError, StopIteration):
                pass
            else:
                iresult = []
                for index in indices:
                    if isinstance(index, AbstractOperation):
                        index = index(*args, **kw_all)
                    iresult.append(index)
                indices = iresult
                self = self.with_indices(indices)
        self = super(MultiSum, self).__call__(*args, **kw_all)
        return self

    ## SIMPLIFYING ##
    _SIMPLIFY_OPS = ['_extract_constants']
    def _extract_constants(self, **kw__None):
        '''extracts variables which self treats as constant, where possible.
        Checks:
            if self treats summand as constant, return summand * multisum(1)
            if summand is a product, pull out all factors self treats as constant.
        '''
        if self.treats_as_constant(self.summand):
            return self.summand * self._new(1)
        elif is_opinstance(self.summand, Product):
            constant_factors = []
            other_factors = []
            for factor in self.summand:
                if self.treats_as_constant(factor):
                    constant_factors.append(factor)
                else:
                    other_factors.append(factor)
            if len(constant_factors) == 0:
                return self   # return self exactly, to indicate nothing changed.
            constant_prod = self.Product(*constant_factors)
            other_prod = self.Product(*other_factors)
            return constant_prod * self._new(other_prod)
        else:
            return self

    ## EXPANDING ##
    _EXPAND_OPS = ['_expand_multisum']
    _DEFAULT_FALSE_OPS = ['_expand_multisum']

    def _expand_multisum(self, **kw__None):
        '''expands the MultiSum into a regular Sum by plugging in each of the indices.'''
        if not self.is_expandable:
            return self
        summands = [self.summand.subsall((self.index, i)) for i in self.indices]
        return self.Sum(*summands)


''' --------------------- Operations - OperationContainer --------------------- '''

class OperationContainer(ao.OperationContainer, MultipleOperation):
    '''class for containing EquationSystem, Equation, Inequality.

    Operations on self will be applied to all objects contained by self.
        E.g. (LHS = RHS) + x --> LHS + x = RHS + x

    Also, __add__ and __radd__ have additional functionality here:
        adding two OperationContainer objects will always return an EquationSystem.

    And, we can track assumptions, using these rules:
        - Assumptions must be None, or an EquationSystem. (Equations are upcasted to EquationSystem.)
        - self.assumptions and self._assumptions_derived are tracked separately.
        - adding two objects with assumptions will add their assumptions as well.
            (this functionality is implemented in EquationSystem._add_system)
    '''
    def __init__(self, *terms, assumptions=None, _assumptions_derived=None,
                 _is_assumption=False, **kw):
        # super __init__
        MultipleOperation.__init__(self, *terms, **kw)
        # initialize assumptions.
        self._is_assumption = _is_assumption
        if not _is_assumption:    # an assumption is not allowed to have assumptions about it.
            default = self.EquationSystem(_is_assumption=True)  # empty equation system
            def get(assump):
                return default if assump is None else assump
            self.assumptions          = get(assumptions)   # "input" assumptions
            self._assumptions_derived = get(_assumptions_derived)  # "derived" assumptions

    def _new(self, *terms, **kw):
        '''new, but maintain assumptions of self unless assumptions are provided in kwargs.'''
        for assump in ('assumptions', '_assumptions_derived', '_is_assumption'):
            kw[assump] = kw.pop(assump, getattr(self, assump))
        result = type(self)(*terms, **kw)
        self._transmit_genes(result)
        return result

    ## ARITHMETIC ##
    def __add__(self, x):
        '''self + x. "overloading" 'add' based on type of x.
        EquationSystem or Equation --> "append" (like list addition)
        else --> add x to each Equation in self.
        '''
        if isinstance(x, OperationContainer):
            return self._add_system(x)
        else:
            return ao.OperationContainer.__add__(self, x)

    def __radd__(self, x):
        '''x + self. "overloading" 'add' based on type of x.
        EquationSystem or Equation --> "append" (like list addition)
        else --> add x to each Equation in self.
        '''
        if isinstance(x, OperationContainer):
            return x._add_system(self)
        else:
            return ao.OperationContainer.__radd__(self, x)

    def _add_system(self, system):
        '''adds system of equations to self.'''
        EqSys = self.EquationSystem   # this is the class for "EquationSystem" according to self.
        self   = EqSys(self)
        system = EqSys(system)
        # handle properties
        kwargs = dict()
        kwargs['solved'] = self.solved + [i + len(self) for i in system.solved]
        # handle assumptions
        if not (self._is_assumption or system._is_assumption):
            # compile assumptions
            for assump in ('assumptions', '_assumptions_derived'):
                kwargs[assump] =  getattr(self, assump) + getattr(system, assump)
            # take assumptions away from the equations inside self and system
            #  since those assumptions will belong to result of this function, instead.
            self   = [t._new(*t, assumptions=None, _assumptions_derived=None) for t in self]
            system = [t._new(*t, assumptions=None, _assumptions_derived=None) for t in system]
        # get result
        result = EqSys(*self, *system, **kwargs)   # equations are from *self and *system
        return result

    def add_assumptions(self, *assumptions, _derived=False):
        '''add assumptions to self.assumptions, or self._assumptions_derived if _derived.
        returns combined list of assumptions.
        '''
        assert not self._is_assumption, "Assumption is not allowed to own any assumptions."
        assump = '_assumptions_derived' if _derived else 'assumptions'
        result = getattr(self, assump, self.EquationSystem())
        result = result + self.EquationSystem(*assumptions)
        setattr(self, assump, result)  # self.assump = result
        return result

    ## OBJECT DISPLAY ##
    def _str_assumptions(self, str_in=None, tab='  ', align=True, index_from=None,
                         assumptions=True, hide_assumptions=False, **kw):
        '''return str_in with info about assumptions appended.

        str_in: string or None
            append assumptions info to this string.
            None -> just return assumptions string.
        tab, align, index_from, **kw:
            these kwargs go to assumptions.__str__.
            see EquationSystem.__str__ for details.
        assumptions (default True), hide_assumptions (default False): bool
            if either of these is flipped, return str_in without changing anything.
        '''
        if (not assumptions) or hide_assumptions:  # user has turned off showing assumptions
            return str_in
        if self._is_assumption:  # self is an assumption and is not allowed to own assumptions.
            return str_in
        # str for input assumptions
        result = [] if str_in is None else [str_in]   # result will be later joined via r' \\' + '\n'
        if len(self.assumptions) > 0:
            result.append('$Assumptions (input)$')
            result.append(_str(self.assumptions, tab=tab, align=align, index_from=index_from, **kw))
        # str for "derived" assumptions
        if len(self._assumptions_derived) > 0:
            result.append('$Assumptions ("derived")$')
            d_index = None if index_from is None else index_from + len(self.assumptions)
            result.append(_str(self._assumptions_derived, tab=tab, align=align, index_from=d_index, **kw))
        return (r' \\' + '\n').join(result)

    def _repr_contents(self, **kw):
        contents = ao.OperationContainer._repr_contents(self, **kw)
        if not self._is_assumption:
            if len(self.assumptions) > 0:
                contents.append('assumptions={}'.format(_repr(self.assumptions, **kw)))
            if len(self._assumptions_derived) > 0:
                contents.append('_assumptions_derived={}'.format(_repr(self._assumptions_derived, **kw)))
        return contents

    ## SIMPLIFYING ##
    _SIMPLIFY_OPS = ['_apply_assumptions']

    def _apply_assumptions(self, assumptions=[], **kw__None):
        '''applies assumptions, and all "imposed" (not "derived") assumptions, to self.
        (Doesn't apply "Inequality" assumptions because we aren't smart enough to do that yet.)
        '''
        for assumption in itertools.chain(assumptions, self.assumptions):
            if is_opinstance(assumption, Equation):
                self = self.substitute(assumption)
        return self


''' --------------------- Operations - Equation --------------------- '''

class SystemTooComplicatedError(NotImplementedError):
    '''this is the error when the equation system is too complex and we don't know how to solve it.'''
    pass


def _eqn_math_maker(op, opname=None):
        '''returns an eqn_math function which does op.'''
        def _eqn_math_doer(self, x):
            '''Does math with equations;
            if x is an Equation:
                returns Equation({op}(self[0], x[0]), {op}(self[1], x[1]))
            else:
                returns {op}(self, x)
            '''
            return self.eqn_math(op, x)
        if opname is not None:
            _eqn_math_doer.__name__ = 'eqn_{op}'.format(op=opname)
            _eqn_math_doer.__doc__  = _eqn_math_doer.__doc__.format(op=opname)
        return _eqn_math_doer

class Equation(OperationContainer, BinaryOperation):
    OPTYPE = 'Equation'
    _relation = '='

    def __str__(self, **kw):
        result = _str(self[0], internal=False, **kw) + ' = ' + _str(self[1], internal=False, **kw)
        return self._str_assumptions(str_in=result, **kw)

    ## ARITHMETIC ##
    def __truediv__(self, b):
        '''self / b. arithmetic is defined in OperationContainer.
        But here we add the assumption that we do not divide by 0, i.e. b != 0.
        '''
        result  = self.op(lambda x: x / b)
        if DIV0_TRACKING and isinstance(b, ao.AbstractOperation):
            result.add_assumptions(self.Inequality(b, '!=', 0), _derived=True)
        return result

    ## SUBSTITUTIONS ##
    def substitute(self, *substitutions):
        '''substitutes substitutions into self.
        for any substitution which is an Equation (instead of a tuple),
            if that substitution changes any term in self,
                associate any assumptions in self into the result.
        '''
        result = self
        for sub in substitutions:
            orig   = result
            result = super(Equation, result).substitute(sub)
            if isinstance(sub, Equation):
                if orig != result:
                    result.add_assumptions(sub.assumptions)
                    result.add_assumptions(sub._assumptions_derived, _derived=True)
        return result

    ## CONVENIENCE ##
    def eqn_math(self, op, x):
        '''Does math with equations;
        if x is an Equation:
            returns Equation(op(self[0], x[0]), op(self[1], x[1]))
        else:
            returns op(self, x)
        '''
        if isinstance(x, Equation):
            return self._new(op(self[0], x[0]), op(self[1], x[1]))
        else:
            return op(self, x)

    eqn_add = _eqn_math_maker(lambda self, x: self + x, opname='add')
    eqn_sub = _eqn_math_maker(lambda self, x: self - x, opname='sub')
    eqn_mul = _eqn_math_maker(lambda self, x: self * x, opname='mul')
    eqn_div = _eqn_math_maker(lambda self, x: self / x, opname='div')

    def subtract_rhs(self):
        '''returns self[0] - self[1] = 0.'''
        return self._new(self[0] - self[1], 0)

    def subtract_lhs(self):
        '''returns 0 = self[1] - self[0]'''
        return self._new(0, self[1] - self[0])

    def degree(self, x, upper=True):
        '''returns degree of x in self. if upper, return largest degree; else return smallest degree.'''
        return self.Sum(self[0], self[1]).degree(x, upper=upper)

    def polynomialize(self, x, monic=False, **kw):
        '''converts self to be an equation between two Polynomials in x.
        if monic, converts self to be (mp(x) = 0),
            where mp(x) is a monic polynomial in x.
            ("monic" <--> "coefficient of term with largest degree equals 1")
        '''
        self = self.numerate(x)
        if monic:
            lhs = self[0] - self[1]
            rhs = 0
            k, mp = apply(lhs, 'polynomialize', x, monic=True, _monic_internal=True)
            result = self._new(mp, 0)
            if DIV0_TRACKING and isinstance(k, ao.AbstractOperation):
                result.add_assumptions(self.Inequality(k, '!=', 0), _derived=True)
        else:
            lhs = apply(self[0], 'polynomialize', x, **kw)
            rhs = apply(self[1], 'polynomialize', x, **kw)
            result = self._new(lhs, rhs)
        return result

    def polyfractize(self, x, **kw):
        '''converts self to be an equation between two PolyFracs in x.'''
        lhs = apply(self[0], 'polyfractize', x, **kw)
        rhs = apply(self[1], 'polyfractize', x, **kw)
        return self._new(lhs, rhs)

    def polysimplified(self, x, **kw):
        '''converts self to be a polynomial in x, and simplifies coefficients.'''
        self = self.numerate(x)
        self = self - self[1]
        rhs = 0
        lhs = apply(self[0], 'polysimplified', x, **kw)
        return self._new(lhs, rhs)

    def _numerate_step(self, x):
        '''multiplies self by the first denominator in self which contains x.'''
        mul = self._first_denominator_appearance(x)
        if mul is not None:
            result = self * mul
            result = result.apply('_distribute', focus_only=mul)
            result = result.apply('_collect', collect_sum=False)   # collect powers and products.
            return result
        else:
            return self

    def numerate(self, x, print_freq=5):
        '''multiplies by things as needed, to ensure x is not in any denominators, in self.'''
        updater = tools.ProgressUpdatePrinter(print_freq=print_freq, wait=True)
        self_prev = None
        i = 0
        while self_prev is not self:
            i+=1
            updater.print('beginning numerate step {}.'.format(i), print_time=True, end='\r')
            self_prev = self
            self = self._numerate_step(x)
        updater.finalize(process_name='numerate')
        return self

    ## SOLVING ##
    def eliminate(self, x, show_work=False, **kw__simplified):
        '''eliminate x from self, if possible.
        if self looks like x * A = 0, then return A = 0.
        else, raise SystemTooComplicatedError.
        '''
        explain = tools.debug_viewer(show_work)
        focus = self.least_soluble_in(x)
        if focus is False:
            raise SystemTooComplicatedError("Equation is not soluble in {}:\n{}", format=[x, self])
        self = (self - self[1]).simplified(focus_only=focus, **kw__simplified)
        lhs = self[0]   # rhs = 0.
        if not (is_opinstance(lhs, Product) and x in lhs):
            raise SystemTooComplicatedError("To eliminate x={}, need equation like x * A = 0.".format(x))
        explain('Equation looks like (A * ({}) == 0). This implies (A == 0)', format=[x])
        lhs = lhs.without_term(x)
        if lhs._contains_deep_(x):
            raise SystemTooComplicatedError("Equation looks like A(x) * x = 0; don't know how to solve for x={}".format(x))
        return self._new(lhs, 0)

    def solubility_list(self, x):
        '''returns the lists of solubilities for self.'''
        return [s for side in self for s in solubility_list(side, x)]

    def solvable(self, x, _return_solubility=False):
        '''returns the name of the function to use for solving self for x,
        or False if we do not know how to solve self for x.

        if _return_solubility, instead return a tuple with (result, solubility_list(x))

        TODO: if solub == tools.Set([x]), we have something like Ax = 0.
            In this case, we should return the system [(A = 0) OR (x = 0)],
            unless we are also provided an assumption that A!=0 or x!=0.
        '''
        solub = self.solubility_list(x)
        if tools.Set(solub) in (tools.Set([x, None]), tools.Set([x])):
            result = 'linsolve'
        else:
            result = False
        if _return_solubility:
            return (result, solub)
        else:
            return result

    def solve(self, x, *args, **kw):
        '''solve self for x.
        Subclasses should not need to overwrite solve(),
            but should instead overwrite solvable(), and add new solving helper functions.
        '''
        fstr, _solubility = self.solvable(x, _return_solubility=True)
        if fstr is False:
            cant_solve_errmsg = "Equation too hard to solve for {}:\n{}".format(x, self) + \
                                "\n(Got solubility_list = {})".format(_solubility)
            raise SystemTooComplicatedError(cant_solve_errmsg)
        else: # solve and return.
            f = getattr(self, fstr) # if self doesn't have fstr, can't solve, and error would be raised.
            return f(x, *args, _assert_solvable=kw.pop('_assert_solvable', False), **kw)

    # -- solving, helper functions -- #

    def linsolve(self, x, ldebug=False, simplify_result=True, _assert_solvable=True, **kw__simplify):
        '''solve self for x.
        Only works if self is linear in x.

        TODO: allow option to simplify or not simplify the result.
        E.g. one might want to leave numbers, rather than evaluate numbers.
        '''
        if _assert_solvable:
            assert self.solvable(x) == 'linsolve'

        if not simplify_result:
            kw__simplify['distribute_if'] = lambda self: self._contains_deep_(x)
        
        self = self.subtract_rhs()   # put stuff on LHS
        self = self.simplified(collect_only=[x], **kw__simplify)
        # << now equation should look like Ax + B = 0
        explain = tools.debug_viewer(ldebug)
        explain('After bringing RHS to LHS, eqn looks like:', view=self)
        if isinstance(self[0], Sum):  # equation is: Ax + B = 0,  and B exists (isn't 0).
            ## subtract B from both sides: Ax + B - B = -B
            to_keep, to_subtract = self[0].split(lambda term: apply(term, '_contains_deep_', x, default=False))
            self = self._new(to_keep, self[1] - to_subtract)
            #self = self.simplified(evaluate_abstract_only=True, **kw__simplify)
            # << now equation should look like Ax = -B
            explain('After bringing B (from Ax + B = 0) to RHS, eqn looks like:', view=self)
        if isinstance(self[0], Product):  # equation is: Ax = -B,  and A exists (isn't 1)
            ## divide A from both sides: Ax / A = -B / A
            to_keep, to_divide = self[0].split(lambda factor: apply(factor, '_contains_deep_', x, default=False))
            self = self._new(to_keep, self[1] / to_divide)
            #self = (self / to_divide).simplified(only=['simplify_id', 'flatten', 'distribute'],
            #                                     focus_only=to_divide, **kw__simplify)
            explain('After bringing A (from Ax = -B) to RHS, eqn looks like:', view=self)
            assert self[0] == x, 'Expected LHS = x, but got LHS = {}'.format(self[0])
        elif self[0] == x:
            pass
        else:
            raise SystemTooComplicatedError('Equation too hard to linsolve (expected Ax=-B form, x={}):\n{}'.format(x, self))
        # << now equation should look like x = -B / A. (We are basically done!)
        if simplify_result:
            explain('Simplifying... ', end='', flush=True)
            self = self.simplified(**kw__simplify)
            explain('After simplifying, eqn looks like:', view=self)
        return self

    ## SIMPLIFYING ##
    _SIMPLIFY_OPS = ['_divide_common_factor']

    def _divide_common_factor(self, **kw__None):
        '''a b x = a c y --> b x = c y.
        g x = 0 --> x = 0  (if g is constant AND x is not constant)

        [TODO] seems to be turning ((g x = 0).subs([g, 0])) into (x = 1)...
        '''
        if self[0] == self[1]:
            return self
        factor, lhs_over_f, rhs_over_f = gcf(self[0], self[1])
        if factor == 1:
            # check for this case:   g x = 0 --> x = 0  (if g is constant AND x is not constant)
            if is_opinstance(self[0], Product):
                constants, varys = tools.split(self[0], lambda y: is_constant(y))
                if (len(varys) > 0) and (len(constants) > 0) and equals(self[1], 0):
                    constant = self[0]._new(*constants)
                    if not equals(constant, 0):                  # constant(s) != 0.
                        result = self._new(self[0]._new(*varys), self[1])    #  g x = 0 --> x = 0
                        if DIV0_TRACKING and isinstance(constant, AbstractOperation):   # track the assumption g != 0.
                            result.add_assumptions(self.Inequality(constant, '!=', 0), _derived=True)
                        self = result
            elif is_opinstance(self[1], Product):   # [TODO] encapsulate this code ^ v
                constants, varys = tools.split(self[1], lambda y: is_constant(y))
                if (len(varys) > 0) and (len(constants) > 0) and equals(self[0], 0):
                    constant = self[1]._new(*constants)
                    if not equals(constant, 0):
                        result = self._new(self[0], self[1]._new(*varys))
                        if DIV0_TRACKING and isinstance(constant, AbstractOperation):
                            result.add_assumptions(self.Inequality(constant, '!=', 0), _derived=True)
                        self = result
            return self
        else:
            result = self._new(lhs_over_f, rhs_over_f)
            if DIV0_TRACKING:
                result.add_assumptions(self.Inequality(factor, '!=', 0), _derived=True)
            return result


''' --------------------- Operations - EquationSystem --------------------- '''

class EquationSystem(OperationContainer, MultipleOperation):
    '''a system of equations. Initialize via EquationSystem(eq1, eq2, ...).

    *eqns:
        Equation objects to put in the EquationSystem.
    assumptions: (default None)
        EquationSystem object with the list of assumptions attached to self.
        This should contain the assumptions which are "imposed" on self, "externally",
            and will simply be tracked for easier bookkeeping purposes.
        Feel free to enter any EquationSystem here that you want to.
    _assumptions_derived: (default None)
        EquationSystem object with the list of "derived" assumptions attached to self.
        This should containt he assumptions which are "derived" through solving,
            and will be tracked for bookkeeping / "warning" the user.
        For example, solving [A x = B] for x will yield [x = B/A], but
            with [A != 0] put into _assumptions_derived.
        It is intented that _assumptions_derived should only be altered internally.
    solved: (default [])
        list of indices telling which equations in self have already been solved.
        EquationSystem solvers should ignore any solved equations,
            after plugging their contents into the other equations.

    Behaves like a list when iterating or indexing.
    E.g. eqs[3], eqs[5:8], and (eq for eq in eqs) are valid.
    ALSO, can attempt to index with an AbstractOperation object.
    E.g. eqs[Symbol('x')] will return the first equation with LHS==Symbol('x').
    '''
    OPTYPE = 'EquationSystem'

    ## OBJECT CREATION ##
    def __new__(cls, *eqns, **kwargs):
        '''construct new EquationSystem (usually), but first check if it's appropriate.
        if *eqns has length 1, AND eqns[0] IS an EquationSystem, return eqns[0] instead.
        '''
        if len(eqns) == 1:
            if is_opinstance(eqns[0], EquationSystem):
                return eqns[0]
        # < if we get to this point, none of the exceptions applied.
        instance = object.__new__(cls)
        instance.__init__(*eqns, **kwargs)
        return instance

    def __init__(self, *eqns, solved=[], **kw):
        # remove duplicate equations, while tracking "solved" properly.
        eqns_unique   = []
        solved_unique = []
        for i, eq in enumerate(eqns):
            j = tools.find(eqns_unique, eq, equals=equals)
            if j is None:   # eq not in eqns_unique
                eqns_unique.append(eq)
                if i in solved:   # i is a solved equation according to "solved".
                    solved_unique.append(i)
            elif (i in solved) and (not j in solved):
                # eqns[i] == eqns[j], i is "solved", j was marked as "not solved"
                solved_unique.append(j)   # mark j as "solved".
                # TODO: track assumptions in this case as well.
            # else, continue; we have already seen this equation.
        # >> actually do the initialization work here <<
        OperationContainer.__init__(self, *eqns_unique, **kw)
        self.solved=solved_unique
        # bookkeeping on assumptions - gather eqns assumptions into EquationSystem assumptions.
        if not self._is_assumption:
            for eqn in eqns:
                self.add_assumptions(*eqn.assumptions)
                self.add_assumptions(*eqn._assumptions_derived, _derived=True)

    def _new(self, *eqns, **kw):
        '''if len(eqns) == len(self), use self.solved for result.solved, unless solved in kw.'''
        if ('solved' not in kw) and (len(eqns) == len(self)):
            kw['solved'] = self.solved
        for assump in ('assumptions', '_assumptions_derived', '_is_assumption'):
            kw[assump] = kw.pop(assump, getattr(self, assump))
        result = type(self)(*eqns, **kw)
        self._transmit_genes(result)
        return result

    ## OBJECT DISPLAY ##
    def __str__(self, tab='  ', align=True, index_from=None,
                hide_assumptions=False, show_solved=None, **kw):
        '''tab is put before each equation.
        align is whether to put in an align block (aligns on '=').
        index_from:
            None --> don't put any numbers to label the equations
            number --> label equations with numbers (e.g. (1)), starting with index_from.
        show_solved:
            True  --> show all equations in the system, even if they are "solved".
            False --> only show "unsolved" equations in the system.
            None  --> only show "unsolved" equations, except if all are "solved" then show all, instead.
        '''
        # str for equations
        if show_solved or (show_solved is None and len(self.solved) == len(self)):
            ieqs, eqs = list(range(len(self))), [eq for eq in self]
        elif len(self.solved) == len(self):
            result = (' $EquationSystem({} equations, all solved); '
                     'Use show_solved=True to show all solved equations.$ ')
            # (the ' $', '$ ' are so that during view(), it looks like plaintext, not mathtext.)
            return result.format(len(self))
        else:
            ieqs, eqs = zip(*[(i, self[i]) for i in self.unsolved])
        eqs = [tab + _str(eq, hide_assumptions=True, **kw) for eq in eqs]
        relations = [eq._relation for eq in self]
        if align: 
            eqs = [eq.replace(relation, '&'+relation) for eq, relation in zip(eqs, relations)]
        if index_from is not None:
            sep = r'&&' if align else r'\quad'
            eqs = [r'({}{})    {}    {}'.format(i+index_from, '*' if i in self.solved else '', sep, eq) for i, eq in zip(ieqs, eqs)]
        eqstr = (r' \\' + '\n').join(eqs)   # \\ tells latex "newline"
        s = ''
        if align:  s += r'\begin{align}' + '\n'
        s += eqstr
        if align:  s += '\n' + r'\end{align}'
        if hide_assumptions:
            return s
        else:
            a_index = index_from if index_from is None else index_from + len(self)
            return self._str_assumptions(str_in=s,
                                         tab=tab, align=align, index_from=a_index, **kw)

    ## SUBSTITUTIONS ##
    def __call__(self, *args, unsolved_only=True, **kw):
        '''substitute into each equation in self; only the unsolved ones if unsolved_only.'''
        eqns = [eqn for eqn in self]
        unsolved = self.unsolved if unsolved_only else range(len(self))
        for i in unsolved:
            eqns[i] = eqns[i](*args, **kw)
        return self._new(*eqns)

    def __setitem__(self, i, equation, solved=False):
        '''create a new EquationSystem like self but replace self[i] with eqn.
        if solved, also indicate that i is solved, in the new system.
        '''
        eqns = list(self)
        eqns[i] = equation
        kw = ( dict(solved = self.solved + [i]) ) if solved else dict()
        return self._new(*eqns, **kw)

    replace_equation = property(lambda self: self.__setitem__)  # alias
    eqn_replaced     = property(lambda self: self.__setitem__)  # alias    
    
    ## CONVENIENCE ##
    def _get_index_equality_check(self, eqn, abstract_op):
        '''tells whether abstract_op equals the LHS of eqn.
        This allows to use self.get_index(x) to find (first) equation with LHS=x,
        and self[x] to get (first) equation with LHS=x.
        '''
        return eqn[0] == abstract_op

    def mark_solved(self, *solved):
        '''creates a new EquationSystem with the equations indexed by solved marked as solved,
        in addition to whichever equations were already marked as solved.

        negative indices are allowed; they will be replaced by the corresponding index to range(len(self))
        '''
        idx = range(len(self))
        solved = [idx[s] for s in solved]
        return self._new(*self, solved=list(set(self.solved + list(solved))))

    def mark_unsolved(self, *unsolved):
        '''creates a new EquationSystem with the equations indexed by unsolved marked as unsolved.
        Any other equations marked as solved will continue to be marked as solved in the new system.
        '''
        idx = range(len(self))
        unsolved = [idx[s] for s in unsolved]
        return self._new(*self, solved=list(set(self.solved) - set(unsolved)))

    def solution_put(self, i_eqn, var, subs=False, **kw__solve):
        '''solve eqn i_eqn for var var, then return new system where i_eqn is that solution and is marked as solved.
        if subs, also sub the solution into all the other unsolved equations in the result.
        '''
        if subs:
            result = self.solution_put(i_eqn, var, subs=False, **kw__solve)
            result = result.subs(result[i_eqn], unsolved_only=True, **kw__solve)
            return result
        eqns = list(self)
        eqns[i_eqn] = eqns[i_eqn].solve(var, **kw__solve)
        solved = list(set(self.solved + [i_eqn]))
        return self._new(*eqns, solved=solved)

    def drop_equations(self, *indices):
        '''return an EquationSystem with the indicated equations removed.'''
        solved = tools.pop_index_tracker(self.solved, indices)
        eqns   = [eqn for i, eqn in enumerate(self) if not i in indices]
        return self._new(*eqns, solved=solved)

    def append_equations(self, eqns):
        return self._new(*self, *eqns)

    append = property(lambda self: self.append_equations)  # alias

    def polynomialize(self, x, **kw):
        return self.op(lambda eqn: apply(eqn, 'polynomialize', x, **kw), _prevent_new=True)

    def numerate(self, x):
        return self.op(lambda eqn: apply(eqn, 'numerate', x), _prevent_new=True)

    ## SOLVING ##
    @property
    def unsolved(self):
        '''list of indices for which equations in self are not solved.'''
        solved = self.solved
        return [i for i in range(len(self)) if i not in solved]

    def unsolved_system(self):
        '''return EquationSystem with only the unsolved equations from self.'''
        return self._new(*[self[i] for i in self.unsolved])

    def solubility_list(self, x):
        '''returns the lists of solubilities for self.'''
        return [apply(t, 'solubility_list', x, default=[None]) for t in self]

    def linsolve_step(self, x, **kw):
        '''take one step of trying to eliminate terms from equations'''
        # maybe I should define this in linearizing.py instead...
        #   yeah. It will be simpler to write the first time there.
        #   then I can maybe abstract it to here...
        #      But that's probably outside the scope of original project.
        raise NotImplementedError

    def coef_matrix(self, targets):
        '''returns the coefficient matrix for (self, targets).
        c00 t0 + c01 t1 + ... + c0N tN = 0
        ...
        cN0 t0 + cN1 t1 + ... + cNN tN = 0

        returns C.

        requires same number of equations as targets.
        '''
        if len(targets) != len(self):
            errmsg = ('Number of equations ({}) not equal to number of targets ({})')
            errmsg = errmsg.format(len(self), len(targets))
            raise ValueError(errmsg)
        self = self._new(*[(eqn - eqn[1])._simplyfied() for eqn in self])
        self = self.apply('collect', collect_only=targets)
        assert all(eqn[1] == 0 for eqn in self), 'Expected all RHSs to be 0 at this point.'
        C = [[0 for t in targets] for eqn in self]   # coefficient matrix.
        for i, eqn in enumerate(self):
            terms = get_summands(eqn[0])
            for term in terms:
                factors = get_factors(term)
                for f, factor in enumerate(factors):
                    t = tools.find(targets, factor)
                    if t is not None:
                        C[i][t] = self.Product(*tools._list_without_i_(factors, f))
                        break
        raise NotImplementedError('Untested')
        return C

    def matformat(self, targets):
        '''convert self to matrix format.'''
        C = self.coef_matrix(targets)
        eqns = [self.Equation(
                    self.Sum(*[
                        self.Product(cij, term) for cij, term in zip(c_row, targets)
                    ])
                ) for c_row in C ]
        raise NotImplementedError('Untested')
        return self._new(eqns)

    def detsolve(self, targets):
        '''remove targets from self by taking the determinant of the coefficient matrix.
        Only works for a system of equations which is linear in the targets, and
        has the same number of equations as the targets.
        '''
        raise NotImplementedError


''' --------------------- Operations - Inequality --------------------- '''

def inequality_interpret(relation_str, mode='magic'):
    '''converts relation_str to the corresponding string in the given mode.
    E.g. (' != ', mode='magic') --> '__ne__';  ('__gt__', mode='latex') --> r'\gt'

    result will always be stripped (no leading / trailing whitespace).

    relation_str: string representing the relation in some mode.
        insensitive to leading and trailing spaces.
    mode: string telling which mode to use.
        case-insensitive.
        'magic' or 'dunder' --> convert to python magic method.
            E.g. '__ne__', '__lt__', '__ge__'
        'latex' or 'mathtext' --> convert to latex format.
            E.g. r'\neq', r'\lt', r'\geq'
        'python' or 'code' --> convert to python code format.
            E.g. '!=', '<', '>='
    '''
    # mode (lookup)
    mode = mode.lower()
    if mode in ('magic', 'dunder'):
        mode = 0
    elif mode in ('latex', 'mathtext'):
        mode = 1
    elif mode in ('python', 'code'):
        mode = 2
    else:
        raise ValueError("Expected mode in ('magic', 'latex', 'python') but got mode={}".format(repr(mode)))
    # relations (lookup table)
    eq = ('__eq__',   '='  , '==')
    gt = ('__gt__', r'\gt' , '>' )
    lt = ('__lt__', r'\lt' , '<' )
    ne = ('__ne__', r'\neq', '!=')
    ge = ('__ge__', r'\geq', '>=')
    le = ('__le__', r'\leq', '<=')
    RELATIONS = (eq, gt, lt, ne, ge, le)
    # relation string (lookup)
    relation_str = relation_str.strip()
    for reltype in RELATIONS:
        for s in reltype:
            if relation_str == s:
                return reltype[mode]
    raise ValueError("Unrecognized relation_str: {}".format(relation_str))

class Inequality(OperationContainer, BinaryOperation):
    '''class for representing inequalities, e.g. x != 0.'''
    def __init__(self, t1, relation_str, t2, **kw):
        self.relation_str = relation_str
        OperationContainer.__init__(self, t1, t2, **kw)

    def _new(self, t1, t2, **kw):
        result = type(self)(t1, self.relation_str, t2, **kw)
        self._transmit_genes(result)
        return result

    @property
    def _relation(self):
        return inequality_interpret(self.relation_str, mode='latex')

    def __str__(self, mode='latex', **kw):
        relation = inequality_interpret(self.relation_str, mode=mode)
        lhs = _str(self.t1, **kw)
        rhs = _str(self.t2, **kw)
        result = '{} {} {}'.format(lhs, relation, rhs)
        return self._str_assumptions(str_in=result, **kw)

    def _repr_contents(self, **kw):
        return [_repr(self.t1, **kw), repr(self.relation_str), _repr(self.t2, **kw)]