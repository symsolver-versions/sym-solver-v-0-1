#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on ??? 2021

@author: Sevans

File purpose: Miscellaneous quality-of-life functions.

This file is intended to provide functions which:
- are helpful for solving specific, small problems
- could be useful in other projects as well
"""

# import builtin modules
import os
import sys
import importlib
import warnings
import time
import collections

# import external public modules
try:
    import numpy as np
except ImportError:
    print('(WWW) numpy failed to import. Some functions in SymSolver.tools may break.')
    np = ImportError('failed to import numpy')
try:
    import matplotlib.pyplot as plt
except ImportError:
    print('(WWW) matplotlib failed to import. Some functions in SymSolver.tools may break.')
    plt = ImportError('failed to import matplotlib')

''' ----------------------------- Defaults ----------------------------- '''
TRACEBACKHIDE = True
class OnOffSwitch():
    def __init__(self, state):   self.state = state
    def __bool__(self): return bool(self.state)
    def enable(self):
        self.state = True
    def disable(self):
        self.state = False
    on  = property(lambda self: self.enable)  # alias for enable
    off = property(lambda self: self.disable) # alias for disable
    def flip(self):
        self.state = not self.state
        return self.state
    def __repr__(self):
        return 'OnOffSwitch <{}> in state {}'.format(hex(id(self)), self.state)
RENDER_MATH = OnOffSwitch(True)   # use RENDER_MATH.disable() to turn it off, or enable() to turn it on.
RENDER_MATH.maxlen = 10000  # if length of string is more than this many characters, don't render as math.

''' ----------------------------- Misc. ----------------------------- '''

def _repr(x, *args, **kw):
    '''repr, applying args and kwargs if possible'''
    try:
        return x.__repr__(*args, **kw)
    except TypeError:
        return x.__repr__()

def _str(x, *args, **kw):
    '''str, applying args and kwargs if possible'''
    try:
        return x.__str__(*args, **kw)
    except TypeError:
        return x.__str__()

def view(x, *args__str, **kw__str):
    '''view x as fancy math string.
    x can be any object as long as repr(x) converts it to math string.
    if x is a string, use x instead of repr(x)

    import IPython.display inside this function in order to reduce overhead.
    '''
    if not isinstance(x, str):
        x = _str(x, *args__str, **kw__str)
    if RENDER_MATH:
        if len(x) > RENDER_MATH.maxlen:
            # string is too large; don't render as math.
            warnmsg_large_str = ('String at <{}> too large to render as math. '
                                 '(Got {} chars; limit is {}.) (To increase the limit, '
                                 'set RENDER_MATH.maxlen to a larger value.)').format(
                                 hex(id(x)), len(x), RENDER_MATH.maxlen)
            warnings.warn(warnmsg_large_str)
        else:
            # render as math
            import IPython.display as ipd
            ipd.display(ipd.Math(x))
            return
    # else
    print(x)

def apply(x, fstr, *args, **kwargs):
    '''return x.fstr(*args, **kwargs), or x if x doesn't have an 'fstr' attribute.'''
    __tracebackhide__ = TRACEBACKHIDE
    # pop default if it was provided.
    doing_default = 'default' in kwargs
    if doing_default:
        default = kwargs.pop('default')
    # call x.fstr(*args, **kwargs)   # (kwargs with 'default' popped.)
    if hasattr(x, fstr):
        return getattr(x, fstr)(*args, **kwargs)
    elif doing_default:
        return default
    else:
        return x

def short_repr(x, n=200):
    '''if repr(x) is longer than n characters, return object.__repr__(x) instead.'''
    result = repr(x)
    if (x.__repr__ == object.__repr__) or len(result) <= n:
        return result
    else:
        return object.__repr__(x)

def is_integer(x):
    return isinstance(x, int) or apply(x, 'is_integer', default=False)

def debug_viewer(debug=False):
    '''returns a function which prints and shows things if debug, else does nothing.
    if converting objects to strings is non-trivial, use the format kwarg:
        Example:
            explain = debug_viewer(debug)
            # the next line will always convert x to string, even if debug is False:
            explain('Did a thing to: {}'.format(x))
            # the next line will hit the input string with .format(*[x]), only if debug is True:
            explain('Did a thing to: {}', format=[x])
        Note the format kwarg is only compatible when there is 1 arg entered, for simplicity.
    '''
    if debug:
        _view_f = view   # this is the function view() from above
        def print_and_view(*args, view=None, format=[], **kw):
            if len(args)>0:
                if len(args)==1:
                    print(args[0].format(*format), **kw)
                elif len(format) > 0:
                    raise TypeError('kwarg "format" only available when precisely 1 arg is entered to the debug_viewer.')
                else:
                    print(*args, **kw)
            if view is not None:
                _view_f(view)
        return print_and_view
    else:
        def do_nothing(*args, **kw):
            pass
        return do_nothing

def print_clear(N=80):
    '''clears current printed line of up to N characters, and returns cursor to beginning of the line.
    debugging: make sure to use print(..., end=''), else your print statement will go to the next line.
    '''
    print('\r'+ ' '*N +'\r',end='')

def argsort(x, reverse=False, key=lambda y: y):
    '''does an argsort using pythons builtin function: sorted'''
    return [ix[0] for ix in sorted(zip(range(len(x)), x), key=lambda ix: key(ix[1]), reverse=reverse)]

def deep_iter(x, breadth_first=False):
    '''iterate through all terms at all layers of x
    breadth_first --> whether to do all at current layer first.
    '''
    yield x
    try:
        iter(x)
    except TypeError: # x is not iterable
        return   # stop generating.
    if breadth_first:
        raise NotImplementedError('deep_iter(breadth_first=True) is not implemented properly yet.')
        todos = []
        for term in x:
            #yield term
            todos.append(term)
        for t in deep_iter(todos, True):
            yield t
    else:  # depth first
        for term in x:
            for t in deep_iter(term, False):
                yield t

def _pop_kw(kw, defaults):
    '''pops keys in defaults (a dict) from kw (a dict).
    returns dict of popped values (defaulting to vals in defaults if key is not in kw).
    '''
    result = dict()
    for key in defaults.keys():
        result[key] = kw.pop(key, defaults[key])
    return result

def symlink(src, newfile, relpath=True):
    '''os.symlink, but with option to make link path be relative.
    src    : string; link will point AT this file.
    newfile: string; this file will be created.
    relpath: True (default) or False
        True  -> dst stores a relative path to src.
                 if dst and src are moved, but not moved relative to each other, link should still work.
        False -> dst stores an absolute path to src.
                 if dst is moved, link should still work. If src is moved, link will break.
    '''
    link = os.path.relpath(src, os.path.dirname(newfile)) if relpath else os.path.abspath(src)
    return os.symlink(link, newfile)

def default_sum(*terms, default=(None, 0)):
    '''return sum of terms, but treating defaults appropriately.
    default: (default_object, value)
        while adding terms, treat default_object (compared via 'is') as value.
        And, if all terms are equal to default_object, return default_object.
    '''
    result = default[0]
    for term in terms:
        if result is default[0]:
            result = term
        else:
            if term is default[0]:
                term = default[1]
            result += term
    return result

def _list_without_i_(l, i):
    '''returns list without i'th element. (nondestructive)'''
    return [elem for j, elem in enumerate(l) if j!=i]

def equal_sets(x, y):
    '''returns whether x and y are equal in the sense of sets.
    equal when:
        for i in x, i in y, AND for j in y, j in x.

    result will be equivalent to Set(x) == Set(y),
    however equal_sets(x, y) is more efficient in terms of speed.
    '''
    # loop through terms, pop same terms until all are popped or one is missing.
    y = [t for t in y]
    popped = []
    for term in x:
        j = 0
        found_match = False
        while j < len(y):
            if equals(term, y[j]):
                yjpop = y.pop(j)    # we found a match for y[j]; it is term.
                if not found_match:        # if this is the first match to term, record it,
                    popped.append(yjpop)   # so that later we can compare terms against popped.
                found_match = True
            else:
                j += 1
        if (not found_match) and (not any(equals(term, z) for z in popped)):
            return False
    if len(y) > 0:   # there is at least 1 element in y which was not in x.
        return False
    return True

class Set():
    '''"set" which is inefficient (doesn't use hashing) but otherwise behaves like a set.'''
    def __init__(self, contents):
        self.contents = contents
        self._reduce()

    def _reduce(self):
        '''remove duplicates in self'''
        result = []
        for s in self.contents:
            if s not in result:
                result.append(s)
        self.contents = result

    def __eq__(self, x):
        '''returns self == x. Equality holds when all of the following are True:
        - for s in self, s in x.
        - for y in x, y in self.
        - len(x) == len(self)
        '''
        if x is self:
            return True
        # check lengths first for efficiency in easy False case.
        if len(self) != len(x):
            return False
        # loop through terms, pop same terms until all are popped or one is missing.
        s = [t for t in self]
        x = [t for t in x]
        while len(s) > 0:
            j = 0
            found_match = False
            while j < len(x):
                if equals(s[0], x[j]):
                    found_match = True
                    s.pop(0)
                    x.pop(j)
                    break
                else:
                    j += 1
            if not found_match:
                return False
        return True

    def __repr__(self):
        return 'Set({})'.format(self.contents)

    def __iter__(self):
        return iter(self.contents)

    def __getitem__(self, i):
        return self.contents[i]

    def __len__(self):
        return len(self.contents)

    def __add__(self, b):
        return Set(list(self) + list(b))

    def __radd__(self, b):
        return Set(list(b) + list(self))

    def __sub__(self, b):
        return Set([s for s in self if not s in b])

def counts(x):
    '''converts x (an iterable) to a list of tuples: (y, number of times y appears in x).'''
    result = []
    for y in x:
        for zi, [z, zcount] in enumerate(result):
            if equals(y, z):
                result[zi][1] += 1
                break
        else:  # didn't find y in result
            result.append([y, 1])
    return result

def counts_idx(x):
    '''converts x (an iterable) to a list of tuples: (y, list of indices where y appears in x).'''
    result = []
    for yi, y in enumerate(x):
        for zi, [z, zidx] in enumerate(result):
            if equals(y, z):
                result[zi][1].append(yi)
                break
        else:  # didn't find y in result
            result.append([y, [yi]])
    return result

def counts_sublist_indices(ll):
    '''converts ll (an iterable of iterables) to a list of tuples:
        [(y, dict of xi: [list of yi] such that ll[xi][yi]==y), for x in ll for y in x]
    '''
    result = []
    for xi, x in enumerate(ll):
        for yi, y in enumerate(x):
            for zi, [z, z_indices] in enumerate(result):
                if equals(y, z):
                    try:
                        result[zi][1][xi].append(yi)
                    except KeyError:
                        result[zi][1][xi] = [yi]   # initialize result[zi][1][xi].
                    break
            else:  # didn't find y in result
                result.append([y, {xi: [yi]}])
    return result

def split(x, condition=lambda y: y):
    '''Returns ([y for y in x if condition(y)], [y for y in x if not condition(y)])'''
    good = []
    bad  = []
    for y in x:
        (good if condition(y) else bad).append(y)
    return (good, bad)

def pop_index_tracker(idx, popping):
    '''returns a new list of indices when popping are popped from the list that idx corresponds to.

    - decrements all indices larger than 7 by 1 if 7 is being popped.
    - removes indices which are being popped, if applicable.
    E.g., (idx=[1,4,8,15], popping=[4,5,9]) --> (1,6,12), because:
        - the 1 is unchanged.
        - the 4 is popped, entirely.
        - the 8 is not popped, however 2 indices are popped below it, so it becomes a 6.
        - the 15 is not popped, however 3 are popped below, so it becomes a 12.
    '''
    result = []
    for i in idx:
        if i in popping:
            continue
        else:
            i = i - sum(i > pop for pop in popping)
            result.append(i)
    return result

class MaintainAttrs():
    '''context manager which ensures selected attrs of obj are restored upon exiting.'''
    def __init__(self, obj, attrs=[]):
        self.obj = obj
        self.attrs = attrs

    def __enter__(self):
        x = dict()
        for attr in self.attrs:
            if hasattr(self.obj, attr):
                x[attr] = getattr(self.obj, attr)
        self.original_attrs = x

    def __exit__(self, exc_type, exc_value, traceback):
        for attr, value in self.original_attrs.items():
            setattr(self.obj, attr, value)

class CustomNewDoesntInit(type):
    '''metaclass which changes behavior of class instantiation of inheritors.
    inheritors will only call __init__ automatically if their __new__ has not been overwritten.
        (i.e. if their __new__ is equal to object.__new__, then still call __init__.
        Otherwise, only call __init__ if told to do so.)
    use class MyClass(metaclass=CustomNewDoesntInit) to inherit this special power.
    '''
    def __call__(cls, *args, **kw):
        __tracebackhide__ = TRACEBACKHIDE
        if cls.__new__ == object.__new__:
            x = object.__new__(cls)
            x.__init__(*args, **kw)
        else:
            x = cls.__new__(cls, *args, **kw)
        return x

''' ----------------------------- Loading/Reloading Packages ----------------------------- '''

def generate_import_warning(modulename, category=Warning,
                            message='Failed to import {module}. Functions relying on {module} may fail.',
                            add_message=''):
    '''raises (via warnings.warn()) and returns a warning that modulename failed to import.
    The warning message will be be (' '.join(message, add_message)).format(module=modulename)

    Example Usage:
        try:
            import pandas as pd
        except:
            pd = generate_import_warning('pandas')
    '''
    failed_import_warning = category(message.format(module=modulename))
    warnings.warn(failed_import_warning)  # warn user that the module failed to import.
    return failed_import_warning

def find(x, element, default=None, equals=lambda v1, v2: v1==v2):
    '''find smallest index i for which equals(x[i], element);
    return None if no such i exists.
    '''
    try:
        return next(i for i, elem in enumerate(x) if equals(elem, element))
    except StopIteration:
        return default

def sort_by_priorities(x, prioritize=[], de_prioritize=[], equals=lambda v1, v2: v1==v2):
    '''returns list of elements of x, reordered acoording to priorities.
    puts p in prioritize first (in order of prioritize) for any p which appear in x.
    puts p in de_prioritize last (de_prioritize[-1] goes at very end) for any p which appear in x.
    If x is infinite, program will stall.

    The equals key can be used to provide a custom "equals" function.
    For example, to prioritize any elements of x containing 'MEFIRST', you could do:
        sort_by_priorities(x, ['MEFIRST'], equals=lambda sp, sx: sp in sx)
    (Note that the second arg passed to equals will be the element of x.)
    '''
    start  = []
    middle = []
    end    = []
    for y in x:
        i = find(prioritize, y, default=None, equals=equals)
        if i is not None:
            start  += [(y, i)]
        else:
            j = find(de_prioritize, y, default=None, equals=equals)
            if j is not None:
                end += [(y, j)]
            else:
                middle += [y]
    # sort start and end
    start = [start[i][0] for i in argsort(start, key=lambda y_i: y_i[1])]
    end   = [end[i][0]   for i in argsort(end,   key=lambda y_i: y_i[1])]
    # return result
    return start + middle + end

def reload(package='SymSolver', prioritize=['defaults', 'tools', 'polynomials', 'abstract_operations',
           'constants', 'scalars', 'vectors', 'linearizing', 'calculus', 'plane_waves'],
           de_prioritize=['expressions']):
    '''reloads all modules from package which have been imported.
    prioritize modules containing any strings in the list prioritize.
    de_prioritize modules containing any strings in the list de_prioritize.
    returns list of names of reloaded modules.
    '''
    reloaded = []
    to_reload = [(name, module) for (name, module) in sys.modules.items() if name.startswith(package)]
    to_reload = sort_by_priorities(to_reload, prioritize, de_prioritize,
                                   equals=lambda priority, name_and_module: priority in name_and_module[0])
    for (name, module) in to_reload:
        importlib.reload(module)
        reloaded += [name]
    return reloaded

''' ----------------------------- ProgressUpdatePrinter ----------------------------- '''

class EnoughTimePassed:
    '''class which tells whether enough time has passed since the start,
    or since the last time "check" was called.
    "Enough" is defined by the input parameter enough_time (in seconds).

    To always return False, set enough_time to a negative value.

    Example:
        t = EnoughTimePassed(enough_time=2)
        t.check()  # False  # 2 seconds have not yet passed.
        time.sleep(2.5)  # << wait 2.5 seconds. (Or, put code here which takes >= 2 seconds)
        t.check()  # True   # check returns True, then timer is reset,
        t.check()  # False  # so then check returns False, until 2 more seconds have passed.

    See also: ProgressUpdatePrinter
    '''

    def __init__(self, enough_time=2):
        self.enough_time = enough_time       # required minimum time between successful checks, in seconds 
        self._N_success  = 0                 # number of times checked successfully.
        self._attempt    = (enough_time >=0) # whether check should always just return False.
        
        self.time_start  = time.time()       # time when self was created.
        self.time_prev   = 0                 # time of the last successful check.  0 --> print at the first check.
        self.time_delta  = 0                 # time difference between last two successes.

    def check(self):
        '''checks whether enough time has passed since the start or the last successful check.'''
        if not self._attempt:
            return
        now = time.time()
        delta = self.time_since_prev()
        if delta > self.enough_time:
            self.time_prev  = now
            self.time_delta = delta
            self._N_success += 1
            return True
        else:
            return False

    def time_since_prev(self):
        '''returns time since the last successful check.'''
        return time.time() - self.time_prev

    def total_time_elapsed(self, as_string=False):
        '''returns total time elapsed [in seconds] since self was initialized.
        if as_string, get a string about it instead.
        '''
        t = time.time() - self.time_start
        if as_string:
            return 'Total time elapsed: {:.2f} seconds.'.format(t)
        else:
            return t


    time_elapsed = total_time_elapsed  # alias


class ProgressUpdatePrinter(EnoughTimePassed):
    '''class for printing messages but only when enough time has passed.
    "Enough" is defined by the input parameter print_freq (in seconds).

    To prevent all printing, set print_freq to a negative value.

    wait: bool (default False)
        whether to wait until print_freq time has passed before doing the first printout.

    Example:
        updater = ProgressUpdatePrinter(print_freq=2)
        updater.print('This will not be printed')   # not printed because 2 seconds have not passed yet.
        time.sleep(2.5)  # << wait 2.5 seconds. (Or, put code here which takes >= 2 seconds)
        updater.print('This WILL be printed!')      # prints, then timer is reset,
        updater.print('This will not be printed')   # so it won't print again until 2 more seconds have passed.
    '''
    def __init__(self, print_freq=None, clearline=True, clearN=100, enough_time=None, wait=False):
        '''(see class doc.) More notes:
        print_freq and enough_time are aliases for each other. exactly one must be non-None.
        '''
        print_freq = value_from_aliases(print_freq, enough_time)
        EnoughTimePassed.__init__(self, print_freq)
        self.clearline  = clearline          # whether to clear the line before printing.
        self.clearN     = clearN             # number of characters to clear from the line.
        self.print_freq = self.enough_time   # alias
        if wait:
            self.time_prev = time.time()

    def print_clear(self, N=None):
        '''calls print_clear() (defined in RunTools.tools)'''
        if N is None:
            N = self.clearN
        return print_clear(N=N)

    def print(self, *args_message, end='', print_time=False, **kw__print):
        '''prints message given by *args_message, if it is time to print.
        Also, clear the line first, if self.clearline.
        '''
        if not self._attempt:   # if printing is off; because print_freq < 0
            return              # then return, without even checking the timing.
        if self.check():
            self._printed_anything = True
            if self.clearline:
                self.print_clear()
            if print_time:
                args_message = list(args_message) + [self.total_time_elapsed(as_string=True)]
            print(*args_message, end=end, **kw__print)

    def printf(self, f=lambda: '', end='', **kw__print):
        '''prints message given by calling f() (which must return a single string).
        Also, clear the line first, if self.clearline.

        Since f will only be evaluated if/when print is called,
        printf offers the following advantages over print:
            - reduces amount of computation, if message requires computation. Example:
                message = 'total time passed = {}'
                with updater.print(       message.format(updater.total_time_elapsed()) ),
                    updater.total_time_elapsed() is calculated even if we aren't printing.
                with updater.printf( lambda: message.format(updater.total_time_elapsed()) ),
                    updater.total_time_elapsed() is calculated ONLY if we actually end up printing.
            - can use values which might be undefined unless printing is happening.
            - can use values which might change due to self.check().
        and the following disadvantage:
            - using printf successfully is a bit more complicated/challenging than using print.

        Example:
            updater = ProgressUpdatePrinter(print_freq=2)
            updater.printf(lambda: '{} seconds have passed since the last update'.format(updater.time_delta))
        '''
        message = f()
        self.print(message, end=end, **kw__print)

    def clear_update_info(self):
        '''does self.print_clear() if we printed at least one update, and self.clearline.
        otherwise, does nothing.
        '''
        if self.clearline and self._N_success:
            self.print_clear()

    def finalize(self, always=False, min_time=None, process_name=None, **kw__print):
        '''prints 'Completed process in 0.00 seconds!', filling in the process name and time elapsed as appropriate.
        Also, clear the line first, if self.clearline.

        always: False (default) or True
            False --> ignore this kwarg.
            True --> always print the finalize message.
        min_time: None (default) or number
            None --> ignore this kwarg.
            number --> print the finalize message if at least this many seconds have passed.
        process_name: None (default) or string
            None --> don't print any info about the process which completed.
            string --> include this name in the finalize message.
        '''
        time_elapsed = self.total_time_elapsed()
        printing = (always) \
                    or (min_time is not None and time_elapsed > min_time) \
                    or getattr(self, '_printed_anything', False)
        if self.print_freq >= 0 and printing:
            if self.clearline:
                self.print_clear()
            process_name = '' if process_name is None else process_name + ' '
            print('Completed {}in {:.2f} seconds!'.format(process_name, time_elapsed), **kw__print)


def value_from_aliases(*aliases):
    '''returns value from aliases.
    Precisely one of the aliases must be non-None, else raises ValueError.
    '''
    not_nones = [(a is not None) for a in aliases]
    Nvals     = sum(not_nones)
    if Nvals == 1:
        return aliases[next(i for (i, not_none) in enumerate(not_nones) if not_none)]
    else:
        raise ValueError('Expected one non-None value, but got {}!'.format(Nvals))


''' ----------------------------- Plotting ----------------------------- '''

def extent(xcoords, ycoords):
    '''returns extent (to go to imshow), given xcoords, ycoords. Assumes origin='lower'.
    Use this method to properly align extent with middle of pixels.
    (Noticeable when imshowing few enough pixels that individual pixels are visible.)
    
    xcoords and ycoords should be arrays.
    (This method uses their first & last values, and their lengths.)

    returns extent == np.array([left, right, bottom, top]).
    '''
    Nx     = len(xcoords)
    Ny     = len(ycoords)
    dx     = (xcoords[-1] - xcoords[0])/Nx
    dy     = (ycoords[-1] - ycoords[0])/Ny
    return np.array([*(xcoords[0] + np.array([0 - dx/2, dx * Nx + dx/2])),
                     *(ycoords[0] + np.array([0 - dy/2, dy * Ny + dy/2]))])

def get_symlog_ticks(coords, extent, N=5):
    '''returns labels for N ticks given coords.
    Always tries to include one tick for the smallest exponent, and one tick at min and max values.
    '''
    raise NotImplementedError

def evenly_spaced_idx(length, N):
    '''return N evenly spaced indices for a list of the given length.'''
    return np.round(np.linspace(0, length - 1, N)).astype(int)

def colors_from_cmap(cmap, N):
    '''get N evenly spaced colors from the colormap cmap. if cmap is a string, gets matplotlib's default.'''
    if isinstance(cmap, str):
        from matplotlib import cm
        cmap_actual = getattr(cm, cmap)
        return colors_from_cmap(cmap_actual, N)
    return cmap(np.linspace(0, 1, N))

def _colorbar_extent(under=None, over=None):
    '''returns appropriate value for 'extend' in colorbar(extend=..., ...)

    (under provided, over provided) --> value
        True,       True            --> 'both'
        True,       False           --> 'min'
        False,      True            --> 'max'
        False,      False           --> 'neither'
    '''
    lookup = {(True, True): 'both', (True, False): 'min', (False, True): 'max', (False, False): 'neither'}
    return lookup[(under is not None, over is not None)]

def _set_colorbar_extend(cmap):
    '''sets cmap.colorbar_extend appropriately.
    Destructive; cmap will be altered directly (as opposed to returning a new cmap).

    compares cmap(0.0) to cmap.get_under(), and cmap(1.0) to cmap.get_over().
        If unequal, makes triangle at that end.
    E.g. if cmap(0.0) != cmap.get_under(), but cmap(1.0) == cmap.get_over(),
        makes triangle at bottom but not top. extend='min'.

    returns cmap.
    '''
    under_was_set = True if ( not np.array_equal(cmap(0.0), cmap.get_under()) ) else None
    over_was_set  = True if ( not np.array_equal(cmap(1.0), cmap.get_over())  ) else None
    cmap.colorbar_extend = _colorbar_extent(under=under_was_set, over=over_was_set)

def with_colorbar_extend(cmap):
    '''returns a copy of cmap with colorbar_extend set appropriately, based on cmap's extremes.'''
    cmap_c = cmap.copy()
    _set_colorbar_extend(cmap_c)
    return cmap_c

def extended_cmap(cmap=None, under=None, over=None, bad=None, N=None):
    '''creates a cmap with the extremes provided.

    cmap: None, string, or colormap
        None --> use the matplotlib default colormap from rc params.
        string --> getattr(matplotlib.cm, cmap)
            E.g. 'viridis' --> matplotlib.cm.viridis.
        colormap --> use the colormap provided.
    under, over, bad: None, string, RGB, RGBA, or any other color which matplotlib can understand.
        under: color for points less than vmin.
            A triangle with this color will appear at bottom of colorbar, if under is set.
        over: color for points greater than vmax.
            A triangle with this color will appear at top of colorbar, if over is set.
        bad: color for bad points (NaNs).
            There is no representation of this color on the colorbar;
            be sure to tell viewers the meaning of this color if it appears on the plot.
    N: None or int
        resample cmap to this many points, if cmap is provided via string.
        (If cmap is a colormap object, ignore this kwarg.)

    returns: the resulting colormap.
    '''
    cmap0 = plt.get_cmap(cmap, N)
    cmap1 = cmap0.with_extremes(under=under, over=over, bad=bad)
    cmap2 = with_colorbar_extend(cmap1)
    return cmap2

cmap_extended = extended_cmap  # alias

def make_colorbar_axes(location='right', ticks_position=None, ax=None, pad=0.01, size=0.02):
    ''' Creates an axis appropriate for putting a colorbar.

    location: 'right' (default), 'left', 'top', or 'bottom'
        location of colorbar relative to image.
        Note: you will want to set orientation appropriately.
    ticks_position: None (default), 'right', 'left', 'top', or 'bottom'
        None -> ticks are on opposite side of colorbar from image.
        string -> use this value to set ticks position.
    ax: None or axes object
        None -> use plt.gca()
        this is the axes which will inform the size and position for cax.
        it is appropriate to use ax = axes for the image.
    pad: number (default 0.01)
        padding between cax and ax.
        TODO: what does the number really mean?
    size: number (default 0.02)
        size of colorbar.
        TODO: what does the number really mean?

    Adapted from https://stackoverflow.com/a/56900830.
    Returns cax.
    '''
    if ax is None:
        ax = plt.gca()
    p = ax.get_position()
    # calculate cax params.
    ## fig.add_axes(rect) has rect=[x, y, w, h],
    ## where x and y are location for lower left corner of axes.
    ## and w and h are width and height, respectively.
    assert location in ('right', 'left', 'top', 'bottom')
    if location in ('right', 'left'):
        y = p.y0
        h = p.height
        w = size
        if location == 'right':
            x = p.x1 + pad
        else: #'left'
            x = p.x0 - pad
    else: #'top' or 'bottom'
        x = p.x0
        w = p.width
        h = size
        if location == 'top':
            x = p.y1 + pad
        else: #'bottom'
            x = p.y1 - pad

    # make the axes
    cax = plt.gcf().add_axes([x, y, w, h])
    
    # Change ticks position
    if ticks_position is None:
        ticks_position = location
    if ticks_position in ('left', 'right'):
        cax.yaxis.set_ticks_position(ticks_position)
    else: #'top' or 'bottom'
        cax.xaxis.set_ticks_position(ticks_position)

    return cax

make_cax = make_colorbar_axis = make_colorbar_axes  # alias

class MaintainAxes():
    '''context manager which ensures original axes are restored upon exiting.'''
    def __init__(self):
        pass
    def __enter__(self):
        self.ax = plt.gca()
    def __exit__(self, exc_type, exc_value, traceback):
        plt.sca(self.ax)


''' ----------------------------- Arrays ----------------------------- '''

def iter_array(arr, skip_masked=True):
    '''yields (array element, multi_index of that element)
    multi_index of x is the N-tuple such that arr[multi_index] == x.

    skip_masked: bool
        if True, and arr is a masked array, skip all masked points.
    '''
    it = np.nditer(arr, flags=['multi_index', 'refs_ok'])
    if np.ma.isMaskedArray(arr) and skip_masked:
        for x in it:
            if arr.mask[it.multi_index]:
                yield (x[()], it.multi_index)
    else:
        for x in it:
            yield (x[()], it.multi_index)

itarrayte = iter_array   # alias

ObjArrayInfo = collections.namedtuple('ObjArrayInfo', ['type_of_item_0', 'shape'])
NumArrayInfo = collections.namedtuple('NumArrayInfo', ['min', 'mean', 'max', 'shape'])

def stats(arr):
    '''return dict with min, mean, max.'''
    return dict(min=np.nanmin(arr), mean=np.nanmean(arr), max=np.nanmax(arr))

def array_info(arr):
    '''returns namedtuple of min, mean, max, shape.'''
    x = np.asarray(arr)
    if x.ndim == 0  or  x.size == 0:
        return arr   # return original input; it is easy to display.
    elif x.dtype == 'object':
        return ObjArrayInfo(x.dtype, x.shape)
    else:
        return NumArrayInfo(x.min(), x.mean(), x.max(), x.shape)

def array_info_str(arr):
    '''returns string with info about arr's type, and array_info.
    if len(arr) gives a TypeError, just return repr(arr) instead.
    '''
    ai = array_info(arr)
    if ai is arr:
        return repr(arr)
    else:
        return '{}; {}'.format(type(arr), ai)

def equals(x, y):#, *more_args):
    '''check if x == y.

    But also is smart about numpy arrays;
        if (x==y) returns a numpy array,
        return (x==y).all(), insead of crashing.
    (Use numpy's testing.assert_equal function to handle this elegantly.)

    if more_args exist, also check that they are equal to each other, doing one pair at a time.
    E.g. equals(x,y,z) is like x == y == z.  (compares x and y, then y and z.)

    UPDATE: testing reveals that this function was SLOW.
        So now we only use numpy if x == y gives a ValueError.
        (This occurs during e.g. bool(x == y) if x or y contains a numpy array)
    '''
    #if len(more_args) > 0:
    #    return equals(x, y) and equals(y, more_args[0], *more_args[1:])
    if x is y:
        return True
    try:
        return bool(x == y)
    except ValueError:
        pass            # if we can't do bool(result), assume result is an array.
    # use np.testing.assert_equal.
    #   that func can make visible deprecation warning though, even when
    #   numpy arrays are not involved originally. We suppress that warning.
    with warnings.catch_warnings():
        warnings.filterwarnings('ignore', category=np.VisibleDeprecationWarning)
        try:
            np.testing.assert_equal(x, y)
        except AssertionError:
            return False
        else:
            return True

def list_equals(x, y):
    '''return whether lists x and y are equal.'''
    if len(x) != len(y):
        return False
    return all(equals(x[i], y[i]) for i in range(len(x)))

def dict_equals(x, y):
    '''return whether dicts x and y are equal.'''
    if x.keys() != y.keys():
        return False
    return all(equals(x[key], y[key]) for key in x.keys())

# TOOD: encapsulate this code better (avoid repetition between min and max).
def array_max(arrays, key=lambda arr: arr):
    '''returns an array containing max in each index according to key:
        result[idx] == max(key(array)[0][idx], ..., key(array)[n][idx])

    arrays: list or numpy array
        list --> a list of numpy arrays with the same shape
        numpy array --> treat the first dimension as the "list" dimension.
            e.g. shape (7, 3, 4) implies 7 arrays of shape (3, 4).
    '''
    arrays = np.asarray(np.broadcast_arrays(*arrays, subok=True))
    values = np.asarray([key(array) for array in arrays])
    argmax = np.nanargmax(values, axis=0)
    result = np.take_along_axis(arrays, np.expand_dims(argmax, 0), 0)    # shape here is (1, <shape of arrays[0]>)
    return result[0]

def array_min(arrays, key=lambda arr: arr):
    '''returns an array containing min in each index according to key:
        result[idx] == min(key(array)[0][idx], ..., key(array)[n][idx])

    arrays: list or numpy array
        list --> a list of numpy arrays with the same shape
        numpy array --> treat the first dimension as the "list" dimension.
            e.g. shape (7, 3, 4) implies 7 arrays of shape (3, 4).
    '''
    arrays = np.asarray(np.broadcast_arrays(*arrays, subok=True))
    values = np.asarray([key(array) for array in arrays])
    argmin = np.nanargmin(values, axis=0)
    result = np.take_along_axis(arrays, np.expand_dims(argmin, 0), 0)    # shape here is (1, <shape of arrays[0]>)
    return result[0]

def argmax(arr, **kw):
    '''unraveled argmax. **kw are passed to nanargmax.'''
    arr = np.asanyarray(arr)
    idx = np.nanargmax(arr, **kw)
    result = np.unravel_index(idx, arr.shape)
    return result

def argmin(arr, **kw):
    '''unraveled argmin. **kw are passed to nanargmin.'''
    arr = np.asanyarray(arr)
    idx = np.nanargmin(arr, **kw)
    result = np.unravel_index(idx, arr.shape)
    return result