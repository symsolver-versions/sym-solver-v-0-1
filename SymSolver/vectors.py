#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 14 2021

@author: Sevans

File purpose: vectors.

inherits mainly from: scalars.py

TODO:
    - DotProduct and CrossProduct should inherit from Product from abstract_operations.
        - abstract_operations should create Sum, Product, Power which contain no rules,
          but things can inherit from them for type-checking purposes.
"""

''' --------------------- Imports --------------------- '''

import warnings

from .abstract_operations import (
    equals, _equals0,
)
from . import scalars as sc
from .scalars import (
    view, _repr, _str, apply, _simplyfied,
    is_constant,
    get_summands, get_factors,
    _symbol_maker, _operation_collection,
    is_opinstance,
    _solubility_listing,
    SystemTooComplicatedError,
)
from . import tools
from .tools import (
    RENDER_MATH,
)

''' --------------------- Defaults --------------------- '''

DEFAULT_OPERATIONS = sc.DEFAULT_OPERATIONS + ['DotProduct', 'CrossProduct']
CACHING_PROPERTIES = sc.CACHING_PROPERTIES  # whether to cache simple properties

''' --------------------- Convenience Functions --------------------- '''

def is_vector(x, default=False, count_0=False):
    '''returns whether x is a vector.
    if count_0, if we are about to return False, if x == 0, return True instead.
    '''
    result = getattr(x, 'is_vector', default)
    if count_0:
        if bool(result)==False:
            if equals(x, 0):
                result = True
    return result

def is_unit_vector(x, default=False):
    '''returns whether x is a unit vector.'''
    return (is_vector(x) and 1==apply(x, 'magnitude', default=0))

def same_rank(x, y):
    '''returns whether x and y have the same rank.
    i.e. is_vector(x) == is_vector(y) or x==0 or y==0.
    '''
    return (is_vector(x) == is_vector(y)) or equals(x,0) or equals(y,0)

def scalar_vector_split(x):
    '''splits x into (scalar part, vector part).
    E.g. for scalars a,b; vectors u,v:
    u          --> (1, u)
    a u        --> (a, u)
    a (u + v)  --> (a, u + v)
    a u + b v  --> (1, a u + b v)
    a u + a v  --> (1, a u + a v)  # (this func isn't smart enough to factor anything.)
    a          --> (a, 1)
    '''
    ONE = sc.Product.IDENTITY
    if not is_opinstance(x, sc.Product):
        if is_vector(x):
            return (ONE, x)
        else:
            return (x, ONE)
    else:
        scalar_part = []
        vector_part = []
        for t in x:
            if is_vector(t):
                vector_part.append(t)
            else:
                scalar_part.append(t)
        assert len(vector_part) < 2, "Can't deal with multiplying vectors..."
        scalar_part = x._new(*scalar_part)
        vector_part = x._new(*vector_part)
        return (scalar_part, vector_part)

def is_basis_vector(x, default=False):
    '''returns whether x is a basis vector.'''
    return getattr(x, 'is_basis_vector', default)

def get_basis(x):
    '''returns basis given a basis vector x.
    returns None if x is not a basis vector.
    '''
    return getattr(x, 'basis', None)

def componentize(x, basis, targets=None, **kw):
    '''componentizes x in basis.'''
    return apply(x, 'componentize', basis, targets=targets, **kw)

def dof_from_list(x, Ndim):
    return sum(y.dof(Ndim) for y in x)

''' --------------------- Basic Objects --------------------- '''

OperationCollection = _operation_collection(locals(), DEFAULT_OPERATIONS)

class AbstractOperation(OperationCollection, sc.AbstractOperation):
    '''AbstractOperation which might be a vector'''
    HIERARCHY = 1

    @property
    def is_vector(self):
        raise NotImplementedError("property is_vector for class {}".format(type(self)))

    def dot(self, v):
        '''return dot product of self with v.'''
        DOTPRODUCT_FUNCTION = self.DotProduct
        return DOTPRODUCT_FUNCTION(self, v)

    def cross(self, v):
        '''return dot product of self with v.'''
        CROSSPRODUCT_FUNCTION = self.CrossProduct
        return CROSSPRODUCT_FUNCTION(self, v)

    def cross_format(self, u, u_first=True):
        '''converts self to u x w + a u + v form.
        Absorb scalars into w; distribute terms with u if necessary.
        E.g. n (u + B) x k --> u x (n k) + n B x k.
        asserts that self contains u, u is a vector, self is a vector, and u is in a cross product.
        '''
        assert is_vector(u)
        assert is_vector(self)
        assert self._contains_deep_inside_(u, CrossProduct), \
            "u is not inside a CrossProduct in self. u={}; self={}".format(u, self)
        self = self.apply('distribute', distribute_if=lambda s: apply(s, '_contains_deep_', u, default=False))
        self = _simplyfied(self)
        # << now, self should be a sum of terms containing u, some containing u in a cross product.
        return apply(self, '_cross_format', u, u_first=u_first)

    def _cross_format(self, u, *args, **kw):
        raise NotImplementedError("{}._cross_format()".format(type(self)))

    """
    def dot_format(self, u, u_first=True):
        '''converts self to u dot w + a u + v form.
        Absorb scalars into w; distribute terms with u if necessary.
        E.g. n (u + B) dot k --> u dot (n k) + n B dot k.
        asserts that self contains u, u is a vectors, and u is in a dot product.
        '''
        assert is_vector(u)
        assert self._contains_deep_inside_(u, DotProduct), \
            "u is not inside a DotProduct in self. u={}; self={}".format(u, self)
        self = self.apply('distribute', distribute_if=lambda s: apply(s, '_contains_deep_', u, default=False))
        self = _simplyfied(self)
        # << now, self should be a sum of terms containing u, some containing u in a dot product.
        return apply(self, '_dot_format', u, u_first=u_first)

    def _dot_format(self, u, *args, **kw):
        raise NotImplementedError("{}._dot_format()".format(type(self)))
    """

    def get_vector_symbols(self):
        '''returns the list of symbols which are vectors.'''
        return self.get_symbols_with(lambda s: is_vector(s))

    def componentize(self, basis, targets=None, keep_hats=False, zeros=[], **kw):
        '''substitutes vectors with their components in the given basis.
        targets: None or list
            None --> substitute all vectors (only the vector symbols in self).
            list --> only substitute the vectors listed here.
        keep_hats: False or True
            False --> DO apply('hats_id')  ( E.g., converts (u dot xhat) --> (u_x) )
            True --> DON'T convert dot product with hat to a subscript.
        zeros: list
            any basis vectors listed here will give zero contribution.
            E.g. u.componentize((xhat, yhat, zhat), zeros=[yhat]) --> u_x xhat + u_z zhat
        '''
        if targets is None:
            targets = self.get_symbols_with(lambda s: is_vector(s) and s not in basis)
        if len(targets) == 0:
            return self   # return self, exactly, to help indicate nothing was changed.
        subs = [( t, self.Sum(*(t.dot(b)*b for b in basis if b not in zeros)) ) for t in targets if is_vector(t)]
        result = self.subs(*subs, **kw)
        if not keep_hats:
            result = result.apply('hats_id')
        return result

    def hatify(self, targets=None):
        '''substitutes vector symbols with their magnitude times their direction.
        (Note that only vector symbols will be affected, not vector sums.
            E.g. kvec + Bvec --> k * khat + B * Bhat)

        targets: None or list
            None --> substitute all vectors (only the vector symbols in self).
            list --> only substitute the vectors listed here.
        '''
        if targets is None:
            targets = self.get_vector_symbols()
        if len(targets) == 0:
            return self   # return self, exactly, to help indicate nothing was changed.
        subs = [ (t, t.hatify()) for t in targets if is_vector(t) ]
        result = self.subs(*subs)
        return result

    def dof(self, Ndim):
        '''number of components of self.'''
        return Ndim if is_vector(self) else 1


class Symbol(sc.Symbol, AbstractOperation):
    '''Symbol which might be a vector'''
    def __init__(self, s, *args, vector=False, hat=False, **kwargs):
        super().__init__(s, *args, **kwargs)
        self.hat = hat
        if self.hat:
            self.vector = True
        else:
            self.vector = vector

    def _init_properties(self):
        '''returns dict for initializing another symbol like self.'''
        kw = super()._init_properties()
        kw['hat']    = self.hat
        kw['vector'] = self.is_vector
        return kw

    @property
    def is_vector(self):
        '''tells whether I am a vector'''
        return self.vector or self.hat

    @is_vector.setter
    def is_vector(self, value):
        self.vector = value
        if self.hat and not value:
            self.hat = value

    def magnitude(self):
        '''returns self for non-vectors, or self._new(..., vector=False) for vectors.'''
        if self.hat:
            return 1
        elif self.vector:
            return self._new(vector=False)
        else:
            return self   # return self, exactly, to help indicate nothing was changed.

    mag = property(lambda self: self.magnitude())  # alias

    def direction(self):
        '''returns self._new(..., hat=True) for vectors, or raises TypeError for non-vectors.'''
        if self.hat:
            return self
        elif self.vector:
            return self._new(hat=True)
        else:
            raise TypeError('Scalar quantity has no direction! (Quantity={})'.format(self))

    dir = property(lambda self: self.direction())  # alias

    def hatify(self):
        '''returns self.magnitude() * self.direction()'''
        direction = self.dir  # put direction first in case of TypeError.
        magnitude = self.mag
        return magnitude * direction

    def component(self, x):
        '''returns x component of self. (vec(k) --> k_x)'''
        assert self.is_vector, "Can't get component of scalar: {}".format(self)
        if not self.hat:
            if isinstance(x, Symbol):   # if x is an instance of vectors.py Symbol
                x = x._new(vector=False, hat=False)
            old_subscripts = self.subscripts
            return self._new(vector=False, subscripts=old_subscripts + [x])
        else:  # self is a unit vector. x component of khat --> k_x / k   (not k_x).
            kvec = self._new(vector=True, hat=False)
            kmag = kvec.magnitude()
            k_x  = kvec.component(x)
            return k_x / kmag

    def __str__(self, internal=False, enc='()', **kw):
        '''internal tells type of enclosing function.'''
        with tools.MaintainAttrs(self, ['s']):  # restore self.s afterwards
            if self.hat:
                self.s = r'\hat{{{}}}'.format(self.s)           # pretend self.s = '\hat{s}'
            elif self.is_vector:
                self.s = r'\vec{{{}}}'.format(self.s)           # pretend self.s = '\vec{s}'
            result = _str(super(), internal=internal, enc=enc, **kw)  # get __str__ from super()
        return result

    def _repr_contents(self):
        '''returns contents to put inside 'Symbol()' in repr for self.'''
        contents = super()._repr_contents()
        if self.hat:
            contents.append('hat={}'.format(self.hat))
        elif self.vector:
            contents.append('vector={}'.format(self.vector))
        return contents

    def __eq__(self, b):
        '''return self == b'''
        if b is self:
            return True
        if not super().__eq__(b):
            return False
        if is_vector(self) != is_vector(b):
            return False
        if self.hat != getattr(b, 'hat', None):
            return False
        return True

    def num_subs(self):
        '''returns (subables, unsubables).'''
        if self.is_vector:
            return ([], [self])
        else:
            return ([self], [])


Symbols = _symbol_maker(Symbol)


class MultipleOperation(AbstractOperation, sc.MultipleOperation):
    @property
    def is_vector(self):
        '''returns whether self is a vector by checking all terms inside self.
        Subclasses with more complicated vectorness should overwrite this method.
        '''
        # caching
        if CACHING_PROPERTIES:
            if hasattr(self, '_is_vector'):
                return self._is_vector   # cached value
        # result
        result = any(is_vector(t) for t in self)
        # caching
        if CACHING_PROPERTIES:
            self._is_vector = result
        # result
        return result


class BinaryOperation(sc.BinaryOperation, MultipleOperation):
    pass


# define Sum, Product, Power, Equation in this module.
sc.define_operations(globals(), vars(sc),
                     subclasses=[MultipleOperation], operations=sc.DEFAULT_OPERATIONS)

''' --------------------- Basis --------------------- '''

class Basis(AbstractOperation):
    '''ordered list of basis vectors.'''
    __new__ = object.__new__   # overwrite __new__ from AbstractOperation

    def __init__(self, *basis_vectors, **kw__None):
        self.basis_vectors = basis_vectors
        assert all(is_vector(v) for v in basis_vectors)   # all must be vectors..
        for i, vector in enumerate(basis_vectors):
            self._basis_assign(vector, i)

    def _basis_assign(self, v, i=None):
        '''tells v that it is a basis vector, with basis self.'''
        setattr(v, 'is_basis_vector', True)
        setattr(v, 'basis', self)
        setattr(v, '_basis_index_of_self', i)

    def basis_unassign(self, v):
        '''tells v that it is not a basis vector anymore.'''
        assert v.basis == self, "can't unassign from a different basis!"
        setattr(v, 'is_basis_vector', False)
        if hasattr(v, 'basis'):
            delattr(v, 'basis')

    def _repr_contents(self, **kw):
        '''returns contents to put inside 'Basis' in repr for self.'''
        contents = [_repr(v, **kw) for v in self.basis_vectors]
        return contents

    def __str__(self, **kw):
        return '(' + ', '.join(_str(v, **kw) for v in self) + ')'

    def __iter__(self):
        return iter(self.basis_vectors)

    def __len__(self):
        return len(self.basis_vectors)

    def __getitem__(self, i):
        return self.basis_vectors[i]

    def index(self, u):
        '''tells the index for u in self, or None if u is not in self.'''
        try:
            return next(i for i, x in enumerate(self.basis_vectors) if u==x)
        except StopIteration:
            return None


class OrthonormalBasis(Basis):
    '''ordered list of orthonormal basis vectors.
    Knows how to make cross products simpler.
    '''
    def __init__(self, *basis_vectors, **kw):
        assert all(apply(v, 'magnitude', default=0) == 1 for v in basis_vectors)
        Basis.__init__(self, *basis_vectors, **kw)

    def _basis_assign(self, u, i=None):
        '''tells u that it is a basis vector, with basis self.
        Also tells u how to simplify dot products with other basis vectors.
        '''
        Basis._basis_assign(self, u, i=i)
        def _dot_simplify_u(v, _return_none_if_no_simplify=False):
            '''if v is in u.basis, evaluate u dot v.
            Otherwise, return DotProduct(u, v)
            '''
            j = u.basis.index(v)
            i = u._basis_index_of_self
            if j is None:
                if _return_none_if_no_simplify:
                    return None
                else:
                    return self.DotProduct(u, v)
            else:
                return u.basis._dot_simplify(i, j)
        setattr(u, '_dot_simplify', _dot_simplify_u)

    def _dot_simplify(self, i, j):
        '''evaluate dot product between self.basis_vectors[i] and self.basis_vectors[j]'''
        if i == j:
            return 1
        else:
            return 0


class OrthonormalBasis3D(OrthonormalBasis):
    '''ordered list of 3 orthonormal basis vectors.'''
    def __init__(self, x, y, z, **kw):
        OrthonormalBasis.__init__(self, x, y, z, **kw)

    def _basis_assign(self, u, i=None):
        '''tells u that it is a basis vector, with basis self.
        Also tells u how to simplify cross products with other basis vectors.
        '''
        OrthonormalBasis._basis_assign(self, u, i=i)
        def _cross_simplify_u(v, _return_none_if_no_simplify=False):
            '''if v is in u.basis, evaluate u cross v.
            Otherwise, return CrossProduct(u, v)
            '''
            j = u.basis.index(v)
            i = u._basis_index_of_self
            if j is None:
                if _return_none_if_no_simplify:
                    return None
                else:
                    return self.CrossProduct(u, v)
            else:
                return u.basis._cross_simplify(i, j)
        setattr(u, '_cross_simplify', _cross_simplify_u)
    
    x = property(lambda self: self.basis_vectors[0])
    y = property(lambda self: self.basis_vectors[1])
    z = property(lambda self: self.basis_vectors[2])

    def _cross_simplify(self, i, j):
        '''evaluate cross product between self.basis_vectors[i] and self.basis_vectors[j]'''
        if i == j:
            return 0
        elif i == 0:
            if j == 1:   # x cross y
                return self.z
            else:        # x cross z
                return -1 * self.y
        elif i == 1:
            if j == 2:   # y cross z
                return self.x
            else:        # y cross x
                return -1 * self.z
        else:
            if j == 0:   # z cross x
                return self.y
            else:        # z cross y
                return -1 * self.x


''' --------------------- Operations - Sum, Product, Power --------------------- '''

class Sum(sc.Sum, MultipleOperation):
    def __init__(self, *terms, check_rank=True, **kw):
        '''initialize Sum.
        Enforce that all terms have the same rank, unless check_rank=False.
        '''
        if check_rank:
            vectoriality = None
            for t in terms:
                if equals(t, 0):
                    continue   # 0 could be a vector or a scalar.
                elif vectoriality is None:
                    vectoriality = is_vector(t)
                    continue
                elif is_vector(t) != vectoriality:
                    rank_mismatch_errmsg = "All terms must have the same rank. " +  \
                                           "But terms {} and {} did not.".format(terms[0], t)
                    raise TypeError(rank_mismatch_errmsg)
        super().__init__(*terms, **kw)

    def _cross_format(self, u, u_first=True):
        '''convert self into something like: u x w + v'''
        # sort terms into those containing u in a CrossProduct, and everything else.
        non_cross_u = self.IDENTITY
        cross_u = []
        for t in self:
            if not t._contains_deep_inside_(u, CrossProduct):
                non_cross_u += t
            else:
                cross_u.append(t._cross_format(u, u_first=u_first))
        # put the cross_u terms together.
        if not is_opinstance(cross_u, CrossProduct):
            # then we assume cross_u is a Sum, and combine terms (e.g. u x w + u x y --> u x (w + y))
            crossed_non_u = self.IDENTITY
            for t in cross_u:
                assert is_opinstance(t, CrossProduct) and equals(t[0 if u_first else 1], u)
                crossed_non_u += t[1 if u_first else 0]
            cross_terms = (u, crossed_non_u) if u_first else (crossed_non_u, u)
            cross_u = self.CrossProduct(*cross_terms)
        # put everything together into u x w + v form.
        return cross_u + non_cross_u

    """
    def _dot_format(self, u, u_first=True):
        '''convert self into something like: (u dot w) B + v'''
        # sort terms into those containing u in a DotProduct, and everything else.
        non_dot_u = self.IDENTITY
        dot_u = []
        for t in self:
            if not t._contains_deep_inside_(u, DotProduct):
                non_dot_u += t
            else:
                dot_u.append(t._dot_format(u, u_first=u_first))
        # put the dot_u terms together.
        if not is_opinstance(dot_u, DotProduct):
            # then we assume dot_u is a Sum, and combine terms (e.g. u dot w + u dot y --> u dot (w + y))
            dotted_non_u = self.IDENTITY
            for t in dot_u:
                assert is_opinstance(t, DotProduct) and equals(t[0 if u_first else 1], u)
                dotted_non_u += t[1]
            dot_terms = (u, dotted_non_u) if u_first else (dotted_non_u, u)
            dot_u = self.DotProduct(*dot_terms)
        # put everything together into u dot w + v form.
        return dot_u + non_dot_u
    """

    ## SIMPLIFYING ##
    def COLLECTION_PRIORITIZER(self, x):
        '''returns collection priority number for x.
        highest means "collect first".
        if x is None, return the maximum possible value.

        Here we say to prioritize collection of:
            cross products > dot products > vectors > (the default priority order from scalars.py)
        '''
        CHILD_MAX = sc.Sum.MAXIMUM_COLLECTION_PRIORITY
        if is_opinstance(x, CrossProduct):
            return CHILD_MAX + 30
        elif is_opinstance(x, DotProduct):
            return CHILD_MAX + 20
        #elif is_vector(x):
        #    return CHILD_MAX + 10
        else:
            return sc.Sum.COLLECTION_PRIORITIZER(self, x)

    MAXIMUM_COLLECTION_PRIORITY = sc.Sum.MAXIMUM_COLLECTION_PRIORITY + 30


class Product(sc.Product, MultipleOperation):
    def __init__(self, *terms, **kw):
        '''initialize product.
        if two or more terms are vectors, raise NotImplementedError.
        '''
        n_vectors = 0
        for t in terms:
            if is_vector(t):
                n_vectors += 1
                if n_vectors > 1:
                    vector_product_errmsg = "Product of two or more vectors. " +  \
                                            "Did you mean DotProduct or CrossProduct?"
                    raise NotImplementedError(vector_product_errmsg)
        super().__init__(*terms, **kw)

    def _str_terms_order(self):
        '''returns lists of terms of self in order for str representation of self.
        returns:
            list of non-AbstractOperations (e.g. numbers),
            list of constants,
            list of AbstractOperation objects,
            list of unit vectors
        '''
        numbers, constants, terms = super()._str_terms_order()
        result = [numbers, [], [], []]   # 0 - numbers, 1 - constants, 2 - terms, 3 - unit vectors
        for constant in constants:
            if is_unit_vector(constant):
                result[3].append(constant)
            else:
                result[1].append(constant)
        for term in terms:
            if is_unit_vector(term):
                result[3].append(term)
            else:
                result[2].append(term)
        return result

    def _cross_format(self, u, u_first=True):
        '''convert self from n * (u x w) * m --> u x (n * w * m).'''
        non_u = self.IDENTITY
        u_was_first = None
        for t in self:
            if is_opinstance(t, CrossProduct):
                if equals(t[0], u):      # u is first in t
                    u_was_first = True
                    w = t[1]
                elif equals(t[1], u):    # u is second in t
                    u_was_first = False
                    w = t[0]
                else:
                    raise ValueError("received CrossProduct without u in Product._cross_format for u={},\n{}".format(u, self))
                non_u *= w
            else:
                non_u *= t
        if u_was_first is None:
            raise ValueError("received Product without CrossProduct in Product._cross_format for:\n{}".format(self))
        cross_terms = (u, non_u) if u_was_first else (non_u, u)
        result = self.CrossProduct(*cross_terms)
        return result._cross_format(u, u_first=u_first)

    """
    def _dot_format(self, u, u_first=True):
        '''convert self from v n * (u dot w) * m --> u dot (n * w * m) v'''
        non_u  = self.IDENTITY
        scalar, vector = scalar_vector_split(self)
        u_was_first = None
        for t in scalar:
            if is_opinstance(t, DotProduct):
                if equals(t[0], u):      # u is first in t
                    u_was_first = True
                    w = t[1]
                elif equals(t[1], u):    # u is second in t
                    u_was_first = False
                    w = t[0]
                else:
                    raise ValueError("received DotProduct without u in Product._dot_format for u={},\n{}".format(u, self))
                non_u *= w
            else:
                non_u *= t
        if u_was_first is None:
            raise ValueError("received Product without DotProduct in Product._dot_format for:\n{}".format(self))
        dot_terms = (u, non_u) if u_was_first else (non_u, u)
        scalar = self.DotProduct(*dot_terms)
        scalar = scalar._dot_format(u, u_first=u_first)
        return scalar * vector
    """

    ## SOLVING ##
    @_solubility_listing
    def solubility_list(self, x):
        '''returns the list of solubilities for x in self.
        Note: We can't solve for x (scalar) if it is being multiplied by a vector.
        '''
        if (not is_vector(x)) and (is_vector(self)) and (x in self):
            scalar, vector = scalar_vector_split(self)
            return [x * vector]
        else:
            return sc.Product.solubility_list(self, x)


class Power(sc.Power, BinaryOperation):
    def __init__(self, t1, t2, **kw):
        '''initialize Power.
        raise NotImplementedError in the following cases:
            (vector)^(any power other than 1)
            (any number other than 0)^(vector)
        '''
        if is_vector(t1):
            if not equals(t2, 1):
                raise NotImplementedError("vector ** value. Did you mean DotProduct or CrossProduct?")
        if is_vector(t2):
            if not equals(t1, 0):
                raise NotImplementedError("value ** vector.")
        super().__init__(t1, t2, **kw)


''' --------------------- LinearOperation, MultiSum --------------------- '''

class LinearOperation(sc.LinearOperation, MultipleOperation):
    # we need to overwrite distribute from scalars so that f(cx) --> c f(x) is NOT allowed for vectors.
    def _distribute(self, **kw__None):
        '''Implements distribute rules for linear operations:
            f(cx) --> c f(x), when f treats c as constant, and c is not a vector.
            f(x) + f(y) --> f(x + y), for any x, y.
        '''
        # f(x) + f(y) --> f(x + y)
        summands = get_summands(self.x)
        if len(summands) > 1:
            fterms = [self._new(t)._distribute(**kw__None) for t in summands]
            return self.Sum(*fterms)
        # f(cx) --> c f(x)
        x_scalar, x_vector = scalar_vector_split(self.x)
        factors = get_factors(x_scalar)
        cprod = Product.IDENTITY  # hold the constant, scalar product which is pulled out.
        xprod = Product.IDENTITY  # hold the non-constant, scalar product which remains inside.
        distributed_any = False
        for term in factors:
            if term is Product.IDENTITY:
                continue    # e.g. if we have f(1), don't turn it into 1 * f(1).
            elif self.treats_as_constant(term):
                cprod = cprod * term
                distributed_any = True
            else:
                xprod = xprod * term
        if distributed_any:
            fx = self._new(xprod * x_vector)._distribute(**kw__None)
            return cprod * fx
        # <- if we reached this line, we distributed nothing ->
        return self   # return self, exactly, to help indicate nothing was changed.


class MultiSum(sc.MultiSum, LinearOperation):
    pass


''' --------------------- Operations - DotProduct, CrossProduct --------------------- '''

class BinaryVectorProduct(BinaryOperation):
    '''Binary Product where both terms must be vectors.
    Also puts self._PRODUCT_STRING inbetween terms when doing str(self).
    '''
    OPERATION = None   # stops from trying evaluate_numbers(). (TODO: use np.dot()?)
    @property
    def _PRODUCT_STRING(self):  # tells string to put between terms during str(self)
        raise NotImplementedError('property _PRODUCT_STRING for class {}'.format(type(self)))
    @property
    def _SHORTHAND(self):  # function name (attr of self) for doing this product. (e.g. 'dot' or 'cross')
        raise NotImplementedError('property _SHORTHAND for class {}'.format(type(self)))

    def __init__(self, t1, t2, *args, **kw):
        '''initialize, enforcing that t1 and t2 are both vectors (else raise TypeError).'''
        if not (is_vector(t1, count_0=True) and is_vector(t2, count_0=True)):
            raise TypeError("{} requires two vectors, but got {} and {}".format(type(self), t1, t2))
        super().__init__(t1, t2, *args, **kw)

    def __str__(self, internal=False, enc='()', **kw):
        '''string of self. Put in enc if internal to anything other than Sum.'''
        if internal and not issubclass(internal, sc.Sum):
            return r'\left{} {} \right{}'.format(enc[0], _str(self, internal=False, **kw), enc[1])
        else:
            t1str, t2str = [_str(t, internal=type(self), **kw) for t in self]
            return r'{} {} {}'.format(t1str, self._PRODUCT_STRING, t2str)

    def _equals0(self):
        '''return whether self == 0.'''
        return any(_equals0(t) for t in self)

    ## ORDERING ##
    @property
    def _unique_ordering(self):
        '''return function f(x, y) which:
            returns (not (x > y)) in the sense of some unique ordering scheme.
            (i.e. True when the unique order is (x, y); False when the unique order is (y, x))
            The scheme must satisfy:
                (converse)      if x > y, then (not y > x)
                (transitivity)  if y >= z, and z > x, then y > x.
        Right now the scheme we are using is:
            order is determined by the ordering for the string representations of self and x.
        '''
        def uniquely_ordered(x, y):
            return not (_repr(x) > _repr(y))
        return uniquely_ordered

    @property
    def unique_ordering(self):
        '''return function f(*args) which:
            uses the _unique_ordering property to return the indices required to order args.
            E.g. if the unique ordering for x, y, z is x, y, z,
                then unique_ordering(z, x, y) --> (1, 2, 0),
                since [(z, x, y)[i] for i in (1, 2, 0)] == [x, y, z]
            if indices = False, return the uniquely ordered tuple, instead.
        '''
        def uniquely_ordered_indices(*args, indices=True):
            result = []
            ordered = []
            for j, y in enumerate(args):
                for i, x in enumerate(ordered):
                    if self._unique_ordering(y, x):
                        ordered.insert(i, y)
                        result.insert(i, j)
                        break
                else: # didn't break, i.e. didn't insert y yet.
                    ordered.append(y)
                    result.append(j)
            returning = result if indices else ordered
            return tuple(returning)
        return uniquely_ordered_indices

    ## SIMPLIFYING ##
    _SIMPLIFY_OPS = ['_vector_product_basis_id']

    def _vector_product_basis_id(self, hats_id=False, **kw):
        '''expands self only if self[0] and self[1] both contain basis vectors.
        Then apply _basis_id where applicable.
        '''
        sym0 = apply(self[0], 'get_symbols_in', is_vector, default=[])
        if len(sym0)==0: return self
        sym1 = apply(self[1], 'get_symbols_in', is_vector, default=[])
        if len(sym1)==0: return self
        if any(is_basis_vector(s) for s in sym0) and any(is_basis_vector(s) for s in sym1):
            self = self._distribute(**kw)
            self = apply(self, '_basis_id', **kw)
        return self

    ## EXPANDING ##
    _EXPAND_OPS = ['_distribute']

    def _distribute(self, **kw__None):
        '''distributes at top layer of self.
        With '*' as the product (e.g. dot or cross)
        x '*' (y + z) --> (x '*' y + x '*' z).
        vec(x) '*' (b vec(y)) --> b vec(x) '*' vec(y)
        '''
        if self == 0:
            return 0
        dot = self._SHORTHAND  # 'dot' or 'cross'  (or something else I suppose...)
        SumType = sc.Sum
        u, v = self.t1, self.t2    # self == u dot v
        if not any(isinstance(s, SumType) for s in self):
            # vec(x) dot (b vec(y)) --> b vec(x) dot vec(y)
            scalars = []
            vectors = []
            for s in [u, v]:
                if is_opinstance(s, sc.Product):
                    for t in s:
                        if is_vector(t):
                            vectors.append(t)
                        else:
                            scalars.append(t)
                else:
                    vectors.append(s)
            if len(scalars) == 0:
                return self   # nothing to distribute. Return self to be clear that we made no changes.
            else:
                assert len(vectors) == 2, "expecting two vectors at this point..."
                return self.Product(*scalars, apply(vectors[0], dot, vectors[1])) 
        if not isinstance(u, SumType):
            u = [u]
        if not isinstance(v, SumType):
            v = [v]
        result = SumType.IDENTITY
        for u_term in u:
            for v_term in v:
                result += apply(u_term, dot, v_term)
        return result

    ## SOLVING ##
    @_solubility_listing
    def solubility_list(self, x):
        '''returns the list of solubilities for self.'''
        x_in_0 = apply(self[0], '_contains_deep_', x, default=False)
        x_in_1 = apply(self[1], '_contains_deep_', x, default=False)
        if x_in_0 and x_in_1:  # x in multiple terms of self. Haven't implemented how to treat this.
            return [self]
        x_in  = self[0] if x_in_0 else self[1]  # term containing x
        solub = x_in.solubility_list(x)         # x solubility in x_in

        prod_maker = (lambda t: self._new(t, self[1])) if x_in_0 else (lambda t: self._new(self[0], t))
        result = []
        for t in solub:
            if is_vector(t) and apply(t, '_contains_deep_', x):
                result.append(prod_maker(t))
            else:
                result.append(t)
        return result


class DotProduct(BinaryVectorProduct):
    OPTYPE = 'DotProduct'

    @property
    def _PRODUCT_STRING(self): return r'\cdot'
    @property
    def _SHORTHAND(self): return 'dot'
    @property
    def is_vector(self): return False

    """
    def _dot_format(self, u, u_first=True):
        '''formats self to look like u dot w. Or w dot u, if u_first=False.'''
        if equals(self[0], u):
            return self if (  u_first  ) else self._new(self[1], u)
        elif equals(self[1], u):
            return self if (not u_first) else self._new(u, self[0])
        else:
            raise ValueError("received DotProduct without u in DotProduct._dot_format for u={},\n{}".format(u, self))
    """

    ## COMPARISONS ##
    def __eq__(self, x):
        if x is self:
            return True
        if _equals0(x):
            return self._equals0()
        elif is_opinstance(x, type(self)):
            if equals(self[0], x[0]) and equals(self[1], x[1]):
                return True
            if equals(self[1], x[0]) and equals(self[0], x[1]):
                return True
        return False

    ## SUBSTITUTIONS ##
    def num_subs(self):
        '''returns (subables, unsubables).'''
        return ([self], [])

    ## SIMPLIFYING ##
    _SIMPLIFY_OPS = ['_simplify_id', '_magnitude_id', '_hats_id', '_vector_id', '_basis_id']
    _DEFAULT_FALSE_OPS = ['_magnitude_id', '_hats_id']

    def _simplify_id(self, magnitude=False, hats=False, **kw__None):
        '''converts 0 dot x --> 0; x dot (x cross y) --> 0.
        if magnitude, converts x dot x --> |x|**2 (for symbol x)
        if hats, converts k dot xhat --> k_x   (puts x in subscripts of k)'''
        # simplify 0's
        if any(equals(t, 0) for t in self):
            return 0
        # simplify x dot (x cross y)
        crossed = [is_opinstance(t, self.CrossProduct) for t in self]
        if crossed[0]:
            if not crossed[1]:
                if self[1] in self[0]:   # self[0] in [t1, t2] of self[1]
                    return 0
        elif crossed[1]:  # and not crossed[0]
            if self[0] in self[1]:       # self[1] in [t1, t2] of self[0]
                return 0
        return self    # return self, exactly, to help indicate no changes were made.

    def _magnitude_id(self, **kw__None):
        '''converts x dot x --> |x|**2 (for symbol x)'''
        if isinstance(self.t1, Symbol) and equals(self.t1, self.t2):
            return self.t1.magnitude() ** 2
        return self    # return self, exactly, to help indicate no changes were made.

    def _hats_id(self, **kw__None):
        '''converts k dot xhat --> k_x   (puts x in subscripts of k),
        for k a vector (possibly a unit vector) and x part of an OrthonormalBasis.
        (Finds k and x throughout self, when applicable.)
        '''
        t1, t2 = self.t1, self.t2
        if isinstance(t1, Symbol) and isinstance(t2, Symbol):
            t1basis = is_basis_vector(t1) and isinstance(t1.basis, OrthonormalBasis)
            t2basis = is_basis_vector(t2) and isinstance(t2.basis, OrthonormalBasis)
            if bool(t1basis) != bool(t2basis):
                if t1basis:
                    return t2.component(t1)
                else:
                    return t1.component(t2)
        return self    # return self, exactly, to help indicate no changes were made.

    def _vector_id(self, unique_ordering=True, **kw__None):
        '''converts A dot (B cross C) to its unique ordering
            (determined by self._unique_ordering of A, B, C)
        A.(BxC) == C.(AxB) == B.(CxA) == -A.(CxB) == -B.(AxC) == -C.(BxA)

        If the cross product is in the first term (AxB).C,
            do the ordering for C.(AxB) then return a result with cross product in the first term.
            E.g. (AxB).C --> _vector_id(C.(AxB)) == A.(BxC) --> (BxC).A,  assuming C > B > A.

        if unique_ordering=False, or self[1] and self[0] are both not cross products,
            return self without making any changes.
        '''
        if unique_ordering:
            cross = None
            if is_opinstance(self[1], self.CrossProduct):   # A.(BxC)
                cross = 1
                vecs = self[0], self[1][0], self[1][1]   # A, B, C
            elif is_opinstance(self[0], self.CrossProduct): # (AxB).C
                cross = 0
                vecs = self[1], self[0][0], self[0][1]   # C, A, B
            if cross is not None:
                idx  = self.unique_ordering(*vecs)    # idx for the ordered result.
                A, B, C = [vecs[i] for i in idx]
                if idx == (0,1,2):
                    return self   # return self, exactly, to indicate no changes were made.
                sign = +1 if (idx in [(0,1,2), (2,0,1), (1,2,0)]) else -1
                if cross == 1:
                    return sign * (A).dot(B.cross(C))
                else:# cross == 0:
                    return sign * (B.cross(C)).dot(A)
        return self

    def _basis_id(self, **kw__None):
        '''simplifies dot products between basis vectors.'''
        a0, v0 = scalar_vector_split(self[0])
        if not is_basis_vector(v0):
            return self
        a1, v1 = scalar_vector_split(self[1])
        if not is_basis_vector(v1):
            return self
        v0_dot_v1 = v0._dot_simplify(v1, _return_none_if_no_simplify=True)
        if v0_dot_v1 is None:
            return self
        result = a0 * a1 * v0_dot_v1
        return result


class CrossProduct(BinaryVectorProduct):
    OPTYPE = 'CrossProduct'
    
    @property
    def _PRODUCT_STRING(self): return r'\times'
    @property
    def _SHORTHAND(self): return 'cross'
    @property
    def is_vector(self): return True

    def _cross_format(self, u, u_first=True):
        '''formats self to look like u x w. Or w x u, if u_first=False.'''
        if equals(self[0], u):
            return self if (  u_first  ) else self._new(-1 * self[1], u)
        elif equals(self[1], u):
            return self if (not u_first) else self._new(u, -1 * self[0])
        else:
            raise ValueError("received CrossProduct without u in CrossProduct._cross_format for u={},\n{}".format(u, self))

    ## COMPARISONS ##
    def __eq__(self, x):
        # TODO: handle something like uxB compared with -1 * Bxu. (right now we say these are not equal)
        if x is self:
            return True
        if _equals0(x):
            return self._equals0()
        elif is_opinstance(x, type(self)):
            if equals(self[0], x[0]) and equals(self[1], x[1]):
                return True
        return False

    ## SUBSTITUTIONS ##
    def num_subs(self):
        '''returns (subables, unsubables).'''
        return ([], [self])

    ## SIMPLIFYING ##
    _SIMPLIFY_OPS = ['_simplify_id', '_vector_id', '_basis_id']

    def _simplify_id(self, unique_ordering=False, **kw__None):
        '''converts 0 cross x --> 0;  x cross x --> 0.
        if unique_ordering, converts (y cross x) to -1 * (x cross y)
            (if y is "greater than" x in the self._unique_ordering scheme.)
        '''
        if any(equals(t, 0) for t in self):
            return 0
        if equals(self.t1, self.t2):
            return 0
        if unique_ordering:
            if not self._unique_ordering(self.t1, self.t2):
                return -1 * self._new(self.t2, self.t1)
        return self    # return self, exactly, to help indicate no changes were made.

    def _vector_id(self, **kw__None):
        '''simplifies the double cross product (A cross B) cross C.
        (AxB)xC --> (A.C)B - (B.C)A
        Ax(BxC) --> (A.C)B - (A.B)C
        '''
        if is_opinstance(self[0], self.CrossProduct):
            A, B, C = self[0][0], self[0][1], self[1]
            return (A.dot(C))*B - (B.dot(C))*A
        elif is_opinstance(self[1], self.CrossProduct):
            A, B, C = self[0], self[1][0], self[1][1]
            return (A.dot(C))*B - (A.dot(B))*C
        else:
            return self

    def _basis_id(self, **kw__None):
        '''simplifies cross products between basis vectors.'''
        a0, v0 = scalar_vector_split(self[0])
        if not is_basis_vector(v0):
            return self
        a1, v1 = scalar_vector_split(self[1])
        if not is_basis_vector(v1):
            return self
        v0_x_v1 = v0._cross_simplify(v1, _return_none_if_no_simplify=True)
        if v0_x_v1 is None:
            return self
        result = a0 * a1 * v0_x_v1
        return result


''' --------------------- Operations - Equation, EquationSystem, Inequality --------------------- '''

class OperationContainer(sc.OperationContainer, MultipleOperation):
    '''class for containing EquationSystem, Equation, Inequality.

    Operations on self will be applied to all objects contained by self.
        E.g. (LHS = RHS) + x --> LHS + x = RHS + x
    '''
    def dot(self, v):   return self.op(lambda u: _simplyfied(self.DotProduct(u, v)))
    def cross(self, v): return self.op(lambda u: _simplyfied(self.CrossProduct(u, v)))


class Equation(sc.Equation, OperationContainer, BinaryOperation):
    def __init__(self, t1, t2, check_rank=True, **kw):
        '''initialize Equation.
        Enforce rank(lhs) == rank(rhs), unless check_rank=False
        '''
        if check_rank:
            if not same_rank(t1, t2):
                if is_vector(t1):
                    rank_mismatch_errmsg = "LHS is vector but RHS is not!"
                else:
                    rank_mismatch_errmsg = "RHS is vector but LHS is not!"
                raise TypeError(rank_mismatch_errmsg)
        super().__init__(t1, t2, **kw)

    @property
    def is_vector(self):
        return is_vector(self[0]) or is_vector(self[1])

    def components(self, basis):
        '''returns components of self in the provided basis.'''
        return tuple(self.dot(u) for u in basis)

    def componentize(self, basis, targets=None, keep_hats=False, **kw):
        '''returns equations for self in the given basis.
        targets: None or list
            None --> substitute all vectors (only the vector symbols in self).
            list --> only substitute the vectors listed here.
        keep_hats: False or True
            False --> DO apply('hats_id')  ( E.g., converts (u dot xhat) --> (u_x) )
            True --> DON'T convert dot product with hat to a subscript.
        '''
        kw.update(dict(targets=targets, keep_hats=keep_hats))
        result = self.op(lambda x: componentize(x, basis, **kw), _prevent_new=True)
        # break result up into components if result is a vector.
        if is_vector(result):
            components = result.components(basis)
            result = components[0]
            for component in components[1:]:
                result += component
        if not keep_hats:
            result = result.apply('hats_id')
        return result

    ## SOLVING ##
    def eliminate(self, x, basis=None, show_work=False, **kw__simplified):
        '''eliminate x from self, if possible.
        if self looks like (x * A = 0),
            return (A = 0).
        if self looks like (A * (x dot u) = 0),
            return (A = 0).
        if self looks like ((x dot u) v + A x = 0),
            return (v dot u + A = 0)   # (comes from dotting both sides with u, and removing x dot u)
        if self looks like (sum_m(A_m * (B_m dot x)) + C x = 0),
            if basis has been entered,
            return (0 = determinant of the matrix (sum_m(A_m * B_m.T) + C I))
        else, raise SystemTooComplicatedError.
        '''
        # try "scalar" methods
        try:
            return sc.Equation.eliminate(self, x, show_work, **kw__simplified)
        except SystemTooComplicatedError:
            pass   # we will try to deal with the complicated system here.
        # handle ((x dot u) v + A x = 0) case.
        #   in this case, solubility list will look like [x, x dot u, x, x, x dot u, ...]
        explain = tools.debug_viewer(show_work)
        solub = self.solubility_list(x)
        solub = tools.Set(solub)
        dot_mul = []       # tells which vectors we find dotted with x.
        for s in solub:
            if isinstance(s, DotProduct):
                dot_mul_candidate = s[1] if equals(s[0], x) else s[0]
                if (basis is not None) or (len(dot_mul) == 0):
                    dot_mul.append(dot_mul_candidate)
                else: # no basis, and found a second dot product with a different vector in the system.
                    errmsg = ('Too challenging to eliminate {} with no basis provided.'
                            ' Found it in dot products with two different vectors:\n{}\n{}'
                            '\n   in the system:\n{}')
                    errmsg = errmsg.format(x, dot_mul[0], dot_mul_candidate, self)
                    raise SystemTooComplicatedError(errmsg)
            elif not equals(s, x):
                raise SystemTooComplicatedError("Don't know how to eliminate {} from\n{}".format(x, self))
        assert len(dot_mul) > 0, "No DotProducts with {} in self.solubility_list():\n{}".format(x, self)
        if len(dot_mul) == 1:  # only one vector is dotted with x. E.g. (x dot u + ...) but not (x dot u + x dot w + ...).
            dot_mul = dot_mul[0]
            if len(solub) == 1:  # the x dot u term is the ONLY term!
                explain('Assuming the o1 term is nonzero: 0 != ({} dot {}).',format=[dot_mul, x])
                return self.eliminate(self.DotProduct(x, dot_mul), show_work=show_work)
            explain('Dotting equation with ({}), then dividing by ({} dot {})', format=[dot_mul, x, dot_mul])
            dotted = self.dot(dot_mul).apply('distribute', focus_only=dot_mul)
            return dotted.eliminate(self.DotProduct(x, dot_mul), show_work=show_work)
        # else: # go to components and take the determinant of the matrix.
        assert basis is not None, "Expected non-None basis at this point."
        if len(basis)!=3:
            raise NotImplementedError("Determinants for dimensions != 3 are not yet implemented.")
        self = self - self[1]  # gather everything on LHS.
        self = self.simplified(collect_only=[*dot_mul, x], **kw__simplified)
        lhs = self[0]
        A_m = []
        B_m = []
        C   = []
        for term in get_summands(lhs):
            factors = get_factors(term)
            for f, factor in enumerate(factors):
                if is_opinstance(factor, DotProduct):
                    if equals(factor[0], x):
                        B_m.append(factor[1])
                    elif equals(factor[1], x):
                        B_m.append(factor[0])
                    else:   # (some unrelated DotProduct which does not contain x).
                        continue
                    # < if we reach this line, we appended somebody to B_m.
                    A_m.append(self.Product(*tools._list_without_i_(factors, f)))
                    break
                elif equals(factor, x):
                    C.append(self.Product(*tools._list_without_i_(factors, f)))
                    break
        #components = [[(bi, bj) for bj in basis] for bi in basis]  # i --> row; j --> column.
        explain('converting equation to a matrix times {}, using the provided basis:', format=[x], view=basis)
        DOT = self.DotProduct
        # [TODO] explore possibilities using alternate formula:
        #   det(A Bx + C xhat | A By + C yhat | A Bz + C zhat)
        #   == (A Bx + C xhat) dot ((A By + C yhat) cross (A Bz + C zhat))
        M = [[0 for b in basis] for b in basis]
        for A, B in zip(A_m, B_m):
            for i, bi in enumerate(basis):
                for j, bj in enumerate(basis):
                    M[i][j] += DOT(A, bi) * DOT(B, bj)
        for c in C:
            for k, bk in enumerate(basis):
                M[k][k] += c
        explain('The result of M x = 0 for vector x, matrix M, is det(M) = 0')
        #det0 = M[0][0] * M[1][1] * M[2][2] - M[0][0] * M[1][2] * M[2][1]
        #det1 = M[0][1] * M[1][2] * M[2][0] - M[0][1] * M[1][0] * M[2][2]
        #det2 = M[0][2] * M[1][0] * M[2][1] - M[0][2] * M[1][1] * M[2][0]
        det0 = M[0][0] * (M[1][1] * M[2][2] - M[1][2] * M[2][1])
        det1 = M[0][1] * (M[1][2] * M[2][0] - M[1][0] * M[2][2])
        det2 = M[0][2] * (M[1][0] * M[2][1] - M[1][1] * M[2][0])
        det = det0 + det1 + det2
        result = self.Equation(det, 0)
        return result

    def solvable(self, x, _return_solubility=False):
        result, solub = sc.Equation.solvable(self, x, _return_solubility=True)
        if result is False:     # sc.Equation doesn't know how to solve. Check for vector tricks.
            solub_set = tools.Set(solub)
            if any(is_opinstance(s, DotProduct) and s._contains_deep_(x) for s in solub_set):
                result = False   # we can't solve (x dot y) for x.
            elif x in solub_set:
                if any(is_opinstance(s, CrossProduct) and s._contains_deep_(x) for s in solub_set):
                    # equation looks like (u x w + a u + c v = 0).
                    result = 'cross_solve'
        return (result, solub) if _return_solubility else (result)

    # -- solving, helper functions -- #

    def cross_solve(self, u, cdebug=False, simplify_result=False, _assert_solvable=True, **kw__simplified):
        '''solving equation which looks like: u x w + a u + c v = 0, with vectors u, v, w; scalars a, c; a != 0.'''
        # assertions & bookkeeping
        assert is_vector(u), "cross_solve(u) assumes u is a vector but got u={}".format(u)
        if _assert_solvable:
            assert self.solvable(u) == 'cross_solve'
        explain = tools.debug_viewer(cdebug)
        explain('cross_solve-ing for u={} from:', format=[u], view=self)
        # put equation in the proper format.
        lhs = self[0] - self[1]  # move stuff to lhs.
        lhs = lhs.cross_format(u, u_first=True)   # 
        lhs = lhs.simplify(collect_only=[u], **kw__simplified)  # no expand(). E.g. expand(u x (n B)) --> n (u x B). 
        self = self._new(lhs, 0)
        # << now we should have lhs = u x w + a u + cv,  rhs = 0.
        explain('After bringing RHS to LHS, eqn looks like:', view=self)
        if not isinstance(self[0], Sum):
            raise NotImplementedError("cross_solve expected u x w + a u + c v form, u={}, but got:\n{}".format(u, self))
        cross_term = None
        u_term     = None
        v_term     = Sum.IDENTITY
        for t in self[0]:
            if is_opinstance(t, CrossProduct) and equals(t[0], u):
                assert cross_term is None, "Multiple cross terms?\n{}\nand\n{}".format(cross_term, t)
                cross_term = t
            elif apply(t, '_contains_deep_', u):
                assert u_term is None, "Multiple u terms?\n{}\nand\n{}".format(u_term, t)
                u_term = t
            else:
                v_term += t
        c, v = scalar_vector_split(v_term.simplified(**kw__simplified))
        a, _ = scalar_vector_split(u_term)
        _, w = cross_term
        explain("eqn looks like u x w + a u + c v = 0, with\nu={}\nw={}\na={}\nc={}\nv={}", format=[u, w, a, c, v])
        # TODO: add assumptions to the result.
        # namely: c v != 0, a != 0.
        #coef  = c / (a**2 + w.dot(w))
        #terms = v.cross(w) - a * v - (v.dot(w) / a) * w
        coef  = -1 * c / (a**2 + w.dot(w))
        terms = w.cross(v) + a * v + (v.dot(w) / a) * w
        result = self._new(u, coef * terms)
        if simplify_result:
            result = result.simplified(**kw__simplified)
        return result


class EquationSystem(sc.EquationSystem, OperationContainer, MultipleOperation):
    def componentize(self, basis, targets=None, unsolved_only=True, **kw):
        '''breaks vector equations in self into components, according to the basis provided.
        For basis construction, see for example, Basis() or OrthonormalBasis3D().

        targets: None or list
            None --> substitute all vectors (only the vector symbols in self).
            list --> only substitute the vectors listed here.

        if unsolved_only, do not componentize any equations marked as "solved".
        '''
        if len(self) == 0:
            return self
        # else
        eqlist = []
        solved = []
        j = 0
        for i, eqn in enumerate(self):
            isolved = (i in self.solved)
            if unsolved_only and isolved:
                solved.append(j)
                eqlist += [eqn]
                j += 1
            else:
                components = self[i].componentize(basis, targets=targets, **kw)
                if is_opinstance(components, Equation):
                    components = [components]
                eqlist += [*components]
                Ncomp = len(components)
                if isolved:
                    solved += range(j, j + Ncomp)
                j += Ncomp
            #else:
            #    eqlist += [eqn]
            #    j += 1
        return self._new(*eqlist, solved=solved)

    def dof(self, Ndim, unsolved_only=True):
        '''number of constraints represented by all the equations in self.'''
        unsolved = self.unsolved if unsolved_only else range(len(self))
        return sum(self[i].dof(Ndim) for i in unsolved)

    def check_freedom(self, free, Ndim, unsolved_only=True):
        '''return number of free dof, number of constraints from self.'''
        Nfree      = dof_from_list(free, Ndim)
        Nconstrain = self.dof(Ndim, unsolved_only=unsolved_only)
        return Nfree, Nconstrain

    def warn_if_trapped(self, free, Ndim):
        '''warns if number of free dof is greater than number of constraints from self.'''
        Nfree, Nconstrain = self.check_freedom(free, Ndim)
        if Nfree > Nconstrain:
            errmsg_unconstrained = ('System {} may be unconstrained. Got {} free,'
                                    '{} constraints').format(hex(id(self)), Nfree, Nconstrain)
            warnings.warn(errmsg_unconstrained)
        return Nfree, Nconstrain

class Inequality(sc.Inequality, OperationContainer, BinaryOperation):
    pass